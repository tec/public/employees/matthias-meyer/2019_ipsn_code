#!/usr/bin/env python3

"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


#
# scripts sends waveform samples to the serial port
#
# 2018, rdaforno
#

from threading import Thread
from serial.serialutil import SerialException
import serial
import sys
import time
import os.path
import numpy as np

# numpy waveform file
filename = '../data/waveforms.npy'
resultsFile = '../data/nn_out.npy'

# sleep time between two samples, can be adjusted to reach the desired emulated sampling frequency
sleepTimeBtwSamples = 0 # 0.00075

# whether or not to also read from the serial port
readFromSerial = True

# used to abort the threads
abort = False

# counting variables
sentWaveforms = 0
resultInv = 0
resultOk  = 0
cpuDC     = 0.0


def connectToSerialPort(serialPort, baudRate):
  try:
    serHandle = serial.Serial(port=serialPort, baudrate=baudRate, timeout=None)
    if serHandle.isOpen():
      return serHandle
  except SerialException:
    print("device %s unavailable" % serialPort)
  except ValueError:
    print("invalid arguments")
  except OSError:
    print("device %s not found" % serialPort)
  except:
    t, v, x = sys.exc_info()
    print("error: %s (%s)" % (v.message, t))
  return None


def writeWaveformToSerialPort(serHandle, filename):
  global abort, sleepTimeBtwSamples, resultInv, resultOk, sentWaveforms
  if not serHandle.isOpen():
    return
  # load waveform and convert to 32-bit integer values
  waveforms = np.int32(np.load(filename))
  numRows = waveforms.shape[0]
  numCols = waveforms.shape[1]
  print("waveform matrix loaded (%u x %u)\r\nsending samples..." % (numRows, numCols))
  try:
    idx = 0
    sampleCount = 0
    numBytes = 0
    row = 0
    startTime = time.time()
    sys.stdout.write("samples | freq | ok  | inv\r\n                          ")
    sys.stdout.flush()
    while not abort:
      loopStart = time.time()
      numBytes = numBytes + serHandle.write(waveforms[row][idx:idx+1])
      idx = idx + 1
      sampleCount = sampleCount + 1
      if (idx % 100) == 0:
        sps = int(sampleCount / (time.time() - startTime))
        sys.stdout.write("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b%7u | %4u | %3u | %3u" % (sampleCount, sps, resultOk, resultInv))
        sys.stdout.flush()
      if idx == numCols:
        idx = 0
        row = row + 1
        sentWaveforms = row
        if row == numRows:
          break
      time.sleep(sleepTimeBtwSamples)
    serHandle.flushOutput()
    time.sleep(1)
  except IOError:
    print("IO error (device removed?)")
  except:
    t, v, x = sys.exc_info()
    print("error: %s (%s)" % (v.message, t))
  abort = True


def readFromSerialPort(serHandle, filename):
  global abort, lastCPUDC, resultOk, resultInv, cpuDC
  if not serHandle.isOpen():
    return
  results = None
  numRows = 0
  # load results file
  if filename != '' and os.path.isfile(filename):
    results = np.float32(np.load(filename))
    # check matrix dimensions
    if results.shape[1] != 3:
      print("invalid dimensions for results matrix!")
      results = None
    else:
      numRows = results.shape[0]
  try:
    serHandle.flushInput()
    serHandle.flushOutput()
    count = 0
    while not abort:
      if serHandle.inWaiting():
        #line = serHandle.read(serHandle.inWaiting()).strip()
        line = serHandle.readline().strip()
        # parse the line
        result = line.decode().split(',')    # decode seems to be necessary in python3
        if len(result) != 3:
          # unexpected output
          print("\r\nunexpected data received: '%s'" % line.decode())
        else:
          # check result
          if results is not None:
            #expVal = float(int(results[count,1] * 1000.0) / 1000.0)    # chop off after 3 digits
            # if int(results[count][0]) != int(result[0]) or float(result[1]) != expVal:
            if int(results[count,0]) != int(result[0]):
              resultInv = resultInv + 1
            else:
              resultOk = resultOk + 1
            count = count + 1
            if count == numRows:
              print("\r\nend of results file reached, aborting...")
              break
          else:
            resultOk = resultOk + 1
          cpuDC = float(result[2])
      time.sleep(0.01)
  except IOError:
    print("\r\nIO error (device removed?)")
  except ValueError:
    print("\r\nvalue error, input was '%s'" % line.decode())
  abort = True


if __name__ == "__main__":
  
  # check arguments
  if len(sys.argv) < 2:
    print("no serial port provided\r\nusage:  %s [port] ([filename])" % sys.argv[0])
    sys.exit()
  serialPort = sys.argv[1]
  # optional: if there is a 2nd argument, assume it is the filename
  if len(sys.argv) > 2:
    filename = sys.argv[2]
  # check filename
  if not os.path.isfile(filename):
    print("file '%s' not found" % filename)
    sys.exit()
  
  # open the serial port
  serHandle = connectToSerialPort(serialPort, 115200)
  if serHandle is None:
    sys.exit()
  print("connected to " + serHandle.portstr + " (" + str(serHandle.baudrate) + ")")
  # start thread
  print("starting threads...")
  if readFromSerial:
    thread1 = Thread(target = readFromSerialPort, args = (serHandle, resultsFile ))
    thread1.start()
  thread2 = Thread(target = writeWaveformToSerialPort, args = (serHandle, filename ))
  thread2.start()
  
  # busy wait loop 
  try:
    while not abort:
      time.sleep(0.2)
  except KeyboardInterrupt:
    print("\b\b  \r\n--- aborted ---")
  
  # wait for the threads to terminate
  abort = True
  if readFromSerial:
    thread1.join()
  thread2.join()
  if serHandle.isOpen():
    serHandle.close()
  
  # print(summary:
  print("\r\n--- summary: ---\r\nwaveforms sent: %u\r\ncorrect results: %u\r\nwrong results: %u\r\naverage CPU duty cycle: %.2f" % (sentWaveforms, resultOk, resultInv, cpuDC))
  

