# Code for IPSN2019 submission

Please see the respective subfolders ReadMe for more information

## mhcnn

Contains the python code to train the convolutional neural network and generate parameter files for MCU implementation.


## mhmcu

Contains the C Code which implements the trained convolutional neural network on a MCU. The parameter files are pregenerated. The ReadMe in *mhcnn* explains how to generate your own parameter files. The implementation has been developed with Atollic TrueSTUDIO for STM32 (Version 9.0.1).

## tools

Helper tools to evaluate the MCU implementation.

## data

Please follow the ReadMe in this folder to download the data correctly.


Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
All rights reserved.