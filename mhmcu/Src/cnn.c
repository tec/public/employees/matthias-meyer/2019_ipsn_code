/*
 * Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Timo Farei-Campagna, Matthias Meyer
 */

#include "cnn.h"
#include "model_weights.h"


// buffers for all CNN layers
static data_buffer ib;
float b_input[B_INPUT_SIZE];

static data_buffer b0;
float b0_data[B0_SIZE];

static data_buffer b1;
float b1_data[B1_SIZE];

static data_buffer b2;
float b2_data[B2_SIZE];

static data_buffer b3;
float b3_data[B3_SIZE];

static data_buffer b4;
float b4_data[B4_SIZE];

static data_buffer b5;
float b5_data[B5_SIZE];

static data_buffer b6;
float b6_data[B6_SIZE];

static data_buffer b7;
float b7_data[B7_SIZE];

static data_buffer f_avg;
float f_avg_data[F_AVG_SIZE];

static data_buffer t_avg;
float t_avg_data[T_AVG_SIZE];

float nn_outputs[NUM_LABELS];
uint8_t nn_labels[NUM_LABELS];

void CNN_Init()
{
// initialize the input buffer
  ib.r_ptr = &b_input[0];
  ib.w_ptr = &b_input[2];
  ib.in_ptr = NULL;       // will be set by do_inference function
  ib.num_channels = 1;
  ib.num_rows = 64;
  ib.num_columns = 6;
  ib.num_shift = 4;
  ib.num_non_buffer_columns = 4;


  // initialize the b0 buffer
  b0.r_ptr = &b0_data[0];
  b0.w_ptr = &b0_data[1];
  b0.in_ptr = ib.r_ptr;
  b0.num_channels = B0_NUM_CHANNELS;
  b0.num_rows = 64;
  b0.num_columns = 5;
  b0.num_shift = 4;
  b0.num_non_buffer_columns = 4;
  b0.opcode2_num_hop_columns = 1;
  b0.opcode3_num_hop_columns = 1;

  b0.fd.kernel = &Layer0_kernel[0];
  b0.fd.bias = &Layer0_bias[0];
  b0.fd.n1 = &Layer0_2_n1[0];
  b0.fd.num_output_channels = Layer0_dims[0];
  b0.fd.num_input_channels = Layer0_dims[1];
  b0.fd.num_rows = Layer0_dims[2];
  b0.fd.num_columns = Layer0_dims[3];
  b0.fd.stride = 1;


  // initialize the b1 buffer
  b1.r_ptr = &b1_data[0];
  b1.w_ptr = &b1_data[2];
  b1.in_ptr = b0.r_ptr;
  b1.num_channels = B1_NUM_CHANNELS;
  b1.num_rows = 32;
  b1.num_columns = 4;
  b1.num_shift = 2;
  b1.num_non_buffer_columns = 2;
  b1.opcode2_num_hop_columns = 1;
  b1.opcode3_num_hop_columns = 1;

  b1.fd.kernel = &Layer1_kernel[0];
  b1.fd.bias = &Layer1_bias[0];
  b1.fd.n1 = &Layer1_2_n1[0];
  b1.fd.num_output_channels = Layer1_dims[0];
  b1.fd.num_input_channels = Layer1_dims[1];
  b1.fd.num_rows = Layer1_dims[2];
  b1.fd.num_columns = Layer1_dims[3];
  b1.fd.stride = 2;


  // initialize the b2 buffer
  b2.r_ptr = &b2_data[0];
  b2.w_ptr = &b2_data[2];
  b2.in_ptr = b1.r_ptr;
  b2.num_channels = B2_NUM_CHANNELS;
  b2.num_rows = 32;
  b2.num_columns = 4;
  b2.num_shift = 2;
  b2.num_non_buffer_columns = 2;
  b2.opcode2_num_hop_columns = 0;
  b2.opcode3_num_hop_columns = 0;

  b2.fd.kernel = &Layer2_kernel[0];
  b2.fd.bias = &Layer2_bias[0];
  b2.fd.n1 = &Layer2_2_n1[0];
  b2.fd.num_output_channels = Layer2_dims[0];
  b2.fd.num_input_channels = Layer2_dims[1];
  b2.fd.num_rows = Layer2_dims[2];
  b2.fd.num_columns = Layer2_dims[3];
  b2.fd.stride = 1;


  // initialize the b3 buffer
  b3.r_ptr = &b3_data[0];
  b3.w_ptr = &b3_data[2];
  b3.in_ptr = b2.r_ptr;
  b3.num_channels = B3_NUM_CHANNELS;
  b3.num_rows = 16;
  b3.num_columns = 3;
  b3.num_shift = 1;
  b3.num_non_buffer_columns = 1;
  b3.opcode2_num_hop_columns = 0;
  b3.opcode3_num_hop_columns = 0;

  b3.fd.kernel = &Layer3_kernel[0];
  b3.fd.bias = &Layer3_bias[0];
  b3.fd.n1 = &Layer3_2_n1[0];
  b3.fd.num_output_channels = Layer3_dims[0];
  b3.fd.num_input_channels = Layer3_dims[1];
  b3.fd.num_rows = Layer3_dims[2];
  b3.fd.num_columns = Layer3_dims[3];
  b3.fd.stride = 2;


  // initialize the b4 buffer
  b4.r_ptr = &b4_data[0];
  b4.w_ptr = &b4_data[0];
  b4.in_ptr = b3.r_ptr;
  b4.num_channels = B4_NUM_CHANNELS;
  b4.num_rows = 16;
  b4.num_columns = 1;
  b4.num_shift = 0;
  b4.num_non_buffer_columns = 1;
  b4.opcode2_num_hop_columns = 0;
  b4.opcode3_num_hop_columns = 0;

  b4.fd.kernel = &Layer4_kernel[0];
  b4.fd.bias = &Layer4_bias[0];
  b4.fd.n1 = &Layer4_2_n1[0];
  b4.fd.num_output_channels = Layer4_dims[0];
  b4.fd.num_input_channels = Layer4_dims[1];
  b4.fd.num_rows = Layer4_dims[2];
  b4.fd.num_columns = Layer4_dims[3];
  b4.fd.stride = 1;


  // initialize the b5 buffer
  b5.r_ptr = &b5_data[0];
  b5.w_ptr = &b5_data[0];
  b5.in_ptr = b4.r_ptr;
  b5.num_channels = B5_NUM_CHANNELS;
  b5.num_rows = 16;
  b5.num_columns = 1;
  b5.num_shift = 0;
  b5.num_non_buffer_columns = 1;
  b5.opcode2_num_hop_columns = 0;
  b5.opcode3_num_hop_columns = 0;

  b5.fd.kernel = &Layer5_kernel[0];
  b5.fd.bias = &Layer5_bias[0];
  b5.fd.n1 = &Layer5_2_n1[0];
  b5.fd.num_output_channels = Layer5_dims[0];
  b5.fd.num_input_channels = Layer5_dims[1];
  b5.fd.num_rows = Layer5_dims[2];
  b5.fd.num_columns = Layer5_dims[3];
  b5.fd.stride = 1;

  // initialize the b6 buffer
  b6.r_ptr = &b6_data[0];
  b6.w_ptr = &b6_data[0];
  b6.in_ptr = b5.r_ptr;
  b6.num_channels = NUM_LABELS;
  b6.num_rows = 16;
  b6.num_columns = 1;
  b6.num_shift = 0;
  b6.num_non_buffer_columns = 1;
  b6.opcode2_num_hop_columns = 0;
  b6.opcode3_num_hop_columns = 0;

  b6.fd.kernel = &Layer6_kernel[0];
  b6.fd.bias = &Layer6_bias[0];
  b6.fd.n1 = &Layer6_2_n1[0];
  b6.fd.num_output_channels = Layer6_dims[0];
  b6.fd.num_input_channels = Layer6_dims[1];
  b6.fd.num_rows = Layer6_dims[2];
  b6.fd.num_columns = Layer6_dims[3];
  b6.fd.stride = 1;

  // initialize the f_avg buffer
  f_avg.r_ptr = &f_avg_data[0];
  f_avg.w_ptr = &f_avg_data[COMPLETE_T/2/2-1];
  f_avg.in_ptr = b6.r_ptr;
  f_avg.num_channels = NUM_LABELS;
  f_avg.num_rows = 1;
  f_avg.num_columns = COMPLETE_T/2/2;
  f_avg.num_shift = 1;
  f_avg.num_non_buffer_columns = 1;
  f_avg.opcode2_num_hop_columns = 0;
  f_avg.opcode3_num_hop_columns = 0;

  // initialize the t_avg buffer
  t_avg.r_ptr = &t_avg_data[0];
  t_avg.w_ptr = &t_avg_data[0];
  t_avg.in_ptr = f_avg.r_ptr;
  t_avg.num_channels = NUM_LABELS;
  t_avg.num_rows = 1;
  t_avg.num_columns = 1;
  t_avg.num_shift = 0;
  t_avg.num_non_buffer_columns = 1;
  t_avg.opcode2_num_hop_columns = 0;
  t_avg.opcode3_num_hop_columns = 0;

  // initialize the b7 buffer
  b7.r_ptr = &b7_data[0];
  b7.w_ptr = &b7_data[0];
  b7.in_ptr = t_avg.r_ptr;
  b7.num_channels = NUM_LABELS;
  b7.num_rows = 1;
  b7.num_columns = 1;
  b7.num_shift = 0;
  b7.num_non_buffer_columns = 1;
  b7.opcode2_num_hop_columns = 0;
  b7.opcode3_num_hop_columns = 0;

  b7.fd.kernel = &Layer7_kernel[0];
  b7.fd.bias = &Layer7_bias[0];
  b7.fd.n1 = &Layer7_2_n1[0];
  b7.fd.num_output_channels = Layer7_dims[0];
  b7.fd.num_input_channels = Layer7_dims[1];
  b7.fd.num_rows = Layer7_dims[2];
  b7.fd.num_columns = Layer7_dims[3];
  b7.fd.stride = 1;
}


/*
 * due to padding in the current network, the inference procedure is realitvely complex. Padding can, however, be
 * disabled such that a more regular inference function with lesser if-statements can be written.
 */
uint8_t do_inference(float *input_buffer, CNN_Output_t *cnn_output)
{
  static uint32_t img_fraction_counter = 0;
  img_fraction_counter++;
  if(img_fraction_counter == COMPLETE_T/TEMP_INPUT_NUM_ROWS) {
    img_fraction_counter = 0;
  }

  ib.in_ptr = input_buffer;


  if(img_fraction_counter != 0) { // the last fraction of the current input has not been received yet
    // input buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter != 1) { // if equal to 1, the buffer contains only zeros anyway, so avoid shifting them
      input_shift();
    }

    // copy DMA_Rx_buffer content to the input buffer
    input_update(1);


    // layer0 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter == 1) { // after the first input fraction has been received, only a part of the buffer must be computed
      conv2D_3(1, &ib, &b0);
      conv2D_3(2, &ib, &b0);
      relu(&b0);
    }
    else {  // the complete buffer must be computed
      shift(&b0);
      conv2D_3(1, &ib, &b0);
      relu(&b0);
    }
    // layer1 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter == 1) { // after the first input fraction has been received, only a part of the buffer must be computed
      conv2D_3(1, &b0, &b1);
      conv2D_3(2, &b0, &b1);
      relu(&b1);
    }
    else {  // the complete buffer must be computed
      shift(&b1);
      conv2D_3(1, &b0, &b1);
      relu(&b1);
    }
    // layer2 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter == 2) {
      conv2D_3(1, &b1, &b2);
      relu(&b2);
    }
    else if(img_fraction_counter > 2) {
      shift(&b2);
      conv2D_3(1, &b1, &b2);
      relu(&b2);
    }
    // layer3 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter > 2) {
      shift(&b3);
      conv2D_3(1, &b2, &b3);
      relu(&b3);
    }
    // layer4 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter > 3) {
      conv2D_3(1, &b3, &b4);
      relu(&b4);
    }
    // layer5 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter > 3) {
      conv2D_1(&b4, &b5);
      relu(&b5);
    }
    // layer6 buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter > 3) {
      conv2D_1(&b5, &b6);
      relu(&b6);
    }
    // f_avg buffer
    // ----------------------------------------------------------------------------------------------------------
    if(img_fraction_counter == 4) {
      f_avg_update(&b6, &f_avg);
    }
    else if(img_fraction_counter > 4) {
      shift(&f_avg);
      f_avg_update(&b6, &f_avg);
    }

    return 0;
  }
  else {
    // immediately after the last input fraction has been received
    /*
     * The time for the remaining instructions is 4*t_hop_fft since after that time the next 4 columns of the
     * Mel spectrogram might be ready to be processed!
     */
    input_shift();
    input_update(1);

    shift(&b0);
    conv2D_3(1, &ib, &b0);
    relu(&b0);

    shift(&b1);
    conv2D_3(1, &b0, &b1);
    relu(&b1);

    shift(&b2);
    conv2D_3(1, &b1, &b2);
    relu(&b2);

    shift(&b3);
    conv2D_3(1, &b2, &b3);
    relu(&b3);

    conv2D_3(1, &b3, &b4);
    relu(&b4);

    conv2D_1(&b4, &b5);
    relu(&b5);

    conv2D_1(&b5, &b6);
    relu(&b6);

    shift(&f_avg);
    f_avg_update(&b6, &f_avg);




    /*
     * During the next three code segments the remaining content of the buffer system must propagate to the
     * final layer.
     */
    // segment1
    input_shift();
    input_update(0);

    shift(&b0);
    conv2D_3(1, &ib, &b0);
    conv2D_3(3, &ib, &b0);
    relu(&b0);

    shift(&b1);
    conv2D_3(1, &b0, &b1);
    conv2D_3(3, &b0, &b1);
    relu(&b1);

    shift(&b2);
    conv2D_3(1, &b1, &b2);
    relu(&b2);

    shift(&b3);
    conv2D_3(1, &b2, &b3);
    relu(&b3);

    conv2D_3(1, &b3, &b4);
    relu(&b4);

    conv2D_1(&b4, &b5);
    relu(&b5);

    conv2D_1(&b5, &b6);
    relu(&b6);

    shift(&f_avg);
    f_avg_update(&b6, &f_avg);





    // segment2
    input_shift();

    shift(&b0);

    shift(&b1);
    conv2D_3(0, &b0, &b1);

    shift(&b2);
    conv2D_3(0, &b1, &b2);

    shift(&b3);
    conv2D_3(1, &b2, &b3);
    relu(&b3);

    conv2D_3(1, &b3, &b4);
    relu(&b4);

    conv2D_1(&b4, &b5);
    relu(&b5);

    conv2D_1(&b5, &b6);
    relu(&b6);

    shift(&f_avg);
    f_avg_update(&b6, &f_avg);






    // segment3
    shift(&b1);
    shift(&b2);

    shift(&b3);
    conv2D_3(0, &b2, &b3);

    conv2D_3(1, &b3, &b4);
    relu(&b4);

    conv2D_1(&b4, &b5);
    relu(&b5);

    conv2D_1(&b5, &b6);
    relu(&b6);

    shift(&f_avg);
    f_avg_update(&b6, &f_avg);

    // print_buffer(f_avg.r_ptr, f_avg.num_channels, f_avg.num_rows, f_avg.num_columns);

    t_avg_update(&f_avg, &t_avg);

    conv2D_1(&t_avg, &b7);

    // activation_softmax(&t_avg);
    activation_sigmoid(&b7);


        for(uint16_t ii=0;ii<NUM_LABELS;ii++){
            cnn_output->value[ii] = b7.r_ptr[ii];
            if(b7.r_ptr[ii] > NN_OUT_THRESHOLD)
                cnn_output->decision[ii] = 1;
            else
                cnn_output->decision[ii] = 0;
        }

    return 1;
  }
}




uint32_t determine_label(data_buffer *buf) {
  uint32_t num_elements = buf->num_channels*buf->num_rows*buf->num_columns;
  uint32_t res = 1; // guessing "strategy" <-- not choosing 0 because approx. the first 250 inputs have label 0, so you wouldn't notice if the algo is guessing or not.

  for(uint32_t elem=0; elem<num_elements; elem++) {
    if(buf->r_ptr[elem] > buf->r_ptr[res]) {
      res = elem;
    }
  }

  return res;
}


void relu(data_buffer *buf) {
  // ReLu activation maps all negative values in a given buffer to zero
  float *w_working_ptr = &buf->w_ptr[0];

  for(uint32_t channel=0; channel<buf->num_channels; channel++) {
    for(uint32_t row=0; row<buf->num_rows; row++) {
      for(uint32_t column=0; column < buf->num_non_buffer_columns; column++) {
        if(w_working_ptr[column] < 0) {
          w_working_ptr[column] = 0;
        }
      }
      w_working_ptr += buf->num_columns;
    }
  }
}



void activation_softmax(data_buffer *buf) {
  // softmax activation computes the probabilistic output of the CNN
  float temp = 0;
  for(uint32_t channel=0; channel<buf->num_channels; channel++) {
    temp += (float)exp(buf->r_ptr[channel]);
  }
  for(uint32_t channel=0; channel<buf->num_channels; channel++) {
    buf->r_ptr[channel] = (float)exp(buf->r_ptr[channel])/temp;
  }
}

void activation_sigmoid(data_buffer *buf) {
  // sigmoid activation
  for(uint32_t channel=0; channel<buf->num_channels; channel++) {
    buf->r_ptr[channel] = 1.0f / (1.0f + exp(-buf->r_ptr[channel]));
  }
}


void t_avg_update(data_buffer *inbuf, data_buffer *outbuf) {
  float *in_ptr = &inbuf->r_ptr[0];

  float *out_ptr;
  float *out_channel_start_ptr = &outbuf->w_ptr[0];

  uint32_t outbuf_size_per_channel = outbuf->num_columns*outbuf->num_rows;
  uint32_t outbuf_size = outbuf->num_channels*outbuf_size_per_channel;
  uint32_t inbuf_size_per_channel = inbuf->num_rows*inbuf->num_columns;

  float *out_limit = &outbuf->w_ptr[outbuf_size];
  float *in_row_limit;

  // compute convolution
  do {  // loop over input/output channels
    out_ptr = out_channel_start_ptr;  // set the out_ptr to the correct place for the current output channel
    in_row_limit = &in_ptr[inbuf_size_per_channel]; // determine the row limit for the current input channel

    // initialize output to zero
    *out_ptr = 0;

    do {  // loop over input columns in the current input channel
      // sum up all elements from the current input channel
      *out_ptr += *in_ptr;
//      *out_ptr = 111;

      // update input pointer
      in_ptr++;
    } while(in_ptr != in_row_limit);

    *out_ptr /= inbuf->num_columns; // take the average

  out_channel_start_ptr += outbuf_size_per_channel; // update output channel start pointer
  } while(out_channel_start_ptr != out_limit);
}




void f_avg_update(data_buffer *inbuf, data_buffer *outbuf) {
  float *in_ptr = &inbuf->r_ptr[0];

  float *out_ptr;
  float *out_channel_start_ptr = &outbuf->w_ptr[0];

  uint32_t outbuf_size_per_channel = outbuf->num_columns*outbuf->num_rows;
  uint32_t outbuf_size = outbuf->num_channels*outbuf_size_per_channel;
  uint32_t inbuf_size_per_channel = inbuf->num_rows*inbuf->num_columns;

  float *out_limit = &outbuf->w_ptr[outbuf_size];
  float *in_row_limit;

  // compute convolution
  do {  // loop over input/output channels
    out_ptr = out_channel_start_ptr;  // set the out_ptr to the correct place for the current output channel
    in_row_limit = &in_ptr[inbuf_size_per_channel]; // determine the row limit for the current input channel

    // initialize output to zero
    *out_ptr = 0;

    do {  // loop over input rows in the current input channel
      // sum up all elements from the current input channel
      *out_ptr += *in_ptr;
//      *out_ptr = 111;

      // update input pointer
      in_ptr++;
    } while(in_ptr != in_row_limit);

    *out_ptr /= inbuf->num_rows; // take the average

  out_channel_start_ptr += outbuf_size_per_channel; // update output channel start pointer
  } while(out_channel_start_ptr != out_limit);
}



void input_update(uint8_t opcode) {

  // prepare working pointers
  float *w_working_ptr = &ib.w_ptr[0];
  float *in_row0_ptr = &ib.in_ptr[0];

  // // read the label if necessary
  // if(img_fraction_counter == 1) {
  //  ib.label = in_row0_ptr[0];
  //  in_row0_ptr++;
  // }

  float *in_row1_ptr = &in_row0_ptr[TEMP_INPUT_NUM_COLUMNS];
  float *in_row2_ptr = &in_row1_ptr[TEMP_INPUT_NUM_COLUMNS];
  float *in_row3_ptr = &in_row2_ptr[TEMP_INPUT_NUM_COLUMNS];

  if (opcode == 1) {  // copy the values from the DMA_Rx_buffer
    for(uint32_t idx=0; idx<TEMP_INPUT_NUM_COLUMNS; idx++) {
      *w_working_ptr++ = in_row0_ptr[idx];
      *w_working_ptr++ = in_row1_ptr[idx];
      *w_working_ptr++ = in_row2_ptr[idx];
      *w_working_ptr++ = in_row3_ptr[idx];

      w_working_ptr += B_INPUT_NUM_COLUMNS - B_INPUT_SHIFT; // increment pointer to hop over the first two actual buffer columns
    }
  }
  else if(opcode == 0) {  // set the four most right columns to zero
    for(uint32_t idx=0; idx<TEMP_INPUT_NUM_COLUMNS; idx++) {
      *w_working_ptr++ = 0;
      *w_working_ptr++ = 0;
      *w_working_ptr++ = 0;
      *w_working_ptr++ = 0;

      w_working_ptr += B_INPUT_NUM_COLUMNS - B_INPUT_SHIFT; // increment pointer to hop over the first two actual buffer columns
    }
  }
  else {
    // USART_puts("OPERATION CODE NOT KNOWN YET!\n\r");
  }
}


void input_shift(void) {
  float *r_working_ptr = &ib.r_ptr[0];

  for(uint32_t row=0; row<B_INPUT_NUM_ROWS; row++) {
    r_working_ptr[0] = r_working_ptr[B_INPUT_SHIFT];
    r_working_ptr++;
    r_working_ptr[0] = r_working_ptr[B_INPUT_SHIFT];

    r_working_ptr += B_INPUT_SHIFT + 1;
  }
}

float LuT_kernel[LUT_SIZE];
float LuT_bias[LUT_SIZE];

void update_LuT(filter_data *data) {
  // construct lookup table

  // positive values
  for(uint8_t ii=0; ii<(LUT_SIZE - 1)/2; ii++) {
    LuT_kernel[ii] = data->n1[0]/(float)pow(2, ii);
    LuT_bias[ii] = data->n1[1]/(float)pow(2, ii);
  }
  // negative values
  for(uint8_t ii=(LUT_SIZE - 1)/2; ii<LUT_SIZE - 1; ii++) {
    LuT_kernel[ii] = -LuT_kernel[ii - (LUT_SIZE - 1)/2];
    LuT_bias[ii] = -LuT_bias[ii - (LUT_SIZE - 1)/2];
  }
  // zero
  LuT_kernel[LUT_SIZE - 1] = 0;
  LuT_bias[LUT_SIZE - 1] = 0;
}


void shift(data_buffer *buf) {
#ifdef DO_TIME_MEASUREMENTS
  // start time measurement
  time = HAL_GetTick();

  // reset cycle counter
  DWT->CYCCNT = 0;
#endif  // DO_TIME_MEASUREMENTS

  float *r_working_ptr = &buf->r_ptr[0];

  uint32_t num_actual_buffer_columns = buf->num_columns - buf->num_non_buffer_columns;

  for(uint32_t channel=0; channel<buf->num_channels; channel++) {
    for(uint32_t row=0; row<buf->num_rows; row++) {
      for(uint32_t column=0; column<num_actual_buffer_columns; column++) {
        r_working_ptr[column] = r_working_ptr[column + buf->num_shift];
      }

      r_working_ptr += buf->num_columns;
    }
  }

#ifdef DO_TIME_MEASUREMENTS
  // store the cycle count minus the 4 cycles needed to load the counter value itself
  cycles = DWT->CYCCNT - 4;
  // stop time measurement
  time = HAL_GetTick() - time;

  // print cycle & time information
  sprintf(img_msg_buffer, "\n\rshift:\n\r %" PRIu32 " cycles, %" PRIu32 " ms\n\r", cycles, time);
  USART_puts(img_msg_buffer);
#endif  // DO_TIME_MEASUREMENTS
}



void conv2D_1(data_buffer *inbuf, data_buffer *outbuf) {
  // computes the convolution with kernels that have k = 1 element

#ifdef DO_TIME_MEASUREMENTS
  // start time measurement
  time = HAL_GetTick();

  // reset cycle counter
  DWT->CYCCNT = 0;
#endif  // DO_TIME_MEASUREMENTS

  float *in_ptr = &inbuf->r_ptr[0];

  float *out_ptr;
  float *out_channel_start_ptr = &outbuf->w_ptr[0];

  filter_data *fd = &outbuf->fd;
  const uint8_t *bias_ptr = &fd->bias[0];
  const uint8_t *kernel_ptr = &fd->kernel[0];

  uint32_t outbuf_size_per_channel = outbuf->num_columns*outbuf->num_rows;
  uint32_t outbuf_size = outbuf->num_channels*outbuf_size_per_channel;
  uint32_t inbuf_size_per_channel = inbuf->num_rows*inbuf->num_columns;
  uint32_t inbuf_size = inbuf->num_channels*inbuf_size_per_channel;

  float *out_limit = &outbuf->w_ptr[outbuf_size];
  float *out_channel_limit_ptr = &outbuf->w_ptr[outbuf_size_per_channel];
  float *in_limit = &inbuf->r_ptr[inbuf_size];
  float *in_row_limit;

  float filter0;

  float input0;


  // load the desired weights
  update_LuT(fd);

  // compute convolution
  do {  // loop over output channels
    out_ptr = out_channel_start_ptr;  // set the out_ptr to the correct place for the current output channel


    do {  // loop over rows of the current output channel
      *out_ptr = LuT_bias[*bias_ptr];

      out_ptr += outbuf->num_columns;
    } while(out_ptr != out_channel_limit_ptr);

    bias_ptr++; // increment the bias pointer to the next filter channel
    out_channel_limit_ptr += outbuf->num_rows*outbuf->num_columns;  // increment the bias pointer's limiter to the end of the next output channel

    out_ptr = out_channel_start_ptr;  // reset output pointer for MAC operation

    do {  // loop over input channels
      in_row_limit = &in_ptr[inbuf_size_per_channel]; // determine the row limit for the current input channel

      // store the current filter channel (in CPU registers)
      filter0 = LuT_kernel[kernel_ptr[0]];


      do {  // loop over input rows in the current input channel
        // compute MAC operation
        input0 = in_ptr[0];

        *out_ptr += input0*filter0;
//        *out_ptr = 111;

        // update input/output pointers
        out_ptr++;
        in_ptr ++;
      } while(in_ptr != in_row_limit);

      kernel_ptr ++;  // increment kernel pointer to the next filter channel

      out_ptr = out_channel_start_ptr;  // reset output pointer for the next input channel
    } while(in_ptr != in_limit);

    in_ptr = &inbuf->r_ptr[0];  // reset input pointer for the next filter channel

    out_channel_start_ptr += outbuf->num_rows*outbuf->num_columns;  // update output channel start pointer
  } while(out_channel_start_ptr != out_limit);

#ifdef DO_TIME_MEASUREMENTS
  // store the cycle count minus the 4 cycles needed to load the counter value itself
  cycles = DWT->CYCCNT - 4;
  // stop time measurement
  time = HAL_GetTick() - time;

  // print cycle & time information
  sprintf(img_msg_buffer, "\n\rconv2d_1:\n\r %" PRIu32 " cycles, %" PRIu32 " ms\n\r", cycles, time);
  USART_puts(img_msg_buffer);
#endif  // DO_TIME_MEASUREMENTS
}



void conv2D_3(uint8_t opcode, data_buffer *inbuf, data_buffer *outbuf) {
  /*
   * Computes the convolution with kernels that have k = 3 elements.
   *
   * opcode: Note that not every element in a given buffer has to be computed for every input fragment (due to the
   *      pipelining-like nature of the buffer system).
   *      opcode == 1 --> compute all columns
   *      opcode == 2 --> can be used after opcode == 1 was called in order to force some columns to zero at the BEGINNING of a given input
   *      opcode == 3 -->  can be used after opcode == 1 was called in order to force some columns to zero at the END of a given input
   */

#ifdef DO_TIME_MEASUREMENTS
  // start time measurement
  time = HAL_GetTick();

  // reset cycle counter
  DWT->CYCCNT = 0;
#endif  // DO_TIME_MEASUREMENTS

  float *in_ptr = &inbuf->r_ptr[0];

  float *out_ptr;
  float *out_channel_start_ptr = &outbuf->w_ptr[0];

  filter_data *fd = &outbuf->fd;
  const uint8_t *bias_ptr = &fd->bias[0];
  const uint8_t *kernel_ptr = &fd->kernel[0];

  uint32_t outbuf_size_per_channel = outbuf->num_columns*outbuf->num_rows;
  uint32_t outbuf_size = outbuf->num_channels*outbuf_size_per_channel;
  uint32_t inbuf_size = inbuf->num_channels*inbuf->num_rows*inbuf->num_columns;

  float *out_limit = &outbuf->w_ptr[outbuf_size];
  float *out_channel_limit_ptr = &outbuf->w_ptr[outbuf_size_per_channel];
  float *in_limit = &inbuf->r_ptr[inbuf_size];
  float *in_row_limit;
  float *in_col_limit;

  float filter0;
  float filter1;
  float filter2;
  float filter3;
  float filter4;
  float filter5;
  float filter6;
  float filter7;
  float filter8;

  float input0;
  float input1;
  float input2;
  float input3;
  float input4;
  float input5;
  float input6;
  float input7;
  float input8;


  // load the desired weights
  update_LuT(fd);

  uint32_t invariant0 = outbuf->num_rows*outbuf->num_columns;
  uint32_t invariant1 = (inbuf->num_rows - 2)*inbuf->num_columns;
  uint32_t invariant2 = outbuf->num_non_buffer_columns*fd->stride;
  uint32_t invariant3 = outbuf->num_columns - outbuf->num_non_buffer_columns;
  uint32_t invariant4 = inbuf->num_columns - outbuf->num_non_buffer_columns*fd->stride + (fd->stride - 1)*inbuf->num_columns;



  if(opcode == 2) { // set the necessary buffer columns to zero
    float *w_working_ptr = &outbuf->w_ptr[0];

    for(uint32_t channel=0; channel<outbuf->num_channels; channel++) {
      for(uint32_t row=0; row<outbuf->num_rows; row++) {
        for(uint32_t column=0; column < outbuf->opcode2_num_hop_columns; column++) {
          w_working_ptr[column] = 0;
        }
        w_working_ptr += outbuf->num_columns;
      }
    }
  }

  else if(opcode == 1) {  // compute all buffer columns
      // compute convolution
      do {  // loop over output channels
        out_ptr = out_channel_start_ptr;  // set the out_ptr to the correct place for the current output channel


        do {  // loop over rows of the current output channel
          uint32_t col = 0;
          do {
            out_ptr[col] = LuT_bias[*bias_ptr];
            col++;
          } while(col < outbuf->num_non_buffer_columns);

          out_ptr += outbuf->num_columns;
        } while(out_ptr != out_channel_limit_ptr);

        bias_ptr++; // increment the bias pointer to the next filter channel
        out_channel_limit_ptr += invariant0;  // increment the bias pointer's limiter to the end of the next output channel

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        out_ptr = out_channel_start_ptr;  // reset output pointer for MAC operation

        do {  // loop over input channels
          in_row_limit = &in_ptr[invariant1]; // determine the row limit for the current input channel

          // store the current filter channel (in CPU registers)
          filter0 = LuT_kernel[kernel_ptr[0]];
          filter1 = LuT_kernel[kernel_ptr[1]];
          filter2 = LuT_kernel[kernel_ptr[2]];
          filter3 = LuT_kernel[kernel_ptr[3]];
          filter4 = LuT_kernel[kernel_ptr[4]];
          filter5 = LuT_kernel[kernel_ptr[5]];
          filter6 = LuT_kernel[kernel_ptr[6]];
          filter7 = LuT_kernel[kernel_ptr[7]];
          filter8 = LuT_kernel[kernel_ptr[8]];



          if(fd->stride == 1) {
            // simulate zero-padding
            in_col_limit = &in_ptr[outbuf->num_non_buffer_columns*fd->stride];
            do { // loop over input columns of the first input row in the current input channel
              input0 = in_ptr[0];
              input1 = in_ptr[1];
              input2 = in_ptr[2];
              input3 = in_ptr[inbuf->num_columns];
              input4 = in_ptr[inbuf->num_columns + 1];
              input5 = in_ptr[inbuf->num_columns + 2];

              *out_ptr += input0*filter3;
              *out_ptr += input1*filter4;
              *out_ptr += input2*filter5;
              *out_ptr += input3*filter6;
              *out_ptr += input4*filter7;
              *out_ptr += input5*filter8;
//              *out_ptr = 110;

              in_ptr++;
              out_ptr++;

            } while(in_ptr != in_col_limit);

            out_ptr += (outbuf->num_columns - outbuf->num_non_buffer_columns); // to hop over the first actual buffer column!

            in_ptr -= outbuf->num_non_buffer_columns; // reset input pointer
          }








          if(fd->stride == 1) {
            do {  // loop over input rows in the current input channel
              in_col_limit = &in_ptr[invariant2]; // determine the column limit for the current input row

              float* in_ptr1 = &in_ptr[inbuf->num_columns];
              float* in_ptr2 = &in_ptr1[inbuf->num_columns];

              do {  // loop over input columns in the current input column
                // compute MAC operation
                input0 = in_ptr[0];
                input1 = in_ptr[1];
                input2 = in_ptr[2];
                input3 = in_ptr1[0];
                input4 = in_ptr1[1];
                input5 = in_ptr1[2];
                input6 = in_ptr2[0];
                input7 = in_ptr2[1];
                input8 = in_ptr2[2];

                *out_ptr += input0*filter0;
                *out_ptr += input1*filter1;
                *out_ptr += input2*filter2;
                *out_ptr += input3*filter3;
                *out_ptr += input4*filter4;
                *out_ptr += input5*filter5;
                *out_ptr += input6*filter6;
                *out_ptr += input7*filter7;
                *out_ptr += input8*filter8;
  //              *out_ptr = 111;

                // update input/output pointers
                out_ptr++;
                in_ptr += 1;
                in_ptr1 += 1;
                in_ptr2 += 1;
              } while(in_ptr != in_col_limit);

              out_ptr += invariant3; // to hop over the actual buffer columns!

              in_ptr += invariant4; // increment input pointer to the next row
            } while(in_ptr != in_row_limit);
          }
          else if(fd->stride == 2) {
            do {  // loop over input rows in the current input channel
              in_col_limit = &in_ptr[invariant2]; // determine the column limit for the current input row

              float* in_ptr1 = &in_ptr[inbuf->num_columns];
              float* in_ptr2 = &in_ptr1[inbuf->num_columns];

              do {  // loop over input columns in the current input column
                // compute MAC operation
                input0 = in_ptr[0];
                input1 = in_ptr[1];
                input2 = in_ptr[2];
                input3 = in_ptr1[0];
                input4 = in_ptr1[1];
                input5 = in_ptr1[2];
                input6 = in_ptr2[0];
                input7 = in_ptr2[1];
                input8 = in_ptr2[2];

                *out_ptr += input0*filter0;
                *out_ptr += input1*filter1;
                *out_ptr += input2*filter2;
                *out_ptr += input3*filter3;
                *out_ptr += input4*filter4;
                *out_ptr += input5*filter5;
                *out_ptr += input6*filter6;
                *out_ptr += input7*filter7;
                *out_ptr += input8*filter8;
  //              *out_ptr = 111;

                // update input/output pointers
                out_ptr++;
                in_ptr += 2;
                in_ptr1 += 2;
                in_ptr2 += 2;
              } while(in_ptr != in_col_limit);

              out_ptr += invariant3; // to hop over the actual buffer columns!

              in_ptr += invariant4; // increment input pointer to the next row
            } while(in_ptr != in_row_limit);
          }
          else if(fd->stride == 4){
            do {  // loop over input rows in the current input channel
              in_col_limit = &in_ptr[invariant2]; // determine the column limit for the current input row

              float* in_ptr1 = &in_ptr[inbuf->num_columns];
              float* in_ptr2 = &in_ptr1[inbuf->num_columns];

              do {  // loop over input columns in the current input column
                // compute MAC operation
                input0 = in_ptr[0];
                input1 = in_ptr[1];
                input2 = in_ptr[2];
                input3 = in_ptr1[0];
                input4 = in_ptr1[1];
                input5 = in_ptr1[2];
                input6 = in_ptr2[0];
                input7 = in_ptr2[1];
                input8 = in_ptr2[2];

                *out_ptr += input0*filter0;
                *out_ptr += input1*filter1;
                *out_ptr += input2*filter2;
                *out_ptr += input3*filter3;
                *out_ptr += input4*filter4;
                *out_ptr += input5*filter5;
                *out_ptr += input6*filter6;
                *out_ptr += input7*filter7;
                *out_ptr += input8*filter8;
  //              *out_ptr = 111;

                // update input/output pointers
                out_ptr++;
                in_ptr += 4;
                in_ptr1 += 4;
                in_ptr2 += 4;
              } while(in_ptr != in_col_limit);

              out_ptr += invariant3; // to hop over the actual buffer columns!

              in_ptr += invariant4; // increment input pointer to the next row
            } while(in_ptr != in_row_limit);
          }
          else {
            // some eror!
          }












          // simulate zero-padding
          in_col_limit = &in_ptr[outbuf->num_non_buffer_columns*fd->stride];
          do { // loop over input columns of the second last input row of the current input channel
            input0 = in_ptr[0];
            input1 = in_ptr[1];
            input2 = in_ptr[2];
            input3 = in_ptr[inbuf->num_columns];
            input4 = in_ptr[inbuf->num_columns + 1];
            input5 = in_ptr[inbuf->num_columns + 2];

            *out_ptr += input0*filter0;
            *out_ptr += input1*filter1;
            *out_ptr += input2*filter2;
            *out_ptr += input3*filter3;
            *out_ptr += input4*filter4;
            *out_ptr += input5*filter5;
//            *out_ptr = 110;

            in_ptr += fd->stride;
            out_ptr++;

          } while(in_ptr != in_col_limit);

          out_ptr += (outbuf->num_columns - outbuf->num_non_buffer_columns); // to hop over the actual buffer columns!

          in_ptr += (inbuf->num_columns - outbuf->num_non_buffer_columns*fd->stride) + inbuf->num_columns;  // increment input pointer to the next input channel







          kernel_ptr += 9;  // increment kernel pointer to the next filter channel

          out_ptr = out_channel_start_ptr;  // reset output pointer for the next input channel
        } while(in_ptr != in_limit);


  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        in_ptr = &inbuf->r_ptr[0];  // reset input pointer for the next filter channel

        out_channel_start_ptr += outbuf->num_rows*outbuf->num_columns;  // update output channel start pointer
      } while(out_channel_start_ptr != out_limit);
  }
  else if(opcode == 3) {  // the last fraction of the input has been received, i.e. the img_fraction_counter was set to zero --> force the necessary buffer columns to zero
    float *w_working_ptr = &outbuf->w_ptr[0];
    w_working_ptr += outbuf->opcode3_num_hop_columns;

    uint8_t num_zero_columns = outbuf->num_non_buffer_columns - outbuf->opcode3_num_hop_columns;

    for(uint32_t channel=0; channel<outbuf->num_channels; channel++) {
      for(uint32_t row=0; row<outbuf->num_rows; row++) {
        for(uint32_t column=0; column < num_zero_columns; column++) {
          w_working_ptr[column] = 0;
        }
        w_working_ptr += outbuf->num_columns;
      }
    }
  }
  else if(opcode == 0) {
    float *w_working_ptr = &outbuf->w_ptr[0];

    for(uint32_t channel=0; channel<outbuf->num_channels; channel++) {
      for(uint32_t row=0; row<outbuf->num_rows; row++) {
        for(uint32_t column=0; column < outbuf->num_non_buffer_columns; column++) {
          w_working_ptr[column] = 0;
        }
        w_working_ptr += outbuf->num_columns;
      }
    }
  }
  else {
    // USART_puts("OPERATION CODE NOT KNOWN YET!\n\r");
  }

#ifdef DO_TIME_MEASUREMENTS
  // store the cycle count minus the 4 cycles needed to load the counter value itself
  cycles = DWT->CYCCNT - 4;
  // stop time measurement
  time = HAL_GetTick() - time;

  // print cycle & time information
  sprintf(img_msg_buffer, "\n\rconv2d_3:\n\r %" PRIu32 " cycles, %" PRIu32 " ms\n\r", cycles, time);
  USART_puts(img_msg_buffer);
#endif  // DO_TIME_MEASUREMENTS
}

