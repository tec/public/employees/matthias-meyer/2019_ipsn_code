/*
 * Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

/*
 * all RTOS related initialization
 *
 * FreeRTOS style guide:
 * https://www.freertos.org/FreeRTOS-Coding-Standard-and-Style-Guide.html
 */

/* Includes ------------------------------------------------------------------*/
#include "rtos_init.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#if RTOS_DEBUG_PRINT_ON
#define RTOS_PRINT(str)           UART_Println(str)
#else  /* RTOS_DEBUG_PRINT_ON */
#define RTOS_PRINT(str)
#endif /* RTOS_DEBUG_PRINT_ON */

/* Private variables ---------------------------------------------------------*/
static QueueHandle_t xQueuePS  = NULL;
static QueueHandle_t xQueueCNN = NULL;
static QueueHandle_t xQueueDT  = NULL;

static uint32_t ulTotalIdleTime = 0;

/* Private function prototypes -----------------------------------------------*/
static void prvSetupHardware(void);
static void prvPreprocessTask(void *pvParameters);
static void prvCNNTask(void *pvParameters);
static void prvTransmissionTask(void *pvParameters);

/* Private functions ---------------------------------------------------------*/

void RTOS_Init(void)
{
  RTOS_PRINT("Initializing FreeRTOS...");

  /* hardware setup required for the RTOS */
  prvSetupHardware();

  /* create the queues */
  xQueuePS  = xQueueCreate(PS_QUEUE_SIZE, sizeof(uint32_t));
  xQueueCNN = xQueueCreate(1, sizeof(uint32_t));
  xQueueDT  = xQueueCreate(1, sizeof(CNN_Output_t));
  CHECK_FOR_NULL_ERROR(xQueuePS && xQueueCNN && xQueueDT);

  /* create all required tasks */
  CHECK_FOR_RTOS_ERROR(xTaskCreate(prvPreprocessTask,
                                   "Preprocess",
                                   configMINIMAL_STACK_SIZE,
                                   0,
                                   tskIDLE_PRIORITY + 3,
                                   NULL));
  CHECK_FOR_RTOS_ERROR(xTaskCreate(prvCNNTask,
                                   "CNN",
                                   4 * configMINIMAL_STACK_SIZE,
                                   0,
                                   tskIDLE_PRIORITY + 2,
                                   NULL));
  CHECK_FOR_RTOS_ERROR(xTaskCreate(prvTransmissionTask,
                                   "Transmission",
                                   2 * configMINIMAL_STACK_SIZE,    /* requires more stack due to DBG_PRINT! */
                                   0,
                                   tskIDLE_PRIORITY + 1,
                                   NULL));

  /* done! */
  RTOS_PRINT("Tasks created");
}
/*----------------------------------------------------------------------------*/

void RTOS_Start(void)
{
  RTOS_PRINT("Starting FreeRTOS scheduler...");

  /* start FreeRTOS scheduler */
  vTaskStartScheduler();

  ERROR();
}
/*----------------------------------------------------------------------------*/

float RTOS_GetCPUDutyCycle(void)
{
  return 1.0f - (float)ulTotalIdleTime / (float)CURRENT_TIME();
}
/*----------------------------------------------------------------------------*/

static void prvSetupHardware(void)
{
  /* Ensure all priority bits are assigned as preemption priority bits. */
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* Configure the Systick interrupt time */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / configTICK_RATE_HZ);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}
/*----------------------------------------------------------------------------*/

/* process spectrogram (PS) task */
static void prvPreprocessTask(void *pvParameters)
{
  static float32_t afInBuffer[PS_INPUT_SAMPLES];
  static float32_t afOutBuffer[CNN_INPUT_ELEM * 2];   /* double buffer */
  static uint32_t ulOffset = 0;
  uint32_t ulStartAddress;

  LED_ON(PS_TASK_LED);
  RTOS_PRINT("PS task started");

  while (1)
  {
    /* wait until something arrives in the queue (block indefinitely) */
    LED_OFF(PS_TASK_LED);
    xQueueReceive(xQueuePS, &ulStartAddress, portMAX_DELAY);
    LED_ON(PS_TASK_LED);

    /* for non continuous waveforms: skip every INPUT_BUFFERS_PER_WAVEFORM buffer */
  #if PS_INPUT_WAVEFORM_LENGTH
    #define INPUT_BUFFERS_PER_WAVEFORM   ((PS_INPUT_WAVEFORM_LENGTH - PS_INPUT_SAMPLES) * PS_INPUT_BYTES_PER_SAMPLE / PS_INPUT_BUFFER_SHIFT + 2)
    static uint32_t skip_count = 0;
    skip_count++;
    if ((skip_count % INPUT_BUFFERS_PER_WAVEFORM) == 0)
    {
      continue;
    }
  #endif /* PS_INPUT_WAVEFORM_LENGTH */

    /* copy data into local buffer */
    uint32_t i;
    for (i = 0; i < PS_INPUT_SAMPLES; i++)
    {
      /* convert 32-bit integer values to float */
      afInBuffer[i]   = (float)(*(int32_t*)ulStartAddress);
      ulStartAddress += PS_INPUT_BYTES_PER_SAMPLE;
  #if !USE_PUSHBUTTON_TRIGGER
      if (ulStartAddress >= (uint32_t)dmaRxBuffer + (2 * AQ_BUFFER_SIZE)) {
        ulStartAddress = (uint32_t)dmaRxBuffer;
      }
  #endif /* !USE_PUSHBUTTON_TRIGGER */
    }

    /* do the preprocessing on the samples */
    calulate_ps(afInBuffer, &afOutBuffer[ulOffset]);

    ulOffset += PS_OUTPUT_ELEM;

    /* once we have gathered 4 buffers, notify CNN task */
    if ((ulOffset == CNN_INPUT_ELEM) || (ulOffset == (2 * CNN_INPUT_ELEM)))
    {
      ulStartAddress = (uint32_t)&afOutBuffer[ulOffset - CNN_INPUT_ELEM];
      if (xQueueSend(xQueueCNN, &ulStartAddress, 0U) != pdPASS)
      {
        WARNING("WARNING: CNN Queue full");
      }
    }
    ulOffset = ulOffset % (CNN_INPUT_ELEM * 2);

    RTOS_PRINT("PS task executed");
  }
}
/*----------------------------------------------------------------------------*/

/* convolutional neural network (CNN) task */
static void prvCNNTask(void *pvParameters)
{
  static CNN_Output_t cnnOutput;
  uint32_t ulStartAddress;

  LED_ON(CNN_TASK_LED);
  RTOS_PRINT("CNN task started");

  while (1)
  {
    /* wait until something arrives in the queue (block indefinitely) */
    LED_OFF(CNN_TASK_LED);
    xQueueReceive(xQueueCNN, &ulStartAddress, portMAX_DELAY);
    LED_ON(CNN_TASK_LED);

    /* apply CNN to the input data */
    uint_fast8_t valid = do_inference((float*)ulStartAddress, &cnnOutput);

    /* notify the DT task */
    if (valid)
    {
      if (xQueueSend(xQueueDT, &cnnOutput, portMAX_DELAY) != pdPASS)
      {
        WARNING("WARNING: DT Queue full");
      }
    }

    RTOS_PRINT("CNN task executed");
  }
}
/*----------------------------------------------------------------------------*/

/* decision and result transmission (DT) task */
static void prvTransmissionTask(void *pvParameters)
{
  static CNN_Output_t result;

  LED_ON(DT_TASK_LED);
  RTOS_PRINT("DT task started");

  while (1)
  {
    /* wait until something arrives in the queue (block indefinitely) */
    LED_OFF(DT_TASK_LED);
    xQueueReceive(xQueueDT, &result, portMAX_DELAY);
    LED_ON(DT_TASK_LED);

#if RTOS_DEBUG_PRINT_ON
    DBG_PRINT("DT executed (decision: %lu, value: %f)\r\n", result.decision[0], result.value[0]);
    DBG_PRINT("CPU duty cycle: %.2f%%\r\n", RTOS_GetCPUDutyCycle() * 100.0f);
#else  /* RTOS_DEBUG_PRINT_ON */
    DBG_PRINT("%lu,%f,%.2f\r\n", result.decision[0], result.value[0], RTOS_GetCPUDutyCycle() * 100.0f);
#endif /* RTOS_DEBUG_PRINT_ON */
  }
}
/*----------------------------------------------------------------------------*/

/* NOTE: the following function is executed within an interrupt service routine! */
void vDMATransferCompleteCallback(uint32_t halfComplete)
{
  static uint32_t ulAddressOffset = 0;
  uint32_t ulStartAddress;
  uint32_t ulLimit;

  LED_ON(DMA_INT_LED);
  LED_ON(ACT_LED);

  if (halfComplete)
  {
    /* first half of the buffer is now ready to be processed */
    if (ulAddressOffset > AQ_BUFFER_SIZE)
    {
      ulLimit = (2 * AQ_BUFFER_SIZE);
      while (ulAddressOffset < ulLimit)
      {
        /* send a notification to the PS task */
        ulStartAddress = (uint32_t)dmaRxBuffer + ulAddressOffset;
        if (xQueueSendFromISR(xQueuePS, &ulStartAddress, 0U) != pdPASS)
        {
          WARNING("WARNING: PS Queue full");
        }
        ulAddressOffset += PS_INPUT_BUFFER_SHIFT;
      }
      ulAddressOffset = ulAddressOffset % (2 * AQ_BUFFER_SIZE);
    }
    ulLimit = AQ_BUFFER_SIZE - PS_INPUT_BUFFER_SIZE;

  } else
  {
    /* 2nd half of the buffer is now ready to be processed */
    /* send a notification to the PS task */
    ulLimit = (2 * AQ_BUFFER_SIZE) - PS_INPUT_BUFFER_SIZE;
  }
  /* send a notification to the PS task */
  while (ulAddressOffset <= ulLimit)
  {
    ulStartAddress = (uint32_t)dmaRxBuffer + ulAddressOffset;
    if (xQueueSendFromISR(xQueuePS, &ulStartAddress, 0U) != pdPASS)
    {
      WARNING("WARNING: PS Queue full");
    }
    ulAddressOffset += PS_INPUT_BUFFER_SHIFT;
  }
  ulAddressOffset = ulAddressOffset % (2 * AQ_BUFFER_SIZE);

  LED_OFF(DMA_INT_LED);
}
/*----------------------------------------------------------------------------*/

/* to bypass the UART DMA transfer, for debugging purposes only */
void vSendSamplesToPSTask(const void* pSamplesBuffer, uint32_t bufferSize)
{
  static uint32_t ulAddressOffset = 0;
  uint32_t ulStartAddress;

  while ((ulAddressOffset + (PS_INPUT_SAMPLES * PS_INPUT_BYTES_PER_SAMPLE)) <= bufferSize)
  {
    /* write to queue until full */
    ulStartAddress = (uint32_t)pSamplesBuffer + ulAddressOffset;
    if (xQueueSendFromISR(xQueuePS, &ulStartAddress, 0U) != pdPASS)
    {
      break;    /* stop */
    }
    ulAddressOffset += PS_INPUT_BUFFER_SHIFT;
  }
}
/*----------------------------------------------------------------------------*/

void vApplicationMallocFailedHook(void)
{
  taskDISABLE_INTERRUPTS();
  ERROR();
  while (1);
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook(void)
{
  LED_ON(RTOS_IDLE_LED);
  LED_OFF(ACT_LED);

  uint32_t idleStart = CURRENT_TIME();

  /* enter sleep mode (CPU off, clocks and peripherals stay on) */
  HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);

  /* resume from sleep mode */
  /* nothing to do, all register and memory states are preserved in sleep mode */

  ulTotalIdleTime += CURRENT_TIME() - idleStart;

  LED_ON(ACT_LED);
  LED_OFF(RTOS_IDLE_LED);
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
  (void)pcTaskName;
  (void)pxTask;
  taskDISABLE_INTERRUPTS();
  ERROR();
  while (1);
}
/*-----------------------------------------------------------*/

/* currently not used */
void vApplicationTickHook(void)
{
  LED_ON(RTOS_TICK_LED);
  LED_ON(ACT_LED);
  LED_OFF(RTOS_TICK_LED);
}
/*-----------------------------------------------------------*/
