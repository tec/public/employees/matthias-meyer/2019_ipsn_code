/*
 * Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

/* STM32L4 initialization code */

/* Includes ------------------------------------------------------------------*/
#include "stm32l4_init.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define UART_WRITE_CHAR(c)            { \
          while (!(UARTHandle.Instance->ISR & UART_FLAG_TXE)); \
          UARTHandle.Instance->TDR = ((c) & (uint8_t)0xFFU); \
        }
#define UART_WRITE_NEWLINE()          { UART_WRITE_CHAR('\r'); \
                                        UART_WRITE_CHAR('\n'); }

/* Private macro -------------------------------------------------------------*/
/* Global variables ----------------------------------------------------------*/
char debugPrintBuffer[DBG_PRINT_BUFFER_SIZE];

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef UARTHandle;
DMA_HandleTypeDef  DMAHandle;
TIM_HandleTypeDef  TIMHandle;

GPIO_TypeDef* LED_PORT[NUM_LEDS] = { GPIOC,        /* LED1 */
                                     GPIOB,        /* LED2 */
                                     GPIOB,        /* LED3 */
                                     GPIOA,        /* LED4 */
                                     GPIOC,        /* LED5 */
                                     GPIOC,        /* LED6 */
                                     GPIOC,        /* LED7 */
                                     GPIOC,        /* LED8 */
                                     GPIOC         /* LED9 */
};
const uint16_t LED_PIN[NUM_LEDS] = { GPIO_PIN_7,   /* LED1 */
                                     GPIO_PIN_7,   /* LED2 */
                                     GPIO_PIN_14,  /* LED3 */
                                     GPIO_PIN_3,   /* LED4 */
                                     GPIO_PIN_0,   /* LED5 */
                                     GPIO_PIN_3,   /* LED6 */
                                     GPIO_PIN_1,   /* LED7 */
                                     GPIO_PIN_4,   /* LED8 */
                                     GPIO_PIN_5    /* LED9 */
};

uint8_t dmaRxBuffer[DMA_UARTRX_BUFFER_SIZE];

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void UART_Init(USART_TypeDef* uart_module, uint32_t baudrate);
void DMA_Init_UARTRX(void);
void LED_Init(Led_t led);
void PushButton_Init(void);
void TIM2_Init(void);

/* Private functions ---------------------------------------------------------*/

void MCU_Init(void)
{
  /* configure basic features: flash prefetch, systick timer, NVIC, ... */
  HAL_Init();

  /* configure the system clock to 80 MHz */
  SystemClock_Config();

  /* enable clocks for all used GPIOs */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /* configure LEDs */
  uint32_t led;
  for (led = 0; led < NUM_LEDS; led++)
  {
    LED_Init(led);
  }
  LED_ON(STATUS_LED);

  /* configure UART */
  UART_Init(UART_MODULE, UART_BAUDRATE);

  /* configure 32-bit timer */
  TIM2_Init();

  /* initialize the User push-button in Interrupt mode */
  PushButton_Init();

  /* make sure interrupts are not yet enabled (need to initialize the RTOS first!) */
  __disable_irq();

  UART_Println("\r\nSTM32L4 initialized");
}
/*----------------------------------------------------------------------------*/

/* user defined hardware init, this function is called from within HAL_Init() */
void HAL_MspInit(void)
{
  /* enable SYSCFG and PWR clock */
  __HAL_RCC_SYSCFG_CLK_ENABLE();
  __HAL_RCC_PWR_CLK_ENABLE();
}
/*----------------------------------------------------------------------------*/

/**
  * System Clock Configuration
  * The system Clock is configured as follows:
  *   System Clock source            = PLL (MSI)
  *   SYSCLK(Hz)                     = 80000000
  *   HCLK(Hz)                       = 80000000
  *   AHB Prescaler                  = 1
  *   APB1 Prescaler                 = 1
  *   APB2 Prescaler                 = 1
  *   MSI Frequency(Hz)              = 4000000
  *   PLL_M                          = 1
  *   PLL_N                          = 40
  *   PLL_R                          = 2
  *   PLL_P                          = 7
  *   PLL_Q                          = 4
  *   Flash Latency(WS)              = 4
  */
void SystemClock_Config(void)
{
  /**
   * Notes on the CLOCK SYSTEM:
   * - clock sources:
   *   - LSI (internal low-speed RC oscillator): 32 kHz
   *   - LSE (external low-speed oscillator):    32768 Hz
   *   - HSE (external high-speed oscillator):   not mounted on NUCLEO board
   *   - HSI (internal RC oscillator):           16 MHz
   *   - MSI (internal RC oscillator):           0.1 - 48 MHz
   * - PLL clock sources: MSI, HSI or HSE
   * - SYSCLK clock sources: MSI, HSI, HSE or PLLCLK
   * - system core clock (AHB, DMA, CPU, memory): HCLK = SYSCLK / AHB prescaler
   * - peripheral clock (PCLK1 and PCLK2):        HCLK / APBx prescaler
   *   - PCLK1 is used for TIM2..7, LPUART, USART2..5, I2C1..4, LPTIM1..2, SWPMI, APB1 peripherals
   *   - PCLK2 is used for TIM1, TIM8, TIM15..17, USART1, APB2 peripherals
   *
   * for more details, consult stm32l496 reference manual, p.210
   */
  RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
  RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };

  /* set voltage to allow for up to 80MHz core clock */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* MSI is enabled after System reset, activate PLL with MSI as source */
  RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState            = RCC_MSI_ON;
  RCC_OscInitStruct.MSIClockRange       = RCC_MSIRANGE_6;
  RCC_OscInitStruct.MSICalibrationValue = RCC_MSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM            = 1;
  RCC_OscInitStruct.PLL.PLLN            = 40;
  RCC_OscInitStruct.PLL.PLLR            = 2;
  RCC_OscInitStruct.PLL.PLLQ            = 4;
  CHECK_FOR_HAL_ERROR(HAL_RCC_OscConfig(&RCC_OscInitStruct));

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
  RCC_ClkInitStruct.ClockType      = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;         /* AHB prescaler = 1 */
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;           /* APB1 prescaler = 1 */
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;           /* APB2 prescaler = 1 */
  CHECK_FOR_HAL_ERROR(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4));
}
/*----------------------------------------------------------------------------*/

void LED_Init(Led_t led)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /* configure the GPIO_LED pin */
  GPIO_InitStruct.Pin   = LED_PIN[led];
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;

  HAL_GPIO_Init(LED_PORT[led], &GPIO_InitStruct);
  HAL_GPIO_WritePin(LED_PORT[led], LED_PIN  [led], GPIO_PIN_RESET);
}
/*----------------------------------------------------------------------------*/

void PushButton_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct;

#if PUSH_BUTTON_INT_ENABLE
  /* configure button pin as input with external interrupt */
  GPIO_InitStruct.Pin  = GPIO_PIN_13;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* enable and set button EXTI Interrupt to the lowest priority */
  HAL_NVIC_SetPriority((IRQn_Type)EXTI15_10_IRQn, 0x0f, 0x00);
  HAL_NVIC_EnableIRQ((IRQn_Type)EXTI15_10_IRQn);

#else /* PUSH_BUTTON_INT_ENABLE */
  /* Configure Button pin as input */
  GPIO_InitStruct.Pin   = GPIO_PIN_13;
  GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

#endif /* PUSH_BUTTON_INT_ENABLE */
}
/*----------------------------------------------------------------------------*/

void UART_TX_ISR(UART_HandleTypeDef *handle)
{

}
/*----------------------------------------------------------------------------*/

void UART_RX_ISR(UART_HandleTypeDef *handle)
{
#if UART_ECHO_CHARS
  /* echo received character */
  uint8_t rcvd = handle->Instance->RDR;
  UART_WRITE_CHAR(rcvd);
#endif /* UART_ECHO_CHARS */
}
/*----------------------------------------------------------------------------*/

void UART_Init(USART_TypeDef* uart_module, uint32_t baudrate)
{
  UARTHandle.Instance            = uart_module;
  UARTHandle.Init.BaudRate       = baudrate;
  UARTHandle.Init.WordLength     = UART_WORDLENGTH_8B;
  UARTHandle.Init.StopBits       = UART_STOPBITS_1;
  UARTHandle.Init.Parity         = UART_PARITY_NONE;
  UARTHandle.Init.HwFlowCtl      = UART_HWCONTROL_NONE;
  UARTHandle.Init.Mode           = UART_MODE_TX_RX;
  UARTHandle.Init.OverSampling   = UART_OVERSAMPLING_16;
  CHECK_FOR_HAL_ERROR(HAL_UART_DeInit(&UARTHandle));
  CHECK_FOR_HAL_ERROR(HAL_UART_Init(&UARTHandle));
  UARTHandle.RxISR = UART_RX_ISR;
  UARTHandle.TxISR = UART_TX_ISR;

#if UART_USE_DMA_FOR_RX
  DMA_Init_UARTRX();
  HAL_UART_Receive_DMA(&UARTHandle, dmaRxBuffer, DMA_UARTRX_BUFFER_SIZE);
#else  /* UART_USE_DMA_FOR_RX */
  __HAL_UART_ENABLE_IT(&UARTHandle, UART_IT_RXNE);
#endif /* UART_USE_DMA_FOR_RX */
}
/*----------------------------------------------------------------------------*/

void UART_Print(const char* str)
{
  if (!str || !(UARTHandle.Instance->CR1 & USART_CR1_UE))
  {
    return;
  }
  while (*str)
  {
    UART_WRITE_CHAR(*str);
    str++;
  }
}
/*----------------------------------------------------------------------------*/

void UART_Println(const char* str)
{
  if (!str || !(UARTHandle.Instance->CR1 & USART_CR1_UE))
  {
    return;
  }
  while (*str)
  {
    UART_WRITE_CHAR(*str);
    str++;
  }
  UART_WRITE_NEWLINE();
}
/*----------------------------------------------------------------------------*/

void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
  GPIO_InitTypeDef GPIO_InitStruct;

  if (huart->Instance == LPUART1) {
    /* set UART clock source */
    __HAL_RCC_LPUART1_CONFIG(RCC_LPUART1CLKSOURCE_PCLK1);
    /* Enable GPIO TX/RX and LPUART clock */
    __HAL_RCC_LPUART1_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();
    /* TX GPIO pin configuration  */
    GPIO_InitStruct.Pin       = GPIO_PIN_7 | GPIO_PIN_8;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_LPUART1;
    HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  } else if (huart->Instance == USART2)
  {
    /* set UART clock source */
    __HAL_RCC_USART2_CONFIG(RCC_USART2CLKSOURCE_PCLK1);
    /* Enable GPIO TX/RX and LPUART clock */
    __HAL_RCC_USART2_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    /* TX GPIO pin configuration  */
    GPIO_InitStruct.Pin       = GPIO_PIN_5 | GPIO_PIN_6;
    GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull      = GPIO_PULLUP;
    GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

    /* NVIC config for UART */
    HAL_NVIC_SetPriority(USART2_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);

  } else
  {
    ERROR();
  }
}
/*----------------------------------------------------------------------------*/

void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
  if (huart->Instance == LPUART1)
  {
    __HAL_RCC_LPUART1_FORCE_RESET();
    __HAL_RCC_LPUART1_RELEASE_RESET();
    HAL_GPIO_DeInit(GPIOG, GPIO_PIN_7);
    HAL_GPIO_DeInit(GPIOG, GPIO_PIN_8);
    HAL_NVIC_DisableIRQ(LPUART1_IRQn);

  } else if (huart->Instance == USART2)
  {
    __HAL_RCC_USART2_FORCE_RESET();
    __HAL_RCC_USART2_RELEASE_RESET();
    HAL_GPIO_DeInit(GPIOD, GPIO_PIN_5);
    HAL_GPIO_DeInit(GPIOD, GPIO_PIN_6);
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  }

  if(huart->hdmarx != 0)
  {
    HAL_DMA_DeInit(huart->hdmarx);
    HAL_NVIC_DisableIRQ(DMA1_Channel6_IRQn);
  }
}
/*----------------------------------------------------------------------------*/

/* half transfer complete interrupt callback */
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef* handle)
{
#ifdef DMA_UARTRX_HT_CALLBACK
  DMA_UARTRX_HT_CALLBACK();

#else  /* DMA_UARTRX_HT_CALLBACK */
  /*uint32_t i = 0;
  UART_Print("half: ");
  while (i < DMA_UARTRX_BUFFER_SIZE / 2)
  {
    UART_WRITE_CHAR(dmaRxBuffer[i]);
    i++;
  }
  UART_WRITE_NEWLINE();*/
#endif /* DMA_UARTRX_HT_CALLBACK */
}
/*----------------------------------------------------------------------------*/

/* transfer complete interrupt callback */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* handle)
{
#ifdef DMA_UARTRX_TC_CALLBACK
  DMA_UARTRX_TC_CALLBACK();

#else  /* DMA_UARTRX_TC_CALLBACK */
  /*uint32_t i = DMA_UARTRX_BUFFER_SIZE / 2;
  UART_Print("complete: ");
  while (i < DMA_UARTRX_BUFFER_SIZE)
  {
    UART_WRITE_CHAR(dmaRxBuffer[i]);
    i++;
  }
  UART_WRITE_NEWLINE();*/
#endif /* DMA_UARTRX_TC_CALLBACK */
}
/*----------------------------------------------------------------------------*/

/* init DMA1 channel 6 to handle USART2_RX */
void DMA_Init_UARTRX(void)
{
  __HAL_RCC_DMA1_CLK_ENABLE();
  DMAHandle.Instance                 = DMA1_Channel6;
  DMAHandle.Init.Request             = DMA_REQUEST_2;
  DMAHandle.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  DMAHandle.Init.Mode                = DMA_CIRCULAR;
  DMAHandle.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  DMAHandle.Init.MemInc              = DMA_MINC_ENABLE;
  DMAHandle.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  DMAHandle.Init.PeriphInc           = DMA_PINC_DISABLE;
  DMAHandle.Init.Priority            = DMA_PRIORITY_HIGH;
  CHECK_FOR_HAL_ERROR(HAL_DMA_Init(&DMAHandle));

  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(&UARTHandle, hdmarx, DMAHandle);

  HAL_NVIC_SetPriority(DMA1_Channel6_IRQn, 6, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
}
/*----------------------------------------------------------------------------*/

/* initialize a 32-bit timer to run at 1MHz */
void TIM2_Init(void)
{
  /* default clock source is PCLK1 (= APB1 = HCLK) */
  __HAL_RCC_TIM2_CLK_ENABLE();

  TIMHandle.Instance               = TIM2;
  TIMHandle.Init.Period            = 0xffffffff; //10000 - 1;
  TIMHandle.Init.Prescaler         = (SystemCoreClock / 1000000) - 1;
  TIMHandle.Init.ClockDivision     = 0;
  TIMHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TIMHandle.Init.RepetitionCounter = 0;
  CHECK_FOR_HAL_ERROR(HAL_TIM_Base_Init(&TIMHandle));
  CHECK_FOR_HAL_ERROR(HAL_TIM_Base_Start(&TIMHandle));
}
/*----------------------------------------------------------------------------*/

void Error(const char* msg, const char* filename, unsigned int line)
{
  if (msg)
  {
    if (filename)
    {
      snprintf(debugPrintBuffer, DBG_PRINT_BUFFER_SIZE, "ERROR %s %u: %s\r\n", filename, line, msg);
    } else
    {
      snprintf(debugPrintBuffer, DBG_PRINT_BUFFER_SIZE, "ERROR: %s\r\n", msg);
    }
  } else if (filename)
  {
    snprintf(debugPrintBuffer, DBG_PRINT_BUFFER_SIZE, "ERROR %s %u\r\n", filename, line);
  } else
  {
    strcpy(debugPrintBuffer, "ERROR");
  }
  __disable_irq();
  while (1)
  {
    LED_TOGGLE(ERROR_LED);
    UART_Print(debugPrintBuffer);
    HAL_Delay(1000);
  }
}
/*----------------------------------------------------------------------------*/
