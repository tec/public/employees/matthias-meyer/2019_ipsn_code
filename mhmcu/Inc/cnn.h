/*
 * Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Timo Farei-Campagna, Matthias Meyer
 */

#ifndef __CNN_H
#define __CNN_H

#include "config.h"       /* must be included first */


#define INQ_B 8 // make sure that LUT_SIZE = pow(2, INQ_B - 1) + 1
#define LUT_SIZE 129


// dimensions for the temporary input buffer which has 2 columns less than the actual B_INPUT from below
#define TEMP_INPUT_NUM_CHANNELS 1
#define TEMP_INPUT_NUM_ROWS 4
#define TEMP_INPUT_NUM_COLUMNS 64
#define TEMP_IMG_SIZE TEMP_INPUT_NUM_CHANNELS*TEMP_INPUT_NUM_ROWS*TEMP_INPUT_NUM_COLUMNS

#define COMPLETE_T 24

// size of the buffers necessary for each layer and the input
#define B_INPUT_NUM_CHANNELS 1
#define B_INPUT_NUM_ROWS 64
#define B_INPUT_NUM_COLUMNS 6
#define B_INPUT_SIZE B_INPUT_NUM_CHANNELS*B_INPUT_NUM_ROWS*B_INPUT_NUM_COLUMNS
#define B_INPUT_SHIFT 4

#define B0_NUM_CHANNELS 32
#define B0_NUM_ROWS 64
#define B0_NUM_COLUMNS 5
#define B0_SIZE B0_NUM_CHANNELS*B0_NUM_ROWS*B0_NUM_COLUMNS
#define B0_SHIFT 4

#define B1_NUM_CHANNELS 32
#define B1_SIZE 4*32*B1_NUM_CHANNELS

#define B2_NUM_CHANNELS 32
#define B2_SIZE 4*32*B2_NUM_CHANNELS

#define B3_NUM_CHANNELS 32
#define B3_SIZE 3*16*B3_NUM_CHANNELS

#define B4_NUM_CHANNELS 32
#define B4_SIZE 1*16*B4_NUM_CHANNELS

#define B5_NUM_CHANNELS 32
#define B5_SIZE 1*16*B5_NUM_CHANNELS

#define NUM_LABELS 1
#define B6_SIZE 1*16*NUM_LABELS
#define F_AVG_SIZE COMPLETE_T/2/2 *1*NUM_LABELS // COMPLETE_T/2/2 = 400/4 = 100
#define T_AVG_SIZE 1*1*NUM_LABELS
#define B7_SIZE 1*1*NUM_LABELS


//  0.70545 1.00228 1.38831 1.44069 1.44069 1.44069 ....

extern float nn_outputs[NUM_LABELS];
extern uint8_t nn_labels[NUM_LABELS];


/*
 * Struct for a 3D filter tensor, a 1D bias tensor as well as the dimensions of the two.
 *
 * Note: Both tensors are internally stored as 1D arrays where the dimension variables guarantee a correct access to the data.
 *
 * Filter of a Conv2D layer:
 * In this case the dimension variables are used according to their names.
 *
 * Filter of a fully connected (Dense) layer:
 * In this case num_rows = num_columns = 1 while num_input_channels equals the number of input neurons (number of output values of the previous layer) and num_output_channels equals the number of output neurons (its own number of neurons).
 */
typedef struct
{
  const uint8_t *kernel;
  const uint8_t *bias;

  const float *n1;

  uint32_t num_input_channels;  // in case of a fully connected layer this is equivalent to the number of input neurons
  uint32_t num_output_channels; // in case of a conv2D layer this is equivalent to the number of filters (each having num_input_channels channels)
                  // in case of a fully connected layer this is equivalent to the number of output neurons
  uint32_t num_rows;        // not relevant for fully connected layers --> set to 1 (neutral element)
  uint32_t num_columns;     // not relevant for fully connected layers --> set to 1 (neutral element)

  uint8_t stride;

} filter_data;


typedef struct
{
  float *r_ptr;
  float *w_ptr;

  float *in_ptr;

  float label;

  uint32_t num_channels;
  uint32_t num_rows;
  uint32_t num_columns;
  uint32_t num_shift;
  uint32_t num_non_buffer_columns;  // number of columns that are actually computed inside the update function
  uint8_t opcode2_num_hop_columns;
  uint8_t opcode3_num_hop_columns;

  filter_data fd;
} data_buffer;


typedef struct
{
  uint32_t decision[NUM_LABELS];
  float   value[NUM_LABELS];
} CNN_Output_t;


/*
 * Function that is called after a new input fraction has been received.
 * returns if the cnn_output struct contains valid data
 */
uint8_t do_inference(float *input_buffer, CNN_Output_t *cnn_output);

/*
 * Function to compute the lookup table for a given layer.
 */
void update_LuT(filter_data *data);

// buffer update and shift functions
void input_update(uint8_t opcode);
void input_shift(void);
void conv2D_3(uint8_t opcode, data_buffer *inbuf, data_buffer *outbuf);
void shift(data_buffer *buf);
void relu(data_buffer *buf);
void conv2D_1(data_buffer *inbuf, data_buffer *outbuf);
void f_avg_update(data_buffer *inbuf, data_buffer *outbuf);
void t_avg_update(data_buffer *inbuf, data_buffer *outbuf);
void activation_softmax(data_buffer *buf);
void activation_sigmoid(data_buffer *buf);
uint32_t determine_label(data_buffer *buf);

void CNN_Init();

#endif /* __CNN_H */
