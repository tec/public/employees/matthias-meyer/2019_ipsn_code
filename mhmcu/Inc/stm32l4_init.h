/*
 * Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

#ifndef __STM32L4_INIT_H
#define __STM32L4_INIT_H

/* Includes ------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "config.h"


/* Global defines ------------------------------------------------------------*/

#define STATUS_LED                  LED1    // green
#define ACT_LED                     LED2    // blue
#define ERROR_LED                   LED3    // red


/* Global macros -------------------------------------------------------------*/

#define CHECK_FOR_HAL_ERROR(retval) if (retval != HAL_OK) { ERROR(); }
#define DBG_PRINT(...)              { \
                                      snprintf(debugPrintBuffer, DBG_PRINT_BUFFER_SIZE, __VA_ARGS__); \
                                      UART_Print(debugPrintBuffer); \
                                    }
#define ERROR()                     Error(0, __FILENAME__, __LINE__)
#define WARNING(msg)                { \
                                      LED_ON(ERROR_LED); \
                                      UART_Println(msg); \
                                    }

#ifndef Error_Handler
#define Error_Handler()             Error(0, __FILENAME__, __LINE__)
#endif /* Error_Handler */

#ifndef __FILENAME__
#define __FILENAME__                (strrchr(__FILE__, '/') ? \
                                     strrchr(__FILE__, '/') + 1 : __FILE__)
#endif /* __FILENAME__ */

#define LED_ON(led)                 HAL_GPIO_WritePin(LED_PORT[led], LED_PIN[led], GPIO_PIN_SET)
#define LED_OFF(led)                HAL_GPIO_WritePin(LED_PORT[led], LED_PIN[led], GPIO_PIN_RESET)
#define LED_TOGGLE(led)             HAL_GPIO_TogglePin(LED_PORT[led], LED_PIN[led])

/* get a 32-bit timestamp (elapsed time since startup in us) */
#define CURRENT_TIME()              __HAL_TIM_GET_COUNTER(&TIMHandle)


/* Global typedefs -----------------------------------------------------------*/

typedef enum
{
  LED1 = 0,
  LED_GREEN = LED1,
  LED2 = 1,
  LED_BLUE = LED2,
  LED3 = 2,
  LED_RED = LED3,
  LED4 = 3,
  LED_DBG1 = LED4,
  LED5 = 4,
  LED_DBG2 = LED5,
  LED6 = 5,
  LED_DBG3 = LED6,
  LED7 = 6,
  LED_DBG4 = LED7,
  LED8 = 7,
  LED_DBG5 = LED8,
  LED9 = 8,
  LED_DBG6 = LED9,
  NUM_LEDS
} Led_t;


/* Global variables ----------------------------------------------------------*/

extern uint8_t dmaRxBuffer[DMA_UARTRX_BUFFER_SIZE];
extern char debugPrintBuffer[DBG_PRINT_BUFFER_SIZE];
extern GPIO_TypeDef* LED_PORT[NUM_LEDS];
extern const uint16_t LED_PIN[NUM_LEDS];
extern TIM_HandleTypeDef TIMHandle;


/* Global functions ----------------------------------------------------------*/

void MCU_Init(void);
void UART_Print(const char* str);
void UART_Println(const char* str);
void Error(const char* msg, const char* filename, unsigned int line);


#endif /* __STM32L4_INIT_H */

