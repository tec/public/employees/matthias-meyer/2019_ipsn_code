/*
 * Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

#ifndef __MAIN_H
#define __MAIN_H


/* Config --------------------------------------------------------------------*/

/* Application config */
#define PS_INPUT_SAMPLES          1024                      /* samples per PS task execution (preprocessing) */
#define PS_INPUT_BYTES_PER_SAMPLE 4                         /* 32-bit integers */
#define PS_INPUT_BUFFER_SIZE      (PS_INPUT_SAMPLES * PS_INPUT_BYTES_PER_SAMPLE)  /* size of one buffer for the PS task (32-bit floats) */
#define PS_INPUT_BUFFER_SHIFT     (PS_INPUT_BUFFER_SIZE / 2)                      /* defines the overlap between two consecutive buffers */
#define PS_QUEUE_SIZE             (AQ_BUFFER_SIZE / PS_INPUT_BUFFER_SHIFT + 1)
#define PS_OUTPUT_ELEM            64
#define PS_INPUT_WAVEFORM_LENGTH  12800                     /* if != 0, the shifting will be skipped at the end of the waveform to avoid overlap (must be a multiple of #samples shifted) */

#define AQ_BUFFER_SIZE            (PS_INPUT_BUFFER_SIZE)    /* buffer size for sample acquisition (must be >= PS_INPUT_BUFFER_SIZE) */

#define CNN_INPUT_ELEM            (PS_OUTPUT_ELEM * 4)      /* number of input elements */
#define CNN_OUTPUT_ELEM           1
#define CNN_OUTPUT_BUFFER_SIZE    (sizeof(CNN_Output_t))

#define USE_PUSHBUTTON_TRIGGER    1                   /* either push button trigger to use predefined waveform or continuous UART sampling */
#define RTOS_DEBUG_PRINT_ON       0
#define COPY_TABLES_TO_SRAM       0                   /* to speed up execution, lookup tables can be copied from flash to SRAM */
#define USE_SPARSE_MATMULT        0                   /* use sparse matrix multiplication function for the preprocessing? */

/* FreeRTOS + debugging */
#define RTOS_TICK_LED             LED4                /* pin A0 on Nucleo board */
#define DMA_INT_LED               LED5                /* pin A1 on Nucleo board */
#define PS_TASK_LED               LED6                /* pin A2 on Nucleo board */
#define CNN_TASK_LED              LED7                /* pin A3 on Nucleo board */
#define DT_TASK_LED               LED8                /* pin A4 on Nucleo board */
#define RTOS_IDLE_LED             LED9                /* pin A5 on Nucleo board */

/* STM32L4 config */
#define UART_MODULE               USART2      /* PD5, PD6 */
#define UART_ECHO_CHARS           0           /* echo received chars back? */
#define UART_USE_DMA_FOR_RX       1
#define UART_BAUDRATE             115200
#define DBG_PRINT_BUFFER_SIZE     128
#define DMA_UARTRX_BUFFER_SIZE    (2 * AQ_BUFFER_SIZE)  /* circular buffer, don't change! */
#define PUSH_BUTTON_INT_ENABLE    1           /* enable push button interrupt */


/* Defines -------------------------------------------------------------------*/

#if COPY_TABLES_TO_SRAM
#define CONST_SRAM
#else
#define CONST_SRAM    const
#endif /* COPY_TABLES_TO_SRAM */


/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

#include "stm32l4_init.h"        /* init routines and drivers for STM32L4 MCU */
#include "rtos_init.h"
#include "preprocessing.h"
#include "cnn.h"

#endif /* __MAIN_H */

