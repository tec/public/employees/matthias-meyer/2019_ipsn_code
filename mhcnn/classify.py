"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import pandas as pd
import numpy as np
from aednn.datasets.MHdataset import image_dataset, microseismic_dataset
import aednn.models.mhcnn as image_model
from aednn.models.geocnn import geocnn as microseismic_model
from aednn.toolbox import *
import aednn.custom_metrics

import permasense
permasense.global_settings.get_global_settings()['permasense_vault_dir'] = '/home/perma/permasense_vault/'

from toolbox import * 

import keras
from keras import backend as K
from keras.models import load_model

import os
import glob 
import time 

modes = ['microseismic','image','ensemble']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpu", type=str,
                    help="use the given gpu")
parser.add_argument("-m", "--mode", type=str,
                    help="choose a mode from %s"%str(modes))
args = parser.parse_args()

if args.gpu:
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    GPU_ID = str(args.gpu)
if args.mode:
    if args.mode not in modes:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))
    mode = str(args.mode)
else:
    raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))


def binary_crossentropy_weighted(y_true, y_pred):
    return y_true*y_pred*0
custom_objects = {"binary_crossentropy_weighted": binary_crossentropy_weighted}

tmp_dir = get_tmp_dir()

image_labels = list(pd.read_csv('common/image_labels.csv')['image_labels'].values)
microseismic_labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)
os.makedirs('tmp/pred_segments/',exist_ok=True)

print(image_labels,microseismic_labels)

# dataframe_train = pd.read_csv('/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/annotations/dataset/train_all.csv')
if mode == 'microseismic': 
    dataframe = pd.read_csv('tmp/full_microseismic.csv')
    labels = microseismic_labels
elif mode == 'image':
    dataframe = pd.read_csv('tmp/full_image.csv')
    labels = image_labels

# dataframe = dataframe_train.append(dataframe_test)

dataframe.sort_values('start_time',inplace=True)
 
batch_size = 360
number_of_batches = int( (len(dataframe) + batch_size) // batch_size )

test_params = {}
# test_params['dataframe'] = dataframe.iloc[0:2]
# dataset_test = dataset_class(tmp_dir,name='dataset_classify',params=test_params,labels=image_labels, restore_from_disk=False,verbose=False)
# X_class, y_class = dataset_test.get_data()

print('Loading model')
microseismic_net = load_model('results/microseismic/test/keras.netbest',custom_objects=custom_objects)
image_net = load_model('results/image/test/keras.netbest',custom_objects=custom_objects)

# Make sure that normalization and dropout are set to test mode
K.set_learning_phase(0)


for i in range(number_of_batches):
    continue
    ts = dataframe.iloc[i*batch_size:(i+1)*batch_size]
    test_params['dataframe'] = ts
    start_time = time.time()

    if mode == 'microseismic' or mode == 'ensemble': 
        print(test_params['dataframe'].iloc[0]['start_time'])

        dataset_test = microseismic_class(tmp_dir,name='microseismic_classify',params=test_params,labels=microseismic_labels, restore_from_disk=False,verbose=False)
        X_class, y_class = dataset_test.get_data()

        X_class = microseismic_model.preprocessing(X_class[:,:512,...].astype(K.floatx()))
        y_pred = microseismic_net.predict(X_class,batch_size=120,verbose=0)

        y_pred = threshold(y_pred,0.35)
        y_pred_ms = y_pred

        ts['microseismic_labels_pred'] = "[]"
        for j in range(y_pred.shape[0]):
            if y_pred[j,0] == 1:
                ts.loc[ts.index[j],'microseismic_labels_pred'] = "['mountaineer']"
    if mode == 'image' or mode == 'ensemble': 
        dataset_test = image_dataset(tmp_dir,name='image_classify',params=test_params,labels=image_labels, restore_from_disk=False,verbose=False)
        X_class, y_class = dataset_test.get_data()
        print(test_params['dataframe'].iloc[0]['start_time'], X_class.shape)

        X_class = image_model.preprocessing(X_class.astype(K.floatx()))
        y_pred = image_net.predict(X_class,batch_size=128,verbose=0)

        y_pred = threshold(y_pred,0.5)
        y_pred = y_pred[:,4:5]
        y_pred_img = y_pred

        ts['image_labels_pred'] = "[]"
        for j in range(y_pred.shape[0]):
            if y_pred[j,0] == 1:
                ts.loc[ts.index[j],'image_labels_pred'] = "['mountaineer']"


    if mode == 'ensemble':
        y_pred = np.logical_or(y_pred_img,y_pred_ms)

    print(time.time()-start_time)

    ts['labels_pred'] = "[]"
    for j in range(y_pred.shape[0]):
        if y_pred[j,0] == 1:
            ts.loc[ts.index[j],'labels_pred'] = "['mountaineer']"

    ts.to_csv('tmp/pred_segments/%d.csv'%i)
    
# this is not the best solution to merge the csv.
# they need to be manually edited afterwards.
# 1. Open the resulting file
# 2. Copy the first line
# 3. Search an replace the first line with nothing such that every line which is equal to the first line gets removed
# 4. Reinsert the first line if you have deleted it as well
# 5. Save file
filenames = glob.glob('tmp/pred_segments/*.csv')
with open("tmp/pred_segments.csv", "wb") as outfile:
    for f in filenames:
        with open(f, "rb") as infile:
            outfile.write(infile.read())
    
