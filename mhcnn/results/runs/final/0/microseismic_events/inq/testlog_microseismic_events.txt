Loading microseismic model
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input (InputLayer)           (None, 64, 24, 1)         0         
_________________________________________________________________
block1_conv1 (INQ_Conv2D)    (None, 64, 24, 32)        962       
_________________________________________________________________
activation_1 (Activation)    (None, 64, 24, 32)        0         
_________________________________________________________________
block1_conv2_stride2 (INQ_Co (None, 32, 12, 32)        27746     
_________________________________________________________________
activation_2 (Activation)    (None, 32, 12, 32)        0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 32, 12, 32)        0         
_________________________________________________________________
block2_conv3 (INQ_Conv2D)    (None, 32, 12, 32)        27746     
_________________________________________________________________
activation_3 (Activation)    (None, 32, 12, 32)        0         
_________________________________________________________________
block2_conv4_stride2 (INQ_Co (None, 16, 6, 32)         27746     
_________________________________________________________________
activation_4 (Activation)    (None, 16, 6, 32)         0         
_________________________________________________________________
dropout_2 (Dropout)          (None, 16, 6, 32)         0         
_________________________________________________________________
blockC_conv1 (INQ_Conv2D)    (None, 16, 6, 32)         27746     
_________________________________________________________________
activation_5 (Activation)    (None, 16, 6, 32)         0         
_________________________________________________________________
blockC_conv2 (INQ_Conv2D)    (None, 16, 6, 32)         3170      
_________________________________________________________________
activation_6 (Activation)    (None, 16, 6, 32)         0         
_________________________________________________________________
dropout_3 (Dropout)          (None, 16, 6, 32)         0         
_________________________________________________________________
blockC_conv3 (INQ_Conv2D)    (None, 16, 6, 1)          101       
_________________________________________________________________
activation_7 (Activation)    (None, 16, 6, 1)          0         
_________________________________________________________________
average_pooling2d_1 (Average (None, 1, 6, 1)           0         
_________________________________________________________________
average_pooling2d_2 (Average (None, 1, 1, 1)           0         
_________________________________________________________________
conv_preds (INQ_Conv2D)      (None, 1, 1, 1)           8         
_________________________________________________________________
flatten_1 (Flatten)          (None, 1)                 0         
_________________________________________________________________
output_layer (Activation)    (None, 1)                 0         
=================================================================
Total params: 115,225
Trainable params: 38,403
Non-trainable params: 76,822
_________________________________________________________________

  32/1034 [..............................] - ETA: 25s
 736/1034 [====================>.........] - ETA: 0s 
1034/1034 [==============================] - 1s 876us/step
Validation results {'acc': 0.9381044487427466, 'precision': 0.9204712812960235, 'recall': 0.984251968503937, 'rocauc': 0.9244568113196378, 'f1score': 0.9512937595129376, 'er': 0.061895551257253434}
Validation threshold [0.2]

  32/1248 [..............................] - ETA: 0s
 768/1248 [=================>............] - ETA: 0s
1248/1248 [==============================] - 0s 72us/step
{'acc': 0.9759615384615384, 'precision': 0.9793205317577548, 'recall': 0.9764359351988218, 'rocauc': 0.9759156828893933, 'f1score': 0.9778761061946903, 'er': 0.024038461538461564}
['mountaineer']
y_test per category [679] shape (1248, 1)
y_pred per category [677.] shape (1248, 1)
0
{'acc': 0.9759615384615384, 'precision': 0.9793205317577548, 'recall': 0.9764359351988218, 'rocauc': 0.9759156828893933, 'f1score': 0.9778761061946903, 'er': 0.024038461538461564}
Index(['Unnamed: 0', 'start_time', 'end_time', 'microseismic_labels'], dtype='object')
On nonevents
{'acc': 0.9738095238095238, 'precision': 0.8666666666666667, 'recall': 0.6753246753246753, 'rocauc': 0.8342811035118727, 'f1score': 0.7591240875912408, 'er': 0.02619047619047621}
2018-10-12 16:40:46.980263: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2018-10-12 16:40:46.980297: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2018-10-12 16:40:46.980304: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2018-10-12 16:40:46.980310: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2018-10-12 16:40:46.980316: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
2018-10-12 16:40:47.297562: I tensorflow/core/common_runtime/gpu/gpu_device.cc:955] Found device 0 with properties: 
name: TITAN Xp
major: 6 minor: 1 memoryClockRate (GHz) 1.582
pciBusID 0000:84:00.0
Total memory: 11.90GiB
Free memory: 11.74GiB
2018-10-12 16:40:47.297594: I tensorflow/core/common_runtime/gpu/gpu_device.cc:976] DMA: 0 
2018-10-12 16:40:47.297601: I tensorflow/core/common_runtime/gpu/gpu_device.cc:986] 0:   Y 
2018-10-12 16:40:47.297610: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1045] Creating TensorFlow device (/gpu:0) -> (device: 0, name: TITAN Xp, pci bus id: 0000:84:00.0)
Loading microseismic model
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
input (InputLayer)           (None, 64, 24, 1)         0         
_________________________________________________________________
block1_conv1 (INQ_Conv2D)    (None, 64, 24, 32)        962       
_________________________________________________________________
activation_1 (Activation)    (None, 64, 24, 32)        0         
_________________________________________________________________
block1_conv2_stride2 (INQ_Co (None, 32, 12, 32)        27746     
_________________________________________________________________
activation_2 (Activation)    (None, 32, 12, 32)        0         
_________________________________________________________________
dropout_1 (Dropout)          (None, 32, 12, 32)        0         
_________________________________________________________________
block2_conv3 (INQ_Conv2D)    (None, 32, 12, 32)        27746     
_________________________________________________________________
activation_3 (Activation)    (None, 32, 12, 32)        0         
_________________________________________________________________
block2_conv4_stride2 (INQ_Co (None, 16, 6, 32)         27746     
_________________________________________________________________
activation_4 (Activation)    (None, 16, 6, 32)         0         
_________________________________________________________________
dropout_2 (Dropout)          (None, 16, 6, 32)         0         
_________________________________________________________________
blockC_conv1 (INQ_Conv2D)    (None, 16, 6, 32)         27746     
_________________________________________________________________
activation_5 (Activation)    (None, 16, 6, 32)         0         
_________________________________________________________________
blockC_conv2 (INQ_Conv2D)    (None, 16, 6, 32)         3170      
_________________________________________________________________
activation_6 (Activation)    (None, 16, 6, 32)         0         
_________________________________________________________________
dropout_3 (Dropout)          (None, 16, 6, 32)         0         
_________________________________________________________________
blockC_conv3 (INQ_Conv2D)    (None, 16, 6, 1)          101       
_________________________________________________________________
activation_7 (Activation)    (None, 16, 6, 1)          0         
_________________________________________________________________
average_pooling2d_1 (Average (None, 1, 6, 1)           0         
_________________________________________________________________
average_pooling2d_2 (Average (None, 1, 1, 1)           0         
_________________________________________________________________
conv_preds (INQ_Conv2D)      (None, 1, 1, 1)           8         
_________________________________________________________________
flatten_1 (Flatten)          (None, 1)                 0         
_________________________________________________________________
output_layer (Activation)    (None, 1)                 0         
=================================================================
Total params: 115,225
Trainable params: 38,403
Non-trainable params: 76,822
_________________________________________________________________

  32/1034 [..............................] - ETA: 25s
 736/1034 [====================>.........] - ETA: 0s 
1034/1034 [==============================] - 1s 877us/step
Validation results {'acc': 0.9381044487427466, 'precision': 0.9204712812960235, 'recall': 0.984251968503937, 'rocauc': 0.9244568113196378, 'f1score': 0.9512937595129376, 'er': 0.061895551257253434}
Validation threshold [0.2]

  32/1248 [..............................] - ETA: 0s
 672/1248 [===============>..............] - ETA: 0s
1248/1248 [==============================] - 0s 74us/step
Using TensorFlow backend.
{'acc': 0.9759615384615384, 'precision': 0.9793205317577548, 'recall': 0.9764359351988218, 'rocauc': 0.9759156828893933, 'f1score': 0.9778761061946903, 'er': 0.024038461538461564}
['mountaineer']
y_test per category [679] shape (1248, 1)
y_pred per category [677.] shape (1248, 1)
0
{'acc': 0.9759615384615384, 'precision': 0.9793205317577548, 'recall': 0.9764359351988218, 'rocauc': 0.9759156828893933, 'f1score': 0.9778761061946903, 'er': 0.024038461538461564}
Index(['Unnamed: 0', 'start_time', 'end_time', 'microseismic_labels'], dtype='object')
On nonevents
{'acc': 0.9738095238095238, 'precision': 0.8666666666666667, 'recall': 0.6753246753246753, 'rocauc': 0.8342811035118727, 'f1score': 0.7591240875912408, 'er': 0.02619047619047621}
2018-11-29 11:18:54.788347: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:18:54.788370: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:18:54.788376: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:18:54.788383: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:18:54.788388: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
Using TensorFlow backend.
Loading microseismic model
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
Traceback (most recent call last):
  File "test.py", line 190, in <module>
    import permasense
ModuleNotFoundError: No module named 'permasense'
2018-11-29 11:22:06.762277: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:22:06.762300: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:22:06.762305: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:22:06.762308: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:22:06.762311: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
Using TensorFlow backend.
Loading microseismic model
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
Traceback (most recent call last):
  File "test.py", line 190, in <module>
    import permasense
ModuleNotFoundError: No module named 'permasense'
2018-11-29 11:26:54.913682: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.1 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:26:54.913707: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use SSE4.2 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:26:54.913712: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:26:54.913716: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use AVX2 instructions, but these are available on your machine and could speed up CPU computations.
2018-11-29 11:26:54.913720: W tensorflow/core/platform/cpu_feature_guard.cc:45] The TensorFlow library wasn't compiled to use FMA instructions, but these are available on your machine and could speed up CPU computations.
Using TensorFlow backend.
Loading microseismic model
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
mybuild: INQ_Conv2D
Traceback (most recent call last):
  File "test.py", line 190, in <module>
    import permasense
ModuleNotFoundError: No module named 'permasense'
