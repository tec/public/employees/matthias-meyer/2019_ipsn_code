"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import numpy as np
import os 
import keras

####### Change the path here if necessary
data_dir = '../data/'
def get_data_dir():
    return data_dir

####### Change the path here if necessary
tmp_dir = 'tmp/'
def get_tmp_dir():
    return tmp_dir

def get_features(x,save_name=None):
    from scipy.stats import kurtosis


    def reload_files(x):
        num_samples = x.shape[0]
        num_channels = x.shape[-1]
        std = np.zeros((num_samples,1,num_channels))
        kur = np.zeros((num_samples,1,num_channels))
        energy_bins = np.zeros((num_samples,135,num_channels))

        events_list = []
        for i in range(num_samples):
            print(i,num_samples)
            for j in range(num_channels):
                std[i,0,j] = np.std(x[i,:,j])
                kur[i,0,j] = kurtosis(x[i,:,j])
                fft = np.abs(np.fft.fft(x[i,:,j]))**2

                # We have a sampling rate of 1000 Hz, we only want to assume up to 250 Hz.
                N = int(fft.shape[0]//4 // energy_bins.shape[1])
                for k in range(energy_bins.shape[1]):
                    energy_bins[i,k,j] = np.sum(fft[k*N:(k+1)*N])
                
        return np.concatenate((std,kur,energy_bins),axis=1)


    if not save_name:
        return reload_files(x)
    elif isinstance(save_name,str):
        if not os.path.isfile(save_name):
            x = reload_files(x)
            np.save(save_name,x)
            return x
        else:
            return np.load(save_name)



# keras.utils.generic_utils.get_custom_objects().update({'relu6': keras.applications.mobilenet.relu6,'DepthwiseConv2D': keras.applications.mobilenet.DepthwiseConv2D})
