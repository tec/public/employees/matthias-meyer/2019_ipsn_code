"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import numpy as np
import pandas as pd 
from plotly.offline import plot
import plotly.graph_objs as go

from aednn.toolbox import threshold, evaluate, check_overlap
from aednn.datasets.MHdataset import microseismic_dataset

from aednn.models.geocnn import geocnn
import pandas as pd 
import numpy as np 
from tqdm import tqdm  

from keras.models import load_model 

import permasense
permasense_vault_dir = '/home/perma/permasense_vault/'
permasense.global_settings.get_global_settings()['permasense_vault_dir'] = permasense_vault_dir
import permasense.microseismic.data_loaders as ms_loader

from INQ_layers import INQ_Conv2D, INQ_Dense, Flatten
import INQ_model

import argparse

modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events','image','ensemble']
runs = ['test','inq']
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpu", type=str,
                    help="use the given gpu")
parser.add_argument("-m", "--mode", type=str,
                    help="choose a mode from %s"%str(modes))
parser.add_argument("-r", "--run", type=str, default='inq',
                    help="choose a run from %s"%str(runs))
args = parser.parse_args()

mode = args.mode 
run = args.run 

# Load dummy dataset for transformation
dataset = microseismic_dataset('tmp/',name='none',params={'dataframe':pd.DataFrame.from_dict({'microseismic_labels':['none'],'start_time':[0]}),'datadir':0},transform_type='custom_melspec')
# Load model
net = load_model('results/%s/%s/keras.netbest'%(mode,run),custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})
val_threshold = np.load('results/%s/%s/val_threshold.npy'%(mode,run))


def extract_waveforms(name):
    events = pd.read_csv('tmp/microseismic_%s_events.csv'%name)
    events['start_time']                  = pd.to_datetime(events['start_time'],utc=True)
    events['end_time']                    = pd.to_datetime(events['end_time'],utc=True)
    # events['microseismic_labels']         = events['microseismic_labels'].map(eval)
    # events['microseismic_labels']         = 
    events['labels'] = events['microseismic_labels'].apply(lambda x: 'mountaineer' in x)
    # events['labels'] = events['labels'].apply(lambda x: np.unique(x))
    # events['labels'] = events['labels'].apply(lambda x: x=='mountaineer')

    waveform_list = []
    output_list = []

    for i in range(200):
    # for i in tqdm(range(len(events))):
        event = events.iloc[i]

        # print(event['microseismic_labels'])
        # print(event['labels'])

        waveform = np.zeros((12800,))
        waveform[:12800] = ms_loader.get_numpy_array(event['start_time'],event['end_time'],station='MH36',channels=['EHZ']).reshape((-1,))[:12800]


        waveform_list.append(waveform)

        spec = dataset.transform(waveform)
        spec = np.expand_dims(spec,-1)
        spec_pp = geocnn.preprocessing(spec)
        spec_pp = np.expand_dims(spec_pp,0)

        value = net.predict(spec_pp,batch_size=1)[0]
        decision = threshold(value,val_threshold)

        ground_truth = event['labels'].astype(np.int)
        output_list.append([ground_truth,value,decision])


    nn_out = np.array(output_list).squeeze()
    waveforms = np.array(waveform_list)

    # we are going to interleave the samples and their labels that at the beginning we have alteranting pos/neg
    positive_mask = np.where(nn_out[:,0]==1)[0]
    negative_mask = np.where(nn_out[:,0]==0)[0]

    if len(positive_mask) > len(negative_mask):
        short_mask = negative_mask
        long_mask = positive_mask
    else:
        short_mask = positive_mask
        long_mask = negative_mask

    indices = np.zeros((len(nn_out),),dtype=np.int)
    indices[0:2*len(short_mask):2] = short_mask
    indices[1:2*len(short_mask):2] = long_mask[:len(short_mask)]
    indices[2*len(short_mask):] = long_mask[len(short_mask):]

    nn_out = nn_out[indices]
    waveforms = waveforms[indices]

    print('waveforms shape',waveforms.shape)
    np.save('tmp/waveforms.npy',waveforms)
    
    print('nn_out shape',nn_out.shape)
    np.save('tmp/nn_out.npy',nn_out)

    print(nn_out)


extract_waveforms('test')

