# mhcnn repository


## Requirements

Python and additional modules are required. Using anaconda you can install the requirements by executing the following command from this directory and activating the newly created environment

```
conda env create -f environment.yml
source activate mhcnn
```

Besides the training data, the scripts need additional disk space to store the preprocessed data. This can more than 60GB. Make sure you have enough disk space available. Alternativly, you can specify a location with enough storage. See **Changing paths** section.

## Getting Started

Install the requirements.

Download and extract the dataset. See ../data/ReadMe.md for more information.

To test all the pretrained networks run from the *mhcnn/* directory:

```
python run_experiment.py -id final -r test
python run_experiment.py -id final -r inq
```

This will load and preprocess all the test set files for each experiment, which takes some time the first time (unfortunately without progress bar at the moment). Then, it will display the test results for each classifier.


If the script fails. Run the script to test only one of the classifiers (e.g. microseismicCNN) to check functionality:

```
python test.py -m microseismic_events
```


To train all classifiers you can run 

```
python run_experiment.py
```


To train and test an individual classifier run

```
python train.py -m classifier_name
python test.py -m classifier_name
```

`classifier_name` should be 'microseismic_events'

## Prepare for MCU implementation

If you retrain the network the CNN must be converted to be used on the MCU. First, it must be quantized with INQ. Then, it must be converted to C code.

### INQ 
The train.py script copies the generated files to the results/microseismic_events/test/ folder. The INQ.py script takes these results and quantizes them. Run the script as follows

```
python INQ.py -m microseismic_events
```

### Convert to C code
The MCU implementation requires the weights of the INQ model. These are converted with the following script

```
python convert_to_c_header.py -m microseismic_events
```

The scripts creates a new file *model_weights.h* in the tmp folder. This file can then be copied to mhmcu/Inc/.

## Changing paths

If you run the script from a different folder than where your data is stored you need to update some paths.
Open the file *toolbox.py* change the variable `data_dir` at the beginning of the file to the path where you extracted the datasets (the folder containing dataset/ and event_dataset/). In the same file change the `tmp_dir` variable to a path with enough storage to create temporary training/testing files (60GB or more, if all classifiers are to be trained).