
from __future__ import print_function
import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D, Conv2DTranspose, AveragePooling2D, GaussianNoise, Lambda, TimeDistributed
from keras.layers.normalization import BatchNormalization
# from keras.applications.mobilenet import MobileNet
from keras.layers import Dense, GlobalAveragePooling2D,Reshape, Convolution2D, Dropout, Activation, Flatten
from keras import regularizers

import librosa

import keras.backend as K
import numpy as np 

import tensorflow as tf 

def tf_logamp(x):
    import tensorflow as tf 
    log_offset = 1e-6
    return tf.log(x + log_offset)
    

def get_params():
    return []

def get_model(dataset,input_shape=None,num_labels=None,model_name = 'notmobilenet',final_activation='sigmoid',leaky_relu=False,alpha=1,weights=None):
    if input_shape is None:
        input_shape = dataset.shape[1:]

    if num_labels is None:
        num_labels = dataset.num_labels

    input_shape = tuple(np.array(input_shape)[[1,0,2]])

    input_tensor = Input(shape=input_shape,name='input')
    # model_input = Lambda(tf_logamp)(input_tensor)
    model_input = input_tensor

    if model_name == 'notmobilenet':
        momentum = 0.99
        alpha = 1
        kernel_regularizer = regularizers.l2(0.01)
        kernel_regularizer = None
        dropout_rate = 0.2

        from keras.layers.advanced_activations import LeakyReLU
        activation = 'relu'

        if leaky_relu:
            activation = None           
            ActivationLayer = LeakyReLU
        else:
            def bypass():
                return Lambda(lambda x:x)
            ActivationLayer = bypass

        x = model_input
        # x = Lambda(function=logamp,name='logamp')(input_tensor)

        # x = Conv2D(4, (3, 3), activation=activation, padding='same', name='block1_conv1_40', strides=(1,1))(x)
        # x = Conv2D(4, (3, 3), activation=activation, padding='same', name='block1_conv1_41', strides=(1,1))(x)
        # x = Conv2D(8, (3, 3), activation=activation, padding='same', name='block1_conv1_8', strides=(1,1))(x)
        # x = Conv2D(16, (3, 3), activation=activation, padding='same', name='block1_conv1_16', strides=(1,1))(x)
        # x = Conv2D(32, (3, 3), activation=activation, padding='same', name='block1_conv1_32', strides=(1,1))(x)

        x = Conv2D(int(32*alpha), (3, 3), activation=activation, padding='same', name='block1_conv1', strides=(1,1),kernel_regularizer=kernel_regularizer)(x)
        # x = BatchNormalization(momentum=momentum)(x)
        x = Conv2D(int(32*alpha), (3, 3), activation=activation, padding='same', name='block1_conv2_stride2', strides=(2,2),kernel_regularizer=kernel_regularizer,)(x)
        x = ActivationLayer()(x)
        x = Dropout(dropout_rate)(x)
        # x = BatchNormalization(momentum=momentum)(x)

        # Block 2
        x = Conv2D(int(32*alpha), (3, 3), activation=activation, padding='same', name='block2_conv3', strides=(1,1),kernel_regularizer=kernel_regularizer)(x)
        x = ActivationLayer()(x)
        # x = BatchNormalization(momentum=momentum)(x)
        x = Conv2D(int(32*alpha), (3, 3), activation=activation, padding='same', name='block2_conv4_stride2', strides=(2,2),kernel_regularizer=kernel_regularizer)(x)
        x = ActivationLayer()(x)
        x = Dropout(dropout_rate)(x)
        # x = BatchNormalization(momentum=momentum)(x)
        
        # Classification block
        x = Conv2D(int(32*alpha), (3, 3), activation=activation, padding='same', name='blockC_conv1')(x)
        x = ActivationLayer()(x)
        # x = BatchNormalization()(x)
        x = Conv2D(int(32*alpha), (1, 1), activation=activation, padding='same', name='blockC_conv2')(x)
        x = ActivationLayer()(x)
        x = Dropout(dropout_rate)(x)

        classification_conv     = Conv2D(num_labels, (1, 1), activation=activation, padding='same', name='blockC_conv3')
        x                       = classification_conv(x)
        x = ActivationLayer()(x)

        out_size                = classification_conv.output_shape
        x                       = AveragePooling2D(pool_size=(out_size[1], out_size[2]))(x)
        # x                       = Dropout(1e-3, name='dropout')(x)
        x                       = Convolution2D(num_labels, (1, 1), padding='same', name='conv_preds')(x)

        x                       = Flatten()(x)
        x                       = Activation(final_activation, name='output_layer')(x)


    model = Model(input_tensor,x)
    model.summary()
    return [model,'binary_crossentropy']

def preprocessing(X,y=None,train_data=False,logamp=True,bypass_transpose=False):
    ''' preprocesses the input data to fit the model. expects X to be a 4D (num_batches, width, height, channels) or 3D tensor (width, height, channels)
    '''   

    if logamp:
        X = librosa.power_to_db(X)
    
    if not bypass_transpose:
        if X.ndim == 4:
            X = X.transpose((0, 2, 1, 3))
        else:
            X = X.transpose((1, 0, 2))

    if y is not None:
        return X,y
    else:
        return X