"""
Script for the paper "Systematic Identification of External Influences in Multi-Year Micro-Seismic Recordings Using Convolutional Neural Networks"

Implementation of a seismic event classifier. Adopted from https://github.com/tperol/ConvNetQuake

Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function
    
from keras.preprocessing import image
from keras.layers import Dense, GlobalAveragePooling2D,Reshape, Convolution2D, Conv1D, Dropout, Activation, Flatten, Input, TimeDistributed, LSTM, Lambda, MaxPool2D,Conv2D, GlobalMaxPooling2D, BatchNormalization
from keras.models import Model, Sequential
import numpy as np
from keras.applications.resnet50 import ResNet50
from keras import regularizers

#from aednn.models.mod_mobilenet import MobileNet
# from keras.applications.mobilenet import MobileNet, _depthwise_conv_block
#from keras.applications.mobilenet import preprocess_input

import keras.backend as K 
import tensorflow as tf 
#from aednn.custom_layers import ImageSegmentation

batch_size = 16

class MSAECNN:
    def get_params():
        return []

    def get_model(dataset,input_shape=None,num_labels=None,**kwargs):
        if input_shape is None:
            input_shape = dataset.shape[1:]

        if num_labels is None:
            num_labels = dataset.num_labels
        
        

        input_tensor = Input(shape=input_shape,name='input')

        kernel_regularizer = regularizers.l2(0.001)
        # kernel_regularizer = None

        def cconv1d(x,filters=32,
                kernel_size=3,
                strides=2,
                padding='same',
                dilation_rate=1,
                activation='relu',
                use_bias=True,
                kernel_initializer='glorot_uniform',
                bias_initializer='zeros',
                kernel_regularizer=kernel_regularizer,
                bias_regularizer=None,
                activity_regularizer=None,
                kernel_constraint=None,
                bias_constraint=None,
                **kwargs):
            return Conv1D(filters,kernel_size,strides=strides,
                        padding=padding,dilation_rate=dilation_rate,
                        activation=activation,use_bias=use_bias,kernel_initializer=kernel_initializer,
                        bias_initializer=bias_initializer,kernel_regularizer=kernel_regularizer)(x)
        
        x = cconv1d(input_tensor)
        x = cconv1d(x)
        x = cconv1d(x)
        x = cconv1d(x)
        x = cconv1d(x)
        x = cconv1d(x)
        x = cconv1d(x)
        x = cconv1d(x)
        x = Flatten()(x)
        x = Dense(num_labels,kernel_regularizer=kernel_regularizer,activation='sigmoid')(x)

        model = Model(input_tensor,x)

        return [model]


    def preprocessing(X,y=None,train_data=False,**kwargs):
        ''' preprocesses the input data to fit the model. expects X to be a 4D (num_batches, width, height, channels) or 3D tensor (width, height, channels)
        '''          

        if y is not None:
            return X,y
        else:
            return X
    
    def predict(X_true,y_true):
        """
        """

        pass

    def gen_image(in_data):
        """
            insert one data sample and returns a suitable graphical representation
            generates one image with (width,height,depth)
        """
        print('data shape',in_data.shape)
        img = in_data
        return img
