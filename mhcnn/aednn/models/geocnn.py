"""
Script for the paper "Systematic Identification of External Influences in Multi-Year Micro-Seismic Recordings Using Convolutional Neural Networks"

Implementation of a acoustic event classifier for seismic data

Copyright (c) 2018, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

from __future__ import print_function
import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D, Conv2DTranspose, AveragePooling2D, GaussianNoise, Lambda, TimeDistributed, LSTM
from keras.layers.normalization import BatchNormalization
from keras.applications.mobilenet import MobileNet
from keras.layers import Dense, GlobalAveragePooling2D,Reshape, Convolution2D, Dropout, Activation, Flatten
from keras import regularizers

import librosa

import keras.backend as K
import numpy as np 
    
class geocnn:
    def get_params():

        return []

    def get_model(dataset,input_shape=None,num_labels=None,final_activation='sigmoid',leaky_relu=False,alpha=1,**kwargs):
        if input_shape is None:
            input_shape = dataset.shape[1:]

        if num_labels is None:
            num_labels = dataset.num_labels
            
        input_shape = tuple(np.array(input_shape)[[1,0,2]])

        input_tensor = Input(shape=input_shape,name='input')
        model_input = input_tensor

        momentum = 0.99
        # kernel_regularizer = regularizers.l2(0.00001)
        kernel_regularizer = None 
        padding = 'valid'

        from keras.layers.advanced_activations import LeakyReLU
        activation = 'relu'

        if leaky_relu:
            activation = None           
            ActivationLayer = LeakyReLU
        else:
            # def bypass():
            #     return Lambda(lambda x:x)
            def bypass():
                return Activation(activation)
            ActivationLayer = bypass

        x = model_input

        from keras.initializers import he_uniform

        def cconv2d(x,filters=32,
                kernel_size=(3, 3),
                strides=(1,1),
                padding=padding,
                dilation_rate=(1,1),
                activation=None,
                use_bias=True,
                kernel_initializer=he_uniform(),
                # kernel_initializer='he_uniform',
                bias_initializer='zeros',
                kernel_regularizer=kernel_regularizer,
                bias_regularizer=None,
                activity_regularizer=None,
                kernel_constraint=None,
                bias_constraint=None,
                name = None,
                **kwargs):

            return Conv2D(filters,kernel_size,strides=strides,
                        padding=padding,dilation_rate=dilation_rate,
                        activation=activation,use_bias=use_bias,kernel_initializer=kernel_initializer,
                        bias_initializer=bias_initializer,kernel_regularizer=kernel_regularizer,name=name)(x)
                    

        dropout_rate = 0.2

      
        x = cconv2d(x,int(32*alpha), name='block1_conv1')
        # x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)
        
        x = cconv2d(x,int(32*alpha), name='block1_conv2_stride2', strides=(2,2))
        # x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)
        x = Dropout(dropout_rate)(x)


        # # Block 2
        x = cconv2d(x,int(32*alpha), name='block2_conv3')
        # x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)

        x = cconv2d(x,int(32*alpha), name='block2_conv4_stride2', strides=(2,2))
        # x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)
        x = Dropout(dropout_rate)(x)

        '''x = cconv2d(x,int(128*alpha), name='block3_conv3')
        x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)
        x = cconv2d(x,int(128*alpha), name='block3_conv4_stride2', strides=(2,2))
        x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)
        $I$ & Input & - & - & 24 x 64 x 1\\
        $C_1$ & Conv2D + ReLU & 3x3 & 1 & 24 x 64 x 32\\
        $C_2$ & Conv2D + ReLU & 3x3 & 2 & 12 x 32 x 32\\
        $D_0$ & Dropout & - & 12 x 32 x 32 \\
        $C_3$ & Conv2D + ReLU & 3x3 & 1 & 12 x 32 x 32\\
        $C_4$ & Conv2D + ReLU & 3x3 & 2 & 6 x 16 x 32\\
        $D_1$ & Dropout & - & 6 x 16 x 32 \\
        $C_5$ & Conv2D + ReLU & 3x3 & 1 & 6 x 16 x 32\\
        $C_6$ & Conv2D + ReLU & 1x1 & 1 & 6 x 16 x 32\\
        $D_2$ & Dropout & - & 6 x 16 x 32 \\
        $C_7$ & Conv2D + ReLU & 1x1 & 1 & 6 x 16 x 1\\
        $A_f$ & Average Pooling (Frequency) & - & 1 & 6 x 1 x 1 \\
        $A_t$ & Average Pooling (Time) & - & 1 & 1 x 1 x 1 \\
        $C_8$ & Conv2D + Sigmoid & 1x1 & 1 & 1 x 1 x 1 \\

        x = cconv2d(x,int(128*alpha), name='block4_conv3')
        x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)
        x = cconv2d(x,int(128*alpha), name='block4_conv4_stride2', strides=(2,2))
        x = BatchNormalization(momentum=momentum)(x)
        x = ActivationLayer()(x)'''



        # Classification block
        x = cconv2d(x,int(32*alpha), name='blockC_conv1')
        # x = BatchNormalization()(x)
        x = ActivationLayer()(x)

        x = cconv2d(x,int(32*alpha), (1, 1), name='blockC_conv2')
        x = ActivationLayer()(x)
        x                       = Dropout(dropout_rate)(x)

        classification_conv     = Conv2D(num_labels, (1, 1), activation=activation, padding=padding, kernel_initializer=he_uniform(), name='blockC_conv3')
        x                       = classification_conv(x)
        x = ActivationLayer()(x)

        out_size                = classification_conv.output_shape
        x                       = AveragePooling2D(pool_size=(out_size[1], 1))(x)
        avg_pol_freq = x

        x                       = AveragePooling2D(pool_size=(1,out_size[2]))(x)
        # x                       = Dropout(1e-3, name='dropout')(x)
        x                       = Convolution2D(num_labels, (1, 1), padding=padding, kernel_initializer=he_uniform(seed=5346), name='conv_preds')(x)
        x                       = Flatten()(x)
        x                       = Activation(final_activation, name='output_layer')(x)

        model = Model(input_tensor,x)
        # model.summary()
        return [model,'binary_crossentropy']

    def preprocessing(X,y=None,train_data=False,logamp=True,select_random_channel=False,bypass_transpose=False):
        ''' preprocesses the input data to fit the model. expects X to be a 4D (num_batches, width, height, channels) or 3D tensor (width, height, channels)
        '''   

        if select_random_channel and X.shape[-1] == 3:
            idx = np.random.randint(3)
            X = X[...,idx:idx+1]

        if logamp:
            X = 10*np.log10(X+0.0000000001)
            # X = librosa.power_to_db(X)
        
        if not bypass_transpose:
            if X.ndim == 4:
                X = X.transpose((0, 2, 1, 3))
            else:
                X = X.transpose((1, 0, 2))

        if y is not None:
            return X,y
        else:
            return X
