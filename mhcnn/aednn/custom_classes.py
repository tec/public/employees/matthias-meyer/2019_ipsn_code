# from keras.preprocessing.image import *
# This file has been modified from keras (www.keras.io)
from __future__ import absolute_import
from __future__ import print_function

from keras.preprocessing.image import *
to_exclude = ['NumpyArrayIterator']
for name in to_exclude:
    del globals()[name]

import numpy as np
import re
from scipy import linalg
import scipy.ndimage as ndi
from six.moves import range
import os
import threading
import warnings
import multiprocessing.pool
from functools import partial

from keras import backend as K
from keras.utils.data_utils import Sequence
from keras.callbacks import Callback

try:
    from PIL import Image as pil_image
except ImportError:
    pil_image = None

class CustomDataGenerator(object):
    """Generate minibatches of image data with real-time data augmentation.

    # Arguments
        featurewise_center: set input mean to 0 over the dataset.
        samplewise_center: set each sample mean to 0.
        featurewise_std_normalization: divide inputs by std of the dataset.
        samplewise_std_normalization: divide each input by its std.
        zca_whitening: apply ZCA whitening.
        zca_epsilon: epsilon for ZCA whitening. Default is 1e-6.
        rotation_range: degrees (0 to 180).
        width_shift_range: fraction of total width.
        height_shift_range: fraction of total height.
        shear_range: shear intensity (shear angle in radians).
        zoom_range: amount of zoom. if scalar z, zoom will be randomly picked
            in the range [1-z, 1+z]. A sequence of two can be passed instead
            to select this range.
        channel_shift_range: shift range for each channels.
        fill_mode: points outside the boundaries are filled according to the
            given mode ('constant', 'nearest', 'reflect' or 'wrap'). Default
            is 'nearest'.
        cval: value used for points outside the boundaries when fill_mode is
            'constant'. Default is 0.
        horizontal_flip: whether to randomly flip images horizontally.
        vertical_flip: whether to randomly flip images vertically.
        rescale: rescaling factor. If None or 0, no rescaling is applied,
            otherwise we multiply the data by the value provided. This is
            applied after the `preprocessing_function` (if any provided)
            but before any other transformation.
        preprocessing_function: function that will be implied on each input.
            The function will run before any other modification on it.
            The function should take one argument:
            one image (Numpy tensor with rank 3),
            and should output a Numpy tensor with the same shape.
        data_format: 'channels_first' or 'channels_last'. In 'channels_first' mode, the channels dimension
            (the depth) is at index 1, in 'channels_last' mode it is at index 3.
            It defaults to the `image_data_format` value found in your
            Keras config file at `~/.keras/keras.json`.
            If you never set it, then it will be "channels_last".
    """

    def __init__(self,
                 featurewise_center=False,
                 samplewise_center=False,
                 featurewise_std_normalization=False,
                 samplewise_std_normalization=False,
                 zca_whitening=False,
                 zca_epsilon=1e-6,
                 rotation_range=0.,
                 width_shift_range=0.,
                 height_shift_range=0.,
                 shear_range=0.,
                 zoom_range=0.,
                 channel_shift_range=0.,
                 fill_mode='nearest',
                 cval=0.,
                 horizontal_flip=False,
                 vertical_flip=False,
                 rescale=None,
                 preprocessing_function=None,
                 circular_shift = False,
                 crop_spec = False,
                 crop_size = None,
                 scale_range = 0.,
                 add_awgn = False,
                 data_format=None):
        if data_format is None:
            data_format = K.image_data_format()

        self.featurewise_center = featurewise_center
        self.samplewise_center = samplewise_center
        self.featurewise_std_normalization = featurewise_std_normalization
        self.samplewise_std_normalization = samplewise_std_normalization
        self.zca_whitening = zca_whitening
        self.zca_epsilon = zca_epsilon
        self.rotation_range = rotation_range
        self.width_shift_range = width_shift_range
        self.height_shift_range = height_shift_range
        self.shear_range = shear_range
        self.zoom_range = zoom_range
        self.channel_shift_range = channel_shift_range
        self.fill_mode = fill_mode
        self.cval = cval
        self.horizontal_flip = horizontal_flip
        self.vertical_flip = vertical_flip
        self.rescale = rescale
        self.preprocessing_function = preprocessing_function
        self.circular_shift = circular_shift
        self.crop_spec = crop_spec
        self.crop_size = crop_size
        self.add_awgn = add_awgn

        if data_format not in {'channels_last', 'channels_first'}:
            raise ValueError('`data_format` should be `"channels_last"` (channel after row and '
                             'column) or `"channels_first"` (channel before row and column). '
                             'Received arg: ', data_format)
        self.data_format = data_format
        if data_format == 'channels_first':
            self.channel_axis = 1
            self.row_axis = 2
            self.col_axis = 3
        if data_format == 'channels_last':
            self.channel_axis = 3
            self.row_axis = 1
            self.col_axis = 2

        self.mean = None
        self.std = None
        self.principal_components = None

        self.np_iterator = None

        if np.isscalar(zoom_range):
            self.zoom_range = [1 - zoom_range, 1 + zoom_range]
        elif len(zoom_range) == 2:
            self.zoom_range = [zoom_range[0], zoom_range[1]]
        else:
            raise ValueError('`zoom_range` should be a float or '
                             'a tuple or list of two floats. '
                             'Received arg: ', zoom_range)

        if np.isscalar(scale_range):
            self.scale_range = [1 - scale_range, 1 + scale_range]
        elif len(scale_range) == 2:
            self.scale_range = [scale_range[0], scale_range[1]]
        else:
            raise ValueError('`scale_range` should be a float or '
                             'a tuple or list of two floats. '
                             'Received arg: ', scale_range)

    def flow(self, x=None, y=None, use_y=True, dataset=None, batch_size=8, shuffle=True, seed=None,
             save_to_dir=None, save_prefix='', save_format='png',label_whitelist=None, distribution=None):

        # if self.np_iterator is None:
        #     print('init')
        #     self.np_iterator = NumpyArrayIterator(
        #         x, y, self,
        #         batch_size=batch_size,
        #         shuffle=shuffle,
        #         seed=seed,
        #         data_format=self.data_format,
        #         save_to_dir=save_to_dir,
        #         save_prefix=save_prefix,
        #         save_format=save_format)
        #     return self.np_iterator
        # else:
        #     print('here')
        #     self.np_iterator.x = x
        #     self.n = x.shape[0]
        #     if y is not None:
        #         self.np_iterator.y = y
        #     else:
        #         self.np_iterator.y = None
        #     return self.np_iterator
        return NumpyArrayIterator(self,
                x=x, y=y, use_y=use_y,
                dataset = dataset,
                batch_size=batch_size,
                shuffle=shuffle,
                seed=seed,label_whitelist=label_whitelist, distribution=distribution,
                data_format=self.data_format,
                save_to_dir=save_to_dir,
                save_prefix=save_prefix,
                save_format=save_format)

    def flow_from_directory(self, directory,
                            target_size=(256, 256), color_mode='rgb',
                            classes=None, class_mode='categorical',
                            batch_size=8, shuffle=True, seed=None,
                            save_to_dir=None,
                            save_prefix='',
                            save_format='png',
                            follow_links=False):
        return DirectoryIterator(
            directory, self,
            target_size=target_size, color_mode=color_mode,
            classes=classes, class_mode=class_mode,
            data_format=self.data_format,
            batch_size=batch_size, shuffle=shuffle, seed=seed,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
            follow_links=follow_links)

    def standardize(self, x):
        """Apply the normalization configuration to a batch of inputs.

        # Arguments
            x: batch of inputs to be normalized.

        # Returns
            The inputs, normalized.
        """
        if self.preprocessing_function:
            x = self.preprocessing_function(x)
        if self.rescale:
            x *= self.rescale
        # x is a single image, so it doesn't have image number at index 0
        img_channel_axis = self.channel_axis - 1
        if self.samplewise_center:
            x -= np.mean(x, axis=img_channel_axis, keepdims=True)
        if self.samplewise_std_normalization:
            x /= (np.std(x, axis=img_channel_axis, keepdims=True) + 1e-7)

        if self.featurewise_center:
            if self.mean is not None:
                x -= self.mean
            else:
                warnings.warn('This ImageDataGenerator specifies '
                              '`featurewise_center`, but it hasn\'t'
                              'been fit on any training data. Fit it '
                              'first by calling `.fit(numpy_data)`.')
        if self.featurewise_std_normalization:
            if self.std is not None:
                x /= (self.std + 1e-7)
            else:
                warnings.warn('This ImageDataGenerator specifies '
                              '`featurewise_std_normalization`, but it hasn\'t'
                              'been fit on any training data. Fit it '
                              'first by calling `.fit(numpy_data)`.')
        if self.zca_whitening:
            if self.principal_components is not None:
                flatx = np.reshape(x, (-1, np.prod(x.shape[-3:])))
                whitex = np.dot(flatx, self.principal_components)
                x = np.reshape(whitex, x.shape)
            else:
                warnings.warn('This ImageDataGenerator specifies '
                              '`zca_whitening`, but it hasn\'t'
                              'been fit on any training data. Fit it '
                              'first by calling `.fit(numpy_data)`.')
        return x

    def random_transform(self, x, seed=None):
        """Randomly augment a single image tensor.

        # Arguments
            x: 3D tensor, single image.
            seed: random seed.

        # Returns
            A randomly transformed version of the input (same shape).
        """

        # x is a single image, so it doesn't have image number at index 0
        img_row_axis = self.row_axis - 1
        img_col_axis = self.col_axis - 1
        img_channel_axis = self.channel_axis - 1

        if seed is not None:
            np.random.seed(seed)


        if self.add_awgn:
            x = x + np.random.normal(0,np.std(x),x.shape)

        # use composition of homographies
        # to generate final transform that needs to be applied
        if self.rotation_range:
            theta = np.pi / 180 * np.random.uniform(-self.rotation_range, self.rotation_range)
        else:
            theta = 0

        if self.height_shift_range:
            tx = np.random.uniform(-self.height_shift_range, self.height_shift_range) * x.shape[img_row_axis]
        else:
            tx = 0

        if self.width_shift_range:
            ty = np.random.uniform(-self.width_shift_range, self.width_shift_range) * x.shape[img_col_axis]
        else:
            ty = 0

        if self.shear_range:
            shear = np.random.uniform(-self.shear_range, self.shear_range)
        else:
            shear = 0

        if self.zoom_range[0] == 1 and self.zoom_range[1] == 1:
            zx, zy = 1, 1
        else:
            zx, zy = np.random.uniform(self.zoom_range[0], self.zoom_range[1], 2)

        transform_matrix = None
        if theta != 0:
            rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                        [np.sin(theta), np.cos(theta), 0],
                                        [0, 0, 1]])
            transform_matrix = rotation_matrix

        if tx != 0 or ty != 0:
            shift_matrix = np.array([[1, 0, tx],
                                     [0, 1, ty],
                                     [0, 0, 1]])
            transform_matrix = shift_matrix if transform_matrix is None else np.dot(transform_matrix, shift_matrix)

        if shear != 0:
            shear_matrix = np.array([[1, -np.sin(shear), 0],
                                    [0, np.cos(shear), 0],
                                    [0, 0, 1]])
            transform_matrix = shear_matrix if transform_matrix is None else np.dot(transform_matrix, shear_matrix)

        if zx != 1 or zy != 1:
            zoom_matrix = np.array([[zx, 0, 0],
                                    [0, zy, 0],
                                    [0, 0, 1]])
            transform_matrix = zoom_matrix if transform_matrix is None else np.dot(transform_matrix, zoom_matrix)

        if transform_matrix is not None:
            h, w = x.shape[img_row_axis], x.shape[img_col_axis]
            transform_matrix = transform_matrix_offset_center(transform_matrix, h, w)
            x = apply_transform(x, transform_matrix, img_channel_axis,
                                fill_mode=self.fill_mode, cval=self.cval)

        if self.channel_shift_range != 0:
            x = random_channel_shift(x,
                                     self.channel_shift_range,
                                     img_channel_axis)
        if self.horizontal_flip:
            if np.random.random() < 0.5:
                x = flip_axis(x, img_col_axis)

        if self.vertical_flip:
            if np.random.random() < 0.5:
                x = flip_axis(x, img_row_axis)

        if self.circular_shift:
            shift = np.random.randint(x.shape[0])
            x = np.roll(x,shift,0)
            # x = np.roll(x,shift,0)
            # print(x.shape,shift,img_row_axis)
            # off = np.random.randint(x.shape[0]-128)
            # x = x[off:off+128,:,:]

        if self.crop_spec:
            if self.crop_size is None:
                crop_size = x.shape[0]
            else:
                crop_size = self.crop_size
            if x.shape[0] - crop_size > 0:
                off = np.random.randint(x.shape[0]-crop_size)
            else:
                off = 0
            x = x[off:off+crop_size,...]

        if self.scale_range[0] == 1 and self.scale_range[1] == 1:
            scale = 1
        else:
            scale = np.random.uniform(self.scale_range[0], self.scale_range[1], 1)

        x *= scale

        return x

    def fit(self, x,
            augment=False,
            rounds=1,
            seed=None):
        """Fits internal statistics to some sample data.

        Required for featurewise_center, featurewise_std_normalization
        and zca_whitening.

        # Arguments
            x: Numpy array, the data to fit on. Should have rank 4.
                In case of grayscale data,
                the channels axis should have value 1, and in case
                of RGB data, it should have value 3.
            augment: Whether to fit on randomly augmented samples
            rounds: If `augment`,
                how many augmentation passes to do over the data
            seed: random seed.

        # Raises
            ValueError: in case of invalid input `x`.
        """
        x = np.asarray(x, dtype=K.floatx())
        if x.ndim != 4:
            raise ValueError('Input to `.fit()` should have rank 4. '
                             'Got array with shape: ' + str(x.shape))
        if x.shape[self.channel_axis] not in {1, 3, 4}:
            warnings.warn(
                'Expected input to be images (as Numpy array) '
                'following the data format convention "' + self.data_format + '" '
                '(channels on axis ' + str(self.channel_axis) + '), i.e. expected '
                'either 1, 3 or 4 channels on axis ' + str(self.channel_axis) + '. '
                'However, it was passed an array with shape ' + str(x.shape) +
                ' (' + str(x.shape[self.channel_axis]) + ' channels).')

        if seed is not None:
            np.random.seed(seed)

        x = np.copy(x)
        if augment:
            ax = np.zeros(tuple([rounds * x.shape[0]] + list(x.shape)[1:]), dtype=K.floatx())
            for r in range(rounds):
                for i in range(x.shape[0]):
                    ax[i + r * x.shape[0]] = self.random_transform(x[i])
            x = ax

        if self.featurewise_center:
            self.mean = np.mean(x, axis=(0, self.row_axis, self.col_axis))
            broadcast_shape = [1, 1, 1]
            broadcast_shape[self.channel_axis - 1] = x.shape[self.channel_axis]
            self.mean = np.reshape(self.mean, broadcast_shape)
            x -= self.mean

        if self.featurewise_std_normalization:
            self.std = np.std(x, axis=(0, self.row_axis, self.col_axis))
            broadcast_shape = [1, 1, 1]
            broadcast_shape[self.channel_axis - 1] = x.shape[self.channel_axis]
            self.std = np.reshape(self.std, broadcast_shape)
            x /= (self.std + K.epsilon())

        if self.zca_whitening:
            flat_x = np.reshape(x, (x.shape[0], x.shape[1] * x.shape[2] * x.shape[3]))
            sigma = np.dot(flat_x.T, flat_x) / flat_x.shape[0]
            u, s, _ = linalg.svd(sigma)
            self.principal_components = np.dot(np.dot(u, np.diag(1. / np.sqrt(s + self.zca_epsilon))), u.T)




# class Iterator(Sequence):
#     """Base class for image data iterators.

#     Every `Iterator` must implement the `_get_batches_of_transformed_samples`
#     method.

#     # Arguments
#         n: Integer, total number of samples in the dataset to loop over.
#         batch_size: Integer, size of a batch.
#         shuffle: Boolean, whether to shuffle the data between epochs.
#         seed: Random seeding for data shuffling.
#     """

#     def __init__(self, n, batch_size, shuffle, seed):
#         self.n = n
#         self.batch_size = batch_size
#         self.seed = seed
#         self.shuffle = shuffle
#         self.batch_index = 0
#         self.total_batches_seen = 0
#         self.lock = threading.Lock()
#         self.index_array = None
#         self.index_generator = self._flow_index()

#     def _set_index_array(self):
#         self.index_array = np.arange(self.n)
#         if self.shuffle:
#             self.index_array = np.random.permutation(self.n)

#     def __getitem__(self, idx):
#         if idx >= len(self):
#             raise ValueError('Asked to retrieve element {idx}, '
#                              'but the Sequence '
#                              'has length {length}'.format(idx=idx,
#                                                           length=len(self)))
#         if self.seed is not None:
#             np.random.seed(self.seed + self.total_batches_seen)
#         self.total_batches_seen += 1
#         if self.index_array is None:
#             self._set_index_array()
#         index_array = self.index_array[self.batch_size * idx:
#                                        self.batch_size * (idx + 1)]
#         return self._get_batches_of_transformed_samples(index_array)

#     def __len__(self):
#         return (self.n + self.batch_size - 1) // self.batch_size  # round up

#     def on_epoch_end(self):
#         self._set_index_array()

#     def reset(self):
#         self.batch_index = 0

#     def _flow_index(self):
#         # Ensure self.batch_index is 0.
#         self.reset()
#         while 1:
#             if self.seed is not None:
#                 np.random.seed(self.seed + self.total_batches_seen)
#             if self.batch_index == 0:
#                 self._set_index_array()

#             current_index = (self.batch_index * self.batch_size) % self.n
#             if self.n > current_index + self.batch_size:
#                 self.batch_index += 1
#             else:
#                 self.batch_index = 0
#             self.total_batches_seen += 1
#             yield self.index_array[current_index:
#                                    current_index + self.batch_size]

#     def __iter__(self):
#         # Needed if we want to do something like:
#         # for x, y in data_gen.flow(...):
#         return self

#     def __next__(self, *args, **kwargs):
#         return self.next(*args, **kwargs)

#     def _get_batches_of_transformed_samples(self, index_array):
#         """Gets a batch of transformed samples.

#         # Arguments
#             index_array: array of sample indices to include in batch.

#         # Returns
#             A batch of transformed samples.
#         """
#         raise NotImplementedError

class IteratorUniform(Sequence,Callback):
    """Base class for image data iterators.
    Every `Iterator` must implement the `_get_batches_of_transformed_samples`
    method.
    # Arguments
        n: Integer, total number of samples in the dataset to loop over.
        batch_size: Integer, size of a batch.
        shuffle: Boolean, whether to shuffle the data between epochs.
        seed: Random seeding for data shuffling.
    """

    def __init__(self, n, y, batch_size, shuffle, seed, whitelist=None,distribution=None):
        self.batch_size = batch_size
        self.seed = seed
        self.shuffle = shuffle
        self.batch_index = 0
        self.total_batches_seen = 0
        self.lock = threading.Lock()
        self.index_array = None
        self.index_generator = self._flow_index()
        self.epoch_counter = 0
        self.distribution = distribution

        self.n = n

        if self.distribution == 'uniform' or self.distribution == 'one_per_batch':     
            if y is None:
                RuntimeError('If uniform distribution is chosen y must be provided')
            if y.ndim == 1:
                y = y.reshape((-1,1))
            self.num_labels = y.shape[-1]
            if whitelist is None:
                whitelist = np.arange(self.num_labels)
            m = y.nonzero()                                              # this also removes any samples without a label
            self.bin_count = np.array(list(np.bincount(m[1])))
            # label_id = np.argmax(self.bin_count)                            # get the label with the most occurences
            # print(m)
            self.index_list = []
            bin_count_mask = []
            for i in range(self.num_labels):
                # the user can provide a list of labels which should be included when sampling from the dataset
                if i not in whitelist:
                    continue

                index = m[0][m[1] == i]
                if index.size == 0:
                    # print('continue')
                    continue
                self.index_list.append(index)
                bin_count_mask.append(i)

            self.bin_count = self.bin_count[bin_count_mask]         # remove all labels which have not been added to the index_list

            # we would also like to add the samples without an explicit label
            non_label_entries = (np.sum(y,axis=1) == 0).nonzero()
            if len(non_label_entries[0]) > 0:
                self.index_list.append(non_label_entries[0])
                self.bin_count = np.append(self.bin_count,len(non_label_entries[0]))

            # we now must update the num_labels and maxlength
            self.num_labels = len(self.index_list)
            
            # print(self.bin_count)
            self.max_length = np.max(self.bin_count)

            self.fill_threshold = self.n
            self.fill_threshold = np.min(self.bin_count)
            self.fill_threshold_delta = 0

    def _set_index_array(self):
        if self.distribution == 'uniform':
            # self.index_array = np.arange(self.max_length*self.num_labels)
            self.index_array = []
            if self.shuffle:
                for i in range(self.num_labels):
                    np.random.shuffle(self.index_list[i])
            idx = 0

            for j in range(self.max_length):
                for i in range(self.num_labels):
                    idx = j*self.num_labels + i
                    if idx > self.fill_threshold and idx > self.bin_count[i]:
                        continue
                    lidx = j%self.bin_count[i]
                    self.index_array.append(self.index_list[i][lidx].astype(np.int))
                    # print(idx,len(self.index_array),self.n,process.memory_info().rss)

                    # self.index_array[idx] =  self.index_list[i][lidx]
            self.index_array = np.array(self.index_array)
            if self.shuffle:
                np.random.shuffle(self.index_array)

        elif self.distribution == 'one_per_batch':
            # self.index_array = np.arange(self.max_length*self.num_labels)
            self.index_array = np.arange(self.n)
            if self.shuffle:
                np.random.shuffle(self.index_array)
                for i in range(self.num_labels):
                    np.random.shuffle(self.index_list[i])
            idx = 0
            label_idx = 0
            for j in range(0,self.n,self.batch_size):
                for i in range(self.num_labels):
                    label_idx = (label_idx + 1) % self.num_labels
                    if self.fill_threshold >= self.bin_count[label_idx]:
                        lidx = idx%self.bin_count[label_idx]
                        self.index_array[j] = self.index_list[label_idx][lidx].astype(np.int)
                        idx += 1
                        break
            # print('hoi',self.index_array)
            # f=open('tmp/ia.txt','w')
            # for ele in self.index_array:
            #     f.write(str(ele)+'\n')
        else:
            self.index_array = np.arange(self.n)
            if self.shuffle:
                np.random.shuffle(self.index_array)

        # print('\n',len(self.index_array),len(np.unique(self.index_array)),'\n')

        # self.index_array = self.index_array[:len(self.index_array)//self.batch_size*self.batch_size]
        self.n = len(self.index_array)

    def __getitem__(self, idx):
        if idx >= len(self):
            raise ValueError('Asked to retrieve element {idx}, '
                             'but the Sequence '
                             'has length {length}'.format(idx=idx,
                                                          length=len(self)))
        if self.seed is not None:
            np.random.seed(self.seed + self.total_batches_seen)
        self.total_batches_seen += 1
        if self.index_array is None:
            self._set_index_array()
        index_array = self.index_array[self.batch_size * idx:
                                       self.batch_size * (idx + 1)]
        return self._get_batches_of_transformed_samples(index_array)

    def __len__(self):
        return (self.n + self.batch_size - 1) // self.batch_size  # round up

    def on_epoch_end(self,epoch=None,logs=None):
        self.epoch_counter += 1
        if self.distribution == 'uniform':
            self.fill_threshold += self.fill_threshold_delta
            print('\nCurrent fill threshold is',self.fill_threshold,'\n')
        self._set_index_array()

    def reset(self):
        self.batch_index = 0
        self.epoch_counter = 0

    def _flow_index(self):
        # Ensure self.batch_index is 0.
        self.reset()
        while 1:
            if self.seed is not None:
                np.random.seed(self.seed + self.total_batches_seen)
            if self.batch_index == 0:
                self._set_index_array()

            current_index = (self.batch_index * self.batch_size) % self.n
            if self.n > current_index + self.batch_size:
                self.batch_index += 1
            else:
                self.batch_index = 0
            self.total_batches_seen += 1
            yield self.index_array[current_index:
                                   current_index + self.batch_size]

    def __iter__(self):
        # Needed if we want to do something like:
        # for x, y in data_gen.flow(...):
        return self

    def __next__(self, *args, **kwargs):
        return self.next(*args, **kwargs)

    def _get_batches_of_transformed_samples(self, index_array, augment_index_array=None, augment_factor=0.5):
        """Gets a batch of transformed samples.
        # Arguments
            index_array: array of sample indices to include in batch.
        # Returns
            A batch of transformed samples.
        """
        raise NotImplementedError


class NumpyArrayIterator(IteratorUniform):
    """Iterator yielding data from a Numpy array.

    # Arguments
        x: Numpy array of input data.
        y: Numpy array of targets data.
        image_data_generator: Instance of `ImageDataGenerator`
            to use for random transformations and normalization.
        batch_size: Integer, size of a batch.
        shuffle: Boolean, whether to shuffle the data between epochs.
        seed: Random seed for data shuffling.
        data_format: String, one of `channels_first`, `channels_last`.
        save_to_dir: Optional directory where to save the pictures
            being yielded, in a viewable format. This is useful
            for visualizing the random transformations being
            applied, for debugging purposes.
        save_prefix: String prefix to use for saving sample
            images (if `save_to_dir` is set).
        save_format: Format to use for saving sample images
            (if `save_to_dir` is set).
    """

    def __init__(self, image_data_generator, x=None, y=None, use_y=True, dataset=None,
                 batch_size=8, shuffle=False, seed=None,
                 data_format=None, label_whitelist = None, distribution=None,
                 save_to_dir=None, save_prefix='', save_format='png'):
        # if y is not None and len(x) != len(y):
        #     raise ValueError('X (images tensor) and y (labels) '
        #                      'should have the same length. '
        #                      'Found: X.shape = %s, y.shape = %s' %
        #                      (np.asarray(x).shape, np.asarray(y).shape))

        if x is None and dataset is None:
            RuntimeError('At least either x or a dataset must be provided')

        if data_format is None:
            data_format = K.image_data_format()

        if x is not None:
            self.x = np.asarray(x, dtype=K.floatx())
            # if self.x.ndim != 4:
            #     raise ValueError('Input data in `NumpyArrayIterator` '
            #                     'should have rank 4. You passed an array '
            #                     'with shape', self.x.shape)
            channels_axis = -1 if data_format == 'channels_last' else 1
            if self.x.shape[channels_axis] not in {1, 3, 4}:
                warnings.warn('NumpyArrayIterator is set to use the '
                            'data format convention "' + data_format + '" '
                            '(channels on axis ' + str(channels_axis) + '), i.e. expected '
                            'either 1, 3 or 4 channels on axis ' + str(channels_axis) + '. '
                            'However, it was passed an array with shape ' + str(self.x.shape) +
                            ' (' + str(self.x.shape[channels_axis]) + ' channels).')

        self.use_y = use_y
        if y is not None:
            self.y = np.asarray(y)
            self.y_shape = self.y.shape
        else:
            self.y = None
            self.y_shape = None

        self.image_data_generator = image_data_generator
        self.data_format = data_format
        self.save_to_dir = save_to_dir
        self.save_prefix = save_prefix
        self.save_format = save_format

        self.dataset = dataset

        if dataset is None:
            x_example = self.x[0]
            num_samples = self.x.shape[0]
        else:
            x,y = self.dataset.get_segment(0,1)
            x_example = x[0]
            num_samples = self.dataset.shape[0]

            if self.y is None and self.use_y:
                self.y = self.dataset.get_y()
                self.y_shape = tuple([num_samples] + list(y[0].shape))
            if self.y is not None:
                if self.y.shape[0] != num_samples:
                    raise RuntimeError('When using a dataset with a different y, please make sure that the number of samples match')

        x_example = self.image_data_generator.random_transform(x_example.astype(K.floatx()))
        x_example = self.image_data_generator.standardize(x_example)
        
        self.shape = tuple([num_samples] + list(x_example.shape))
        
        if self.y is None and distribution == 'uniform':
            RuntimeError('If uniform distribution is chosen y must be provided')
                    
        self.label_whitelist_idx = None
        if label_whitelist is not None:
            if isinstance(label_whitelist[0],str):
                self.label_whitelist_idx = []
                for label in label_whitelist:
                    self.label_whitelist_idx.append(dataset.get_label(label))
            elif isinstance(label_whitelist[0],int):
                self.label_whitelist_idx = label_whitelist
            else:
                raise ValueError('Please provide a label_whitelist as either integer indices or list of strings. For list of strings a dataset must be provided via the dataset argument')
            
            if self.y is not None:
                self.y_shape = (num_samples,len(label_whitelist))
        

        super(NumpyArrayIterator, self).__init__(self.shape[0], self.y, batch_size, shuffle, seed, whitelist=self.label_whitelist_idx, distribution=distribution)

    def _get_batches_of_transformed_samples(self, index_array, augment_index_array=None, augment_factor=0.5):
        # print(index_array)            
        batch_x = np.zeros(tuple([len(index_array)] + list(self.shape)[1:]), dtype=K.floatx())

        batch_y = None
        if self.use_y:
            batch_y = np.zeros(tuple([len(index_array)] + list(self.y_shape)[1:]), dtype=K.floatx())
        
        for i, j in enumerate(index_array):
            if self.dataset is None:
                x = self.x[j]
                if augment_index_array is not None:
                    x = x + self.x[augment_index_array[i]] * augment_factor
            else:
                x,y = self.dataset.get_segment(j,1)
                x = x[0]
                
                if augment_index_array is not None:
                    x_aug,y_aug = self.dataset.get_segment(augment_index_array[i],1)
                    x = x + x_aug[0] * augment_factor

                if self.use_y:
                    y = self.y[j]

                    if self.label_whitelist_idx is None:
                        batch_y[i] = y
                    else:
                        batch_y[i] = y[self.label_whitelist_idx]


                

            x = self.image_data_generator.random_transform(x.astype(K.floatx()))
            x = self.image_data_generator.standardize(x)
            batch_x[i] = x
        if self.save_to_dir:
            for i, j in enumerate(index_array):
                img = array_to_img(batch_x[i], self.data_format, scale=True)
                fname = '{prefix}_{index}_{hash}.{format}'.format(prefix=self.save_prefix,
                                                                  index=j,
                                                                  hash=np.random.randint(1e4),
                                                                  format=self.save_format)
                img.save(os.path.join(self.save_to_dir, fname))

        if self.dataset is None and self.y is not None:
            if self.label_whitelist_idx is None:
                batch_y = self.y[index_array]
            else:
                batch_y = self.y[index_array,self.label_whitelist_idx]

        return batch_x, batch_y

    def next(self):
        """For python 2.x.

        # Returns
            The next batch.
        """
        # Keeps under lock only the mechanism which advances
        # the indexing of each batch.
        with self.lock:
            index_array = next(self.index_generator)

        # print(index_array)
        # The transformation of images is not under thread lock
        # so it can be done in parallel
        return self._get_batches_of_transformed_samples(index_array)


# def _count_valid_files_in_directory(directory, white_list_formats, follow_links):
#     """Count files with extension in `white_list_formats` contained in a directory.

#     # Arguments
#         directory: absolute path to the directory containing files to be counted
#         white_list_formats: set of strings containing allowed extensions for
#             the files to be counted.

#     # Returns
#         the count of files with extension in `white_list_formats` contained in
#         the directory.
#     """
#     def _recursive_list(subpath):
#         return sorted(os.walk(subpath, followlinks=follow_links), key=lambda tpl: tpl[0])

#     samples = 0
#     for root, _, files in _recursive_list(directory):
#         for fname in files:
#             is_valid = False
#             for extension in white_list_formats:
#                 if fname.lower().endswith('.' + extension):
#                     is_valid = True
#                     break
#             if is_valid:
#                 samples += 1
#     return samples


# def _list_valid_filenames_in_directory(directory, white_list_formats,
#                                        class_indices, follow_links):
#     """List paths of files in `subdir` relative from `directory` whose extensions are in `white_list_formats`.

#     # Arguments
#         directory: absolute path to a directory containing the files to list.
#             The directory name is used as class label and must be a key of `class_indices`.
#         white_list_formats: set of strings containing allowed extensions for
#             the files to be counted.
#         class_indices: dictionary mapping a class name to its index.

#     # Returns
#         classes: a list of class indices
#         filenames: the path of valid files in `directory`, relative from
#             `directory`'s parent (e.g., if `directory` is "dataset/class1",
#             the filenames will be ["class1/file1.jpg", "class1/file2.jpg", ...]).
#     """
#     def _recursive_list(subpath):
#         return sorted(os.walk(subpath, followlinks=follow_links), key=lambda tpl: tpl[0])

#     classes = []
#     filenames = []
#     subdir = os.path.basename(directory)
#     basedir = os.path.dirname(directory)
#     for root, _, files in _recursive_list(directory):
#         for fname in sorted(files):
#             is_valid = False
#             for extension in white_list_formats:
#                 if fname.lower().endswith('.' + extension):
#                     is_valid = True
#                     break
#             if is_valid:
#                 classes.append(class_indices[subdir])
#                 # add filename relative to directory
#                 absolute_path = os.path.join(root, fname)
#                 filenames.append(os.path.relpath(absolute_path, basedir))
#     return classes, filenames
