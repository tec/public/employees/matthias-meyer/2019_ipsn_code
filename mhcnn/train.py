"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import sys

import pickle

import os
import glob
import sys
import socket
sys.setrecursionlimit(50000)

import time
from datetime import datetime 

import numpy as np 
import pandas as pd 
import scipy.misc
import scipy 

from aednn.toolbox import *
import aednn.custom_metrics

from toolbox import *

import yaml

modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events','image','ensemble']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpu", type=str,
                    help="use the given gpu")
parser.add_argument("-m", "--mode", type=str,
                    help="choose a mode from %s"%str(modes))
parser.add_argument("-e", "--epochs", type=int,
                    help="number of epochs used in training")
args = parser.parse_args()

if args.gpu:
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    GPU_ID = str(args.gpu)
if args.mode:
    if args.mode not in modes:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))
    mode = str(args.mode)
else:
    raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))

params = {}
params['metrics'] = ['accuracy','recall','precision','f1score']
params['optimizer'] = 'adam'
params['learning_rate'] = 0.0001
params['num_epochs'] = 1000
params['batch_size'] = 16
params['shuffle'] = True
params['transform_type'] = 'custom_melspec'
params['id'] = mode + '/' + datetime.now().strftime('%Y%m%d-%H%M%S')
params['hostname'] = socket.gethostname()
tmp_dir = get_tmp_dir()
results_dir = 'results/%s/'%(params['id'])
print('========================',results_dir,'========================')
os.makedirs(results_dir,exist_ok=True)
params['net_filename'] = results_dir+'keras.net'
params['use_generator'] = True 
params['random_seed'] = 8494563

if 'microseismic' in mode or 'quakenet' in mode or 'svm' in mode:
    from aednn.datasets.MHdataset import microseismic_dataset as dataset_class
    from aednn.models.geocnn import geocnn as model 
    # import aednn.models.aecnn as model 
    labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)

    if 'events' in mode:
        input_shape = (24,64,1)
        params['loss'] = 'binary_crossentropy'
        params['use_generator'] = False
    else:
        input_shape = (400,64,1)
        # params['loss'] = 'binary_crossentropy'
        params['loss'] = 'binary_crossentropy_weighted'

    label_mask = [3]

#    label_whitelist = ['mountaineer']

    params['learning_rate'] = 0.0001
    params['batch_size'] = 32
    params['num_epochs'] = 100

    print(np.array(labels)[label_mask])

    if 'quakenet' in mode or 'svm' in mode:
        params['transform_type'] = 'signal'
        from aednn.models.msaecnn import MSAECNN as model
        if 'events' in mode: 
            input_shape = (10000, 3)
        else:
            input_shape = (120001, 3)

elif mode == 'image':
    from aednn.datasets.MHdataset import image_dataset as dataset_class
    import aednn.models.mhcnn as model
    labels = list(pd.read_csv('common/image_labels.csv')['image_labels'].values)
    input_shape = (448,672,3)
    # label_mask = [0,1,3,4]
    label_mask = [0,1,2,3,4]

    params['loss'] = 'binary_crossentropy_weighted'
    params['num_epochs'] = 20


    print(np.array(labels)[label_mask])
elif mode == 'ensemble':
    from aednn.datasets.MHdataset import matterhorn_dataset as dataset_class
    microseismic_labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)
    image_labels = list(pd.read_csv('common/image_labels.csv')['image_labels'].values)
else:
    raise ValueError('Please provide a mode from either %s'%str(modes))


# overwrite number of epochs if provided
if args.epochs is not None:
    params['num_epochs'] = args.epochs

dataset_params = {}
# dataset_params['datadir'] = '/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/dataset/'
# dataset_params['datadir'] = '/scratch/matthmey/tmp/MHdataset/'

if 'events' in mode:
    dataset_params['datadir'] = get_data_dir() + 'event_dataset/'
    dataset_params['dataframe'] = '../data/microseismic_train_events.csv'
else:
    dataset_params['datadir'] = get_data_dir() + 'dataset/'
    dataset_params['dataframe'] = dataset_params['datadir'] + 'annotations/train.csv'

overwrite = True
if overwrite:
    import permasense
    permasense_vault_dir = '/home/perma/permasense_vault/'
    permasense.global_settings.get_global_settings()['permasense_vault_dir'] = permasense_vault_dir
    del dataset_params['datadir']

# dataset_params['dataframe'] = '/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/annotations/dataset/train_only.csv'
dataframe = pd.read_csv(dataset_params['dataframe'])
# dataframe['end_time'] = pd.to_datetime(dataframe['end_time'],utc=True)
# dataframe = dataframe.loc[dataframe['end_time'] < datetime(2016,7,11)]
dataframe = dataframe.sample(frac=1,random_state=params['random_seed']).reset_index(drop=True) # shuffle rows
# dataframe = dataframe.iloc[:600]
train_split = int(0.9 * len(dataframe))
dataset_params['dataframe'] = dataframe[:train_split]
# dataset_params['bounding_boxes'] = pd.read_csv(dataset_params['datadir'] + 'annotations/bounding_boxes.csv')
dataset = dataset_class(tmp_dir,name='%s_train'%mode,params=dataset_params,crop_image=False,transform_type=params['transform_type'],labels=labels)
X_train0, y_train0 = dataset.get_data(reload=False)

dataset_params_val = dataset_params
dataset_params_val['dataframe'] = dataframe[train_split:]
dataset_val = dataset_class(tmp_dir,name='%s_val'%mode,params=dataset_params_val,crop_image=False,transform_type=params['transform_type'],labels=labels)
X_val0, y_val0 = dataset_val.get_data(reload=False)

xsum = np.sum(X_train0,axis=(1,2,3))
print('Number of all-zero samples',np.sum(xsum == 0))

# dataset.update_labels('/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/annotations/dataset/train_only.csv')
# dataset_val.update_labels('/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/annotations/dataset/train_only.csv')
# dataset_val.update_labels('tmp/relabel_mm_val.csv')

# exit() 
# import scipy.misc
# idx = 0

# mtmask = y_train[:,4] == 1
# x = X_train[mtmask]
# x = model.preprocessing(x)
# for j in range(np.minimum(36,x.shape[0]).astype(np.int)):
#     scipy.misc.imsave('tmp/augmented/mt_%d_img.png'%(j),np.rot90(np.squeeze(x[j,...,0]),k=3))

# y_train = y_train[:,label_mask]
# y_val = y_val[:,label_mask]
# Now we can load everything GPU related

from toolbox import get_features

if 'svm' in mode:
    X_train    = get_features(X_train0,tmp_dir+'features_%s_train.npy'%mode)
    X_val      = get_features(X_val0,tmp_dir+'features_%s_val.npy'%mode)
    y_train = y_train0[:,label_mask].squeeze()
    y_val = y_val0[:,label_mask].squeeze()

    # X_train = X_train[:,:,0]
    # X_val  = X_val[:,:,0]
    X_train = X_train.reshape((X_train.shape[0],X_train.shape[1]*3))
    X_val  = X_val.reshape((X_val.shape[0],X_val.shape[1]*3))

    from sklearn.externals import joblib
    from sklearn import svm, datasets

    # get the best out of 100 tries
    best_eval_results = {'f1score':0}
    best_model = None
    best_thresholds = None
    for i in range(10):
        C = 1.0  # SVM regularization parameter
        clf = svm.LinearSVC(C=C,verbose=False)
        clf.fit(X_train,y_train)
        y_pred = clf.predict(X_val)
        eval_results,thresholds = evaluate(y_val,y_pred,eval_best='f1score',return_thresholds=True)
        if eval_results['f1score'] > best_eval_results['f1score']:
            best_eval_results = eval_results
            best_model = clf
            best_thresholds = thresholds
        print(eval_results)
    print('Best results:',best_eval_results)
    print('With the thresholds:',best_thresholds)
    joblib.dump(clf, results_dir + 'svm.pkl')
    dst_dir = 'results/%s/test/'%mode
    os.makedirs(dst_dir,exist_ok=True)
    dst = dst_dir
    joblib.dump(clf, dst + 'svm.pkl')
    exit()



#======================================================================
import keras
from keras import backend as K

from keras.models import Sequential, model_from_json, Model, save_model, load_model
from keras.optimizers import SGD, Adam
from keras import regularizers
from keras import backend as K
from keras.callbacks import ModelCheckpoint
from keras.utils.generic_utils import get_custom_objects
import tensorflow as tf 
config = tf.ConfigProto()
config.gpu_options.allow_growth = True #Do not assign whole gpu memory, just use it on the go
config.allow_soft_placement = True #If a operation is not define it the default device, let it execute in another.

from keras.backend.tensorflow_backend import set_session
set_session(tf.Session(config=config))


K.set_learning_phase(1)

keras.backend.get_session().run(tf.initialize_all_variables())


num_labels = len(label_mask)



params.update(model.get_params())



###################### CUSTOM LOSS ###################
from functools import partial, update_wrapper

def wrapped_partial(func, *args, **kwargs):
    partial_func = partial(func, *args, **kwargs)
    update_wrapper(partial_func, func)
    return partial_func

def binary_crossentropy_weighted(y_true, y_pred, class_weights):
    y_pred = K.clip(y_pred, K.epsilon(), 1.0 - K.epsilon())
    true_weights = class_weights[:,1] * y_true
    false_weights = class_weights[:,0] * (-y_true + 1)
    sample_and_class_weights = true_weights + false_weights
    loss = K.mean(sample_and_class_weights*(-y_true * K.log(y_pred) - (1.0 - y_true) * K.log(1.0 - y_pred)),axis=-1)
    return loss

# def generate_weighted_loss():
#     y_train = dataset.get_y()
#     # y_train = dataset.microseismic_dataset.get_y()
#     y_train = y_train[:,label_mask]

#     import sklearn
#     class_weights = np.zeros((y_train.shape[1],2))
#     for i in range(y_train.shape[1]):
#         class_weight = sklearn.utils.class_weight.compute_class_weight('balanced', np.unique(y_train[:,i]), y_train[:,i])
#         # print(labels_list[i],class_weight)
#         # class_weights[i] = np.minimum(class_weight,15)
#         # class_weight = np.log10(1+class_weight)
#         # class_weights[i] = class_weight / np.max(class_weight)
#         class_weights[i] = class_weight
#         # class_weights[i][0] = 1
#         # class_weights[i][1] = 1

#         # print('number of samples',np.sum(y_train[:,i]))
#         # print('class weights',class_weights[i])

#     class_weights /= np.max(class_weights)


#     bcw = wrapped_partial(binary_crossentropy_weighted, class_weights=class_weights)
#     get_custom_objects().update({"binary_crossentropy_weighted": bcw})

#     return class_weights
# class_weights = generate_weighted_loss()

def generate_weighted_loss():
    y_train = dataset.get_y()
    # y_train = dataset.microseismic_dataset.get_y()
    N = y_train.shape[1] if num_labels is None else num_labels

    import sklearn
    class_weights = np.zeros((N,2))
    for i in range(N):
        # idx = dataset.label_ids[label_whitelist[i]]
        # idx = label_mask[i]
        idx = i
        class_weight = sklearn.utils.class_weight.compute_class_weight('balanced', np.unique(y_train[:,idx]), y_train[:,idx])
        # print(labels_list[i],class_weight)
        # class_weights[i] = np.minimum(class_weight,15)
        class_weights[i] = class_weight
        # class_weights[i][0] = 1
        # class_weights[i][1] = 1
        # class_weights[i][0] = np.minimum(1,class_weights[i][0])
        # class_weights[i][1] = np.maximum(class_weights[i][1],class_weights[i][0])
        # class_weights[i][1] /= 2

    class_weights /= np.max(class_weights)

    bcw = wrapped_partial(binary_crossentropy_weighted, class_weights=class_weights)
    get_custom_objects().update({"binary_crossentropy_weighted": bcw})

    return class_weights
class_weights = generate_weighted_loss()

#======================================================================
def train_batches():

    model_list = model.get_model(dataset,input_shape,num_labels=num_labels,alpha=1,weights='imagenet')
    net = model_list[0]



    print(dataset.labels)


    import sklearn

    optimizer = params['optimizer']
    if params['optimizer'] == 'sgd':
        optimizer = SGD(params['learning_rate'])
    if params['optimizer'] == 'adam':
        optimizer = Adam(params['learning_rate'],amsgrad=True)
    
    net.compile(optimizer=optimizer, loss=params['loss'], metrics=params['metrics'])
    print('Optimizer (lr): %s (%f) \t Loss: %s '%(params['optimizer'],params['learning_rate'],params['loss']))

    net.summary()

    callbacks=[] 
    callbacks.append(ModelCheckpoint(params['net_filename']+'best_mc', monitor='val_f1score', verbose=0, save_best_only=True, save_weights_only=False, mode='max'))
    
    from keras.callbacks import ReduceLROnPlateau
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                  patience=5, min_lr=0.0000001,verbose=1)
    # callbacks.append(reduce_lr)

    def schedule(epoch,lr):
        if epoch == 50:
            lr /= 10
        if epoch == 75:
            lr /= 10 
        return lr

    callbacks.append(keras.callbacks.LearningRateScheduler(schedule, verbose=0))


    from aednn.custom_callbacks import MetricPlot, SaveBest
    mm = MetricPlot(results_dir+'trainingplots.png',['loss','acc','recall','precision','f1score'])
    callbacks.append(mm)

    from aednn.custom_classes import CustomDataGenerator as DataGenerator
    # if mode == 'microseismic' or mode == 'quakenet':
    if 'microseismic' in mode or 'quakenet' in mode:
        datagen = DataGenerator(circular_shift = True, crop_spec=True,crop_size=input_shape[0],featurewise_center=False, samplewise_center=False, featurewise_std_normalization=False, samplewise_std_normalization=False, zca_whitening=False, zca_epsilon=1e-6, rotation_range=0.0, width_shift_range=0.0, height_shift_range=0.0, shear_range=0., zoom_range=[1,1], channel_shift_range=0., fill_mode='reflect', cval=0., horizontal_flip=False, vertical_flip=False, rescale=None, preprocessing_function=model.preprocessing, data_format=K.image_data_format())
        X_train, y_train = dataset.get_data(reload=False)
        y_train = y_train[:,label_mask]
        datagen_train = datagen.flow(x=X_train, y=y_train, dataset=None, batch_size=params['batch_size'],distribution='one_per_batch',label_whitelist=None)
        X_val, y_val = dataset_val.get_data(reload=False)
        y_val = y_val[:,label_mask]
        datagen_val = datagen.flow(x=X_val, y=y_val, dataset=None,batch_size=params['batch_size'],label_whitelist=None)
    elif mode == 'image':
        datagen = DataGenerator(featurewise_center=False, samplewise_center=False, featurewise_std_normalization=False, samplewise_std_normalization=False, zca_whitening=False, zca_epsilon=1e-6, rotation_range=0.04, width_shift_range=0.02, height_shift_range=0.02, shear_range=0., zoom_range=[1,1.05], channel_shift_range=0., fill_mode='reflect', cval=0., horizontal_flip=False, vertical_flip=False, rescale=None, preprocessing_function=model.preprocessing, data_format=K.image_data_format())
        y_train = dataset.get_y()
        y_train = y_train[:,label_mask]
        datagen_train = datagen.flow(y=y_train, dataset=dataset, batch_size=params['batch_size'],distribution='one_per_batch',label_whitelist=None)
        X_val, y_val = dataset_val.get_data(reload=False)
        y_val = y_val[:,label_mask]
        datagen_val = datagen.flow(y=y_val, dataset=dataset_val, batch_size=params['batch_size'], shuffle=False, label_whitelist=None)

    
    def export_examples():
        import scipy.misc
        idx = 0
        for x,y in datagen_train:
            for j in range(x.shape[0]):
                scipy.misc.imsave('tmp/augmented/%d_img.png'%(idx),np.rot90(np.squeeze(x[j]),k=3))
                idx += 1
            if idx > 17:
                break

    # export_examples()


    steps_per_epoch = len(y_train)//params['batch_size']
    val_steps = len(y_val)//params['batch_size']
    print('Number samples',y_train.shape[0],steps_per_epoch)

    print('y_train per category',np.sum(y_train,axis=0))
    print('y_val per category',np.sum(y_val,axis=0))

    # save the information about our training settings
    with open(results_dir + "params.yml", "w") as yaml_file:
        yaml_file.write(yaml.dump(params))
    with open(results_dir + "network.txt", "w") as net_file:
        net.summary(print_fn=lambda x: net_file.write(x + '\n'))

    # label_idx = np.argmax(np.array(dataset.labels)[label_mask] == 'mountaineer')
    callbacks.append(SaveBest(params['net_filename']+'best', model.preprocessing(X_val[:,:input_shape[0],...].astype(np.float32)), y_val))

    if params['use_generator']:
        train_history = net.fit_generator(datagen_train,steps_per_epoch=steps_per_epoch, epochs=params['num_epochs'], validation_data=datagen_val, validation_steps=val_steps, callbacks=callbacks, workers=0)
        net.save(params['net_filename'])
    else:
        X_train, y_train = dataset.get_data(reload=False)
        y_train = y_train[:,label_mask]
        X_val, y_val = dataset_val.get_data(reload=False)
        y_val = y_val[:,label_mask]

        event_mask = y_train[:] == True
        event_mask = event_mask.reshape((-1,))
        print(event_mask.shape,y_train.shape,X_train.shape)

        X_events = X_train[event_mask].copy()
        X_events = X_events + np.random.normal(0,np.std(X_events),X_events.shape)

        # X_train = np.concatenate((X_train,X_events))
        # y_train = np.concatenate((y_train,np.ones((X_events.shape[0],1))))

        callbacks=[] 
        callbacks.append(ModelCheckpoint(results_dir+'keras_%s.net'%mode, monitor='val_f1score', verbose=0, save_best_only=True, save_weights_only=False, mode='max'))
        callbacks.append(MetricPlot(results_dir+'trainingplots.png',['loss','acc','recall','precision','f1score']))

        X_train_p = model.preprocessing(X_train[:,:input_shape[0],...])
        X_val_p = model.preprocessing(X_val[:,:input_shape[0],...])

        callbacks.append(SaveBest(params['net_filename']+'best',X_val_p, y_val))

        net.fit(X_train_p,y_train,batch_size=params['batch_size'],epochs=params['num_epochs'],shuffle=params['shuffle'],validation_data=(X_val_p,y_val),callbacks=callbacks)

train_batches()
 
from shutil import copyfile
from pathlib import * 


p = Path(results_dir)
for file in p.glob('*'):
    dst_dir = 'results/%s/test/'%mode
    os.makedirs(dst_dir,exist_ok=True)
    dst = dst_dir + str(file.name)
    print('Copying file',file, 'to', dst)
    copyfile(str(file), dst)
