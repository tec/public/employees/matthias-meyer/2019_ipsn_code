"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


# this script loads an INQ model specified in the model_path and weight_path variables and writes a text file in which
# all weights are provided according to V1 and V1' (prime).

# V1:
    # for every layer that contains weights the biggest value (2^n1) of the kernel is stored. The value of the nth layer
    # is stored at index n of the array called kernel_n1. Note that layers without weights are ignored.
    # The same is done for the biases

# V1':


import os
print('\n', 'GPU disabled as the VGG16 model is too big.')
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""

from keras.utils.generic_utils import get_custom_objects
import aednn.custom_metrics
# generate a fake custom loss
def binary_crossentropy_weighted(y_true, y_pred):
    return y_true*y_pred*0
get_custom_objects().update({"binary_crossentropy_weighted": binary_crossentropy_weighted})

import keras
import keras.backend as K
import numpy as np

from keras.models import model_from_json, load_model
from INQ_layers import INQ_Conv2D, INQ_Dense, Flatten
import INQ_model

import aednn.custom_metrics

modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events','image','ensemble']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpu", type=str,
                    help="use the given gpu")
parser.add_argument("-m", "--mode", type=str,
                    help="choose a mode from %s"%str(modes))
parser.add_argument("-e", "--ext", type=str, default='h',
                    help="choose the extension")
args = parser.parse_args()

if args.gpu:
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    GPU_ID = str(args.gpu)
if args.mode:
    if args.mode not in modes:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))
    mode = str(args.mode)
else:
    raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))




model_path = 'results/%s/inq/keras.netbest'%mode


INQ_B = 8



# convencience functions
########################################################################################################################

def write_layers_2_n1_string(fh, kernel, bias, layer_index):
    string = '{' + '{}, {}'.format(np.max(np.abs(temp_kernel)), np.max(np.abs(temp_bias))) + '};\n'

    if 'h' in args.ext:
    #     fh.write('const float Layer{}_2_n1[2];\n'.format(layer_index))
    # elif 'c' in args.ext:
        fh.write('const float Layer{}_2_n1[2] = '.format(layer_index) + string)



def write_layers_n1_string(fh, kernel, bias, layer_index):
    string = '{' + '{}, {}'.format(np.int(np.log2(np.max(np.abs(temp_kernel)))), np.int(np.log2(np.max(np.abs(temp_bias))))) + '};\n'
    if 'h' in args.ext:
    #     fh.write('const int32_t Layer{}_n1[2];\n'.format(layer_index))
    # elif 'c' in args.ext:
        fh.write('const int32_t Layer{}_n1[2] = '.format(layer_index) + string)

def write_layers_dims(fh, kernel, bias, layer_index):
    if len(kernel.shape) == 4:
        # it's a convolutional layer
        num_input_channels, num_output_channels, num_rows, num_columns = kernel.shape
        string = '{' + '{}, {}, {}, {}'.format(num_input_channels, num_output_channels, num_rows, num_columns) + '};\n'
        if 'h' in args.ext:
        #     fh.write('const uint32_t Layer{}_dims[4];\n'.format(layer_index))
        # elif 'c' in args.ext:
            fh.write('const uint32_t Layer{}_dims[4] = '.format(layer_index) + string)

    elif len(kernel.shape) == 2:
        # it's a fully connected layer
        num_input_channels, num_output_channels = kernel.shape
        string = '{' + '{}, {}, {}, {}'.format(num_input_channels, num_output_channels, 1, 1) + '};\n'
        if 'h' in args.ext:
        #     fh.write('const uint32_t Layer{}_dims[4];\n'.format(layer_index))
        # elif 'c' in args.ext:
            fh.write('const uint32_t Layer{}_dims[4] = '.format(layer_index) + string)

def write_layer_symbols_V1(fh, kernel, bias, layer_index):
    # handel kernel
    n1 = np.int(np.log2(np.max(np.abs(kernel))))
    LuT = []
    for ii in range(int(pow(2, INQ_B - 1)/2)):
        LuT.append(pow(2, n1 - ii))

    neg_LuT = [-x for x in LuT]
    LuT += neg_LuT
    LuT.append(0)

    Lut = np.array(LuT)

    string = '{'

    if len(kernel.shape) == 4:
        # it's a convolutional layer
        i, j, k, l = kernel.shape
        for ii in range(i):
            for jj in range(j):
                for kk in range(k):
                    for ll in range(k):
                        string += '{}'.format(np.where(LuT == kernel[ii, jj, kk, ll])[0][0])
                        if not(ii == i - 1 and jj == j - 1 and kk == k - 1 and ll == l - 1):
                            string += ', '

        string += '};\n'

    elif len(kernel.shape) == 2:
        # it's a convolutional layer
        i, j = kernel.shape
        for ii in range(i):
            for jj in range(j):
                string += '{}'.format(np.where(LuT == kernel[ii, jj])[0][0])
                if not (ii == i - 1 and jj == j - 1):
                    string += ', '

        string += '};\n'

    if 'h' in args.ext:
    #     fh.write('const uint8_t Layer{}_kernel[{}];\n'.format(layer_index, kernel.size))
    # elif 'c' in args.ext:
        fh.write('const uint8_t Layer{}_kernel[{}] = '.format(layer_index, kernel.size) + string)


    #handle bias
    n1 = np.int(np.log2(np.max(np.abs(bias))))
    LuT = []
    for ii in range(int(pow(2, INQ_B - 1)/2)):
        LuT.append(pow(2, n1 - ii))

    neg_LuT = [-x for x in LuT]
    LuT += neg_LuT
    LuT.append(0)

    Lut = np.array(LuT)

    string = '{'

    for ii in range(bias.size):
        string += '{}'.format(np.where(LuT == bias[ii])[0][0])
        if not(ii == bias.size - 1):
            string += ', '

    string += '};\n'

    if 'h' in args.ext:
    #     fh.write('const uint8_t Layer{}_bias[{}];\n'.format(layer_index, bias.size))
    # elif 'c' in args.ext:
        fh.write('const uint8_t Layer{}_bias[{}] = '.format(layer_index, bias.size) + string)



# load model
########################################################################################################################
# load json (or YAML) and create model
# json_file = open(model_path, 'r')                   # yaml_file = open('model.yaml', 'r')
# loaded_model_json = json_file.read()                # loaded_model_yaml = yaml_file.read()
# json_file.close()                                   # yaml_file.close()
# model = model_from_json(loaded_model_json, custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})          # loaded_model = model_from_yaml(loaded_model_yaml)


model = load_model(model_path,custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})
print(model.summary())

# load weights into new model
# model.load_weights(weight_path)

val_threshold = np.load('results/%s/%s/val_threshold.npy'%(mode,'inq'))


# store model parameters
########################################################################################################################
if 'h' in args.ext:
    fh = open('tmp/model_weights.h', 'w')
    fh.write('#include <stdint.h>\n\n')
    fh.write('#define NN_OUT_THRESHOLD {}\n\n'.format(val_threshold[0]))
# elif 'c' in args.ext:
#     fh = open('tmp/final_pruned_cifar_model_parameters.c', 'w')
#     fh.write('#include "stm32f4xx_hal.h"\n\n')

layer_index = 0
for layer in model.layers:
    if layer.__class__ == INQ_Conv2D:
        fh.write('\n// Convolutional (Conv2D) layer\n')

        temp_kernel = K.eval(layer.kernel).transpose((3, 2, 0, 1))   # (num_filters, num_channels, filter_height, filter_width)
        temp_bias = K.eval(layer.bias)

        # write desired layer information to .h file
        write_layers_2_n1_string(fh, temp_kernel, temp_bias, layer_index)
        write_layers_n1_string(fh, temp_kernel, temp_bias, layer_index)
        write_layers_dims(fh, temp_kernel, temp_bias, layer_index)
        write_layer_symbols_V1(fh, temp_kernel, temp_bias, layer_index)

        layer_index += 1

    elif layer.__class__ == INQ_Dense:
        fh.write('\n// Fully connected (Dense) layer\n')

        temp_kernel = K.eval(layer.kernel).transpose((1, 0))   # (output_dim, input_dim)
        temp_bias = K.eval(layer.bias)

        # write desired layer information to .h file
        write_layers_2_n1_string(fh, temp_kernel, temp_bias, layer_index)
        write_layers_n1_string(fh, temp_kernel, temp_bias, layer_index)
        write_layers_dims(fh, temp_kernel, temp_bias, layer_index)
        write_layer_symbols_V1(fh, temp_kernel, temp_bias, layer_index)

        layer_index += 1

    else:
        print('No kernel/bias to store for this layer: {} '.format(layer.name))

fh.close()




# model_score = model.evaluate(x_test, y_test, verbose=1)
# print('Test loss:', model_score[0])
# print('Test accuracy:', model_score[1])




































# fh = open('model_parameters_for_c_code.h', 'w')
#
# def convert_bias(kernel):
#     string = '{'
#     ii = 0
#     for elem in kernel:
#         string += '{:.20f}'.format(elem)
#         ii += 1
#         if ii < kernel.size:
#             string += ', '
#     string += '}'
#
#     return string
#
#
# # convert kernel
# rr = 0
# for layer in model.layers:
#     if layer.__class__ == INQ_Conv2D:
#         kernel = K.eval(layer.kernel).transpose((3, 2, 0, 1))   # (num_filters, num_channels, filter_height, filter_width)
#
#         string = '{'
#
#         ii, jj, kk, ll = kernel.shape
#         for mm in range(ii):
#             string += '{'
#             for nn in range(jj):
#                 string += '{'
#                 for oo in range(kk):
#                     string += '{'
#                     for pp in range(ll):
#                         string += '{:.20f}'.format(kernel[mm, nn, oo, pp])
#                         if pp < ll - 1:
#                             string += ', '
#                     string += '}'
#                     if oo < kk - 1:
#                         string += ', '
#                 string += '}'
#                 if nn < jj - 1:
#                     string += ', '
#             string += '}'
#             if mm < ii - 1:
#                 string += ', '
#
#         string += '}'
#
#         fh.write('// Convolutional (Conv2D) layer\n')
#         fh.write('const float kernel_L{}[{}][{}][{}][{}] = '.format(rr, ii, jj, kk, ll) + string + ';\n')
#
#         bias = K.eval(layer.bias)
#         string = convert_bias(bias)
#         fh.write('const float bias_L{}[{}] = '.format(rr, bias.size) + string + ';\n')
#
#         rr += 1
#
#     elif layer.__class__ == INQ_Dense:
#         kernel = K.eval(layer.kernel).transpose((1, 0))   # (output_dim, input_dim)
#         string = '{'
#
#         ii, jj = kernel.shape
#         for mm in range(ii):
#             string += '{'
#             for nn in range(jj):
#                 string += '{:.20f}'.format(kernel[mm, nn])
#                 if nn < jj - 1:
#                     string += ', '
#             string += '}'
#             if mm < ii - 1:
#                 string += ', '
#
#         string += '}'
#
#         fh.write('// Fully connected (Dense) layer\n')
#         fh.write('const float kernel_L{}[{}][{}] = '.format(rr, ii, jj) + string + ';\n')
#
#         bias = K.eval(layer.bias)
#         string = convert_bias(bias)
#         fh.write('const float bias_L{}[{}] = '.format(rr, bias.size) + string + ';\n')
#
#         rr += 1
#
#     fh.write('\n')
#
# fh.close()

