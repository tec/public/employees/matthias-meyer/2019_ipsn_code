"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import numpy as np
import scipy 
import scipy.misc 
import pandas as pd

from toolbox import *
import aednn.custom_metrics

modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events','image','ensemble']
runs = ['test','inq']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpu", type=str,
                    help="use the given gpu")
parser.add_argument("-m", "--mode", type=str,
                    help="choose a mode from %s"%str(modes))
parser.add_argument("-r", "--run", type=str, default='test',
                    help="choose a run from %s"%str(runs))
args = parser.parse_args()

if args.gpu:
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    GPU_ID = str(args.gpu)
if args.mode:
    if args.mode not in modes:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))
    mode = str(args.mode)
else:
    raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))


import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True #Do not assign whole gpu memory, just use it on the go
config.allow_soft_placement = True #If a operation is not define it the default device, let it execute in another.

from keras.backend.tensorflow_backend import set_session
set_session(tf.Session(config=config))

import keras
from keras import backend as K
from keras.models import Sequential, model_from_json, Model, save_model, load_model

from aednn.datasets.MHdataset import image_dataset
from aednn.toolbox import threshold, evaluate, check_overlap

from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve, precision_score, recall_score, roc_auc_score, f1_score
from keras.utils.generic_utils import get_custom_objects


calculate_val_threshold = True

# generate a fake custom loss
def binary_crossentropy_weighted(y_true, y_pred):
    return y_true*y_pred*0
get_custom_objects().update({"binary_crossentropy_weighted": binary_crossentropy_weighted})

if args.run:
    if args.run not in runs:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(runs))
    run = str(args.run)
run = args.run

params = {}
params['transform_type'] = None
params['random_seed'] = 8494563
if 'microseismic' in mode or 'quakenet' in mode or 'svm' in mode:
    from aednn.datasets.MHdataset import microseismic_dataset as dataset_class
    labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)
    if 'events' in mode:
        input_shape = (24,64,1)
    else:
        input_shape = (400,64,1)
    label_mask = [3]

    # label_whitelist = ['mountaineer']

    params['batch_size'] = 32
    params['transform_type'] = 'custom_melspec'

    print('Loading microseismic model')
    from aednn.models.geocnn import geocnn as model 
    import aednn.models.aecnn as model 
    from INQ_layers import INQ_Conv2D, INQ_Dense, Flatten
    import INQ_model



    if 'svm' not in mode:
        net = load_model('results/%s/%s/keras.netbest'%(mode,run),custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})

        # from INQ_layers import INQ_Conv2D, INQ_Dense, Flatten
        # import INQ_model
        
        # net = load_model('results/custom_inq_model.net',custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})

    if 'microseismic' not in mode:
        from aednn.models.msaecnn import MSAECNN as model
        params['transform_type'] = 'signal'
        if 'events' in mode: 
            input_shape = (10000, 3)
        else:
            input_shape = (120001, 3)
        
elif mode == 'image':
    from aednn.datasets.MHdataset import image_dataset as dataset_class
    labels = list(pd.read_csv('common/%s_labels.csv'%mode)['%s_labels'%mode].values)
    input_shape = (448,672,3)
    # label_mask = [0,1,3,4]
    label_mask = [0,1,2,3,4]

    import aednn.models.mhcnn as model
    print('Loading image model') 
    net = load_model('results/%s/%s/keras.netbest'%(mode,run))

elif mode == 'ensemble':
    from aednn.datasets.MHdataset import matterhorn_dataset as dataset_class
    microseismic_labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)
    image_labels = list(pd.read_csv('common/image_labels.csv')['image_labels'].values)
    labels = [microseismic_labels, image_labels]

    microseismic_input_shape = (512,64,3)
    image_input_shape = (448,672,3)

    microseismic_label_mask = [3]
    image_label_mask = [0,1,2,3,4]

    print('Loading image model')
    import aednn.models.mhcnn as image_model
    imgcnn = load_model('results/image/%s/keras.netbest'%(run))
    
    print('Loading microseismic model')
    from aednn.models.geocnn import geocnn as microseismic_model 
    geocnn = load_model('results/microseismic/%s/keras.netbest'%(run))
else:
    raise ValueError('Please provide a mode from either %s'%str(modes))

# label_mask = [1,3,4]
tmp_dir = get_tmp_dir()
batch_size = 32

test_params = {}
if 'events' in mode:
    test_params['datadir'] = get_data_dir() + 'event_dataset/'
    test_params['dataframe'] = '../data/microseismic_test_events.csv'
else:
    test_params['datadir'] = get_data_dir() + 'dataset/'
    test_params['dataframe'] = test_params['datadir'] + 'annotations/test.csv'

overwrite = True
if overwrite:
    import permasense
    permasense_vault_dir = '/home/perma/permasense_vault/'
    permasense.global_settings.get_global_settings()['permasense_vault_dir'] = permasense_vault_dir
    del test_params['datadir']


dataset_test = dataset_class(tmp_dir,name='%s_test'%mode,params=test_params,transform_type=params['transform_type'],labels=labels)


from toolbox import get_features
from sklearn.externals import joblib


label = 'mountaineer'


if 'svm' in mode:
    clf = joblib.load('results/%s/%s/svm.pkl'%(mode,run))

    X_test, y_test = dataset_test.get_data(dataset='%s'%('all'),reload=False)

    X_test     = get_features(X_test,tmp_dir+'features_%s_test.npy'%mode)
    # X_test     = X_test[:,:,0]
    X_test     = X_test.reshape((X_test.shape[0],X_test.shape[1]*3))
    y_test      = y_test[:,label_mask].squeeze()

    # we don't need to calculate a validation threshold since we get a boolean prediction
    y_pred = clf.predict(X_test)
    eval_results = evaluate(y_test,y_pred)
    y_pred_thres = y_pred
    print(eval_results)
    np.save('results/%s/%s/eval_results.npy'%(mode,run),eval_results)
elif mode == 'ensemble':
    for layer in geocnn.layers:
        layer.name = 'geocnn_' + layer.name
        layer.trainable = False

    X_test_ms, X_test_img, y_test = dataset_test.get_data(dataset='%s'%('all'),reload=False)

    X_test_ms = microseismic_model.preprocessing(X_test_ms[:,:microseismic_input_shape[0],...])
    y_pred_ms = geocnn.predict(X_test_ms,batch_size=16,verbose=1)
    X_test_img = image_model.preprocessing(X_test_img.astype(np.float32))
    y_pred_img = imgcnn.predict(X_test_img,batch_size=16,verbose=1)

    np.savez('tmp/pred.npz',ms=y_pred_ms,img=y_pred_img)

    data = np.load('tmp/pred.npz')
    y_pred_ms = data['ms']
    y_pred_img = data['img']

    label_idx = np.argmax(np.array(dataset_test.labels) == 'mountaineer')
    y_mt = y_test[:,label_idx]

    ms_thres = 0.55     # theshold we got from the individual validation testing (manually copied here)
    img_thres = 0.5     # theshold we got from the individual validation testing (manually copied here)

    label_idx_ms = np.argmax(np.array(microseismic_labels)[microseismic_label_mask]  == 'mountaineer')
    eval_results = evaluate(y_mt,threshold(y_pred_ms[:,label_idx_ms],ms_thres))                                 
    print('microseismic') 
    print(eval_results)
    label_idx_img = np.argmax(np.array(image_labels)[image_label_mask]  == 'mountaineer')
    
    eval_results = evaluate(y_mt,threshold(y_pred_img[:,label_idx_img],img_thres))
    print('image')
    print(eval_results)

    print('average')
    y_pred_ensemble = (y_pred_ms[:,label_idx_ms] + y_pred_img[:,label_idx_img])/2
    y_pred_ensemble = np.logical_or(threshold(y_pred_img[:,label_idx_img],img_thres),threshold(y_pred_ms[:,label_idx_ms],ms_thres))
    y_test = y_mt 
    y_pred_thres = y_pred_ensemble
else:
    K.set_learning_phase(0)

    X_test, y_test = dataset_test.get_data(dataset='%s'%('all'),reload=False)
    X_test = X_test[:X_test.shape[0]//batch_size*batch_size]
    y_test = y_test[:y_test.shape[0]//batch_size*batch_size]
    X_test = X_test[:,:input_shape[0],...]
    y_test = y_test[:,label_mask]

    net.summary()


    if calculate_val_threshold:
        dataset_params_val = {}
        if overwrite:
            dataset_params_val['dataframe'] = '../data/microseismic_train_events.csv'
        else:
            dataset_params_val['datadir'] = test_params['datadir'] 
            dataset_params_val['dataframe'] = dataset_params_val['datadir'] + 'annotations/train.csv'

        dataframe = pd.read_csv(dataset_params_val['dataframe'])
        dataframe = dataframe.sample(frac=1,random_state=params['random_seed']).reset_index(drop=True) # shuffle rows
        train_split = int(0.9 * len(dataframe))
        dataset_params_val['dataframe'] = dataframe[train_split:]
        dataset_val = dataset_class(tmp_dir,name='%s_val'%mode,params=dataset_params_val,crop_image=False,transform_type=params['transform_type'],labels=labels)
        X_val0, y_val0 = dataset_val.get_data(reload=False)

        X_val, y_val = dataset_val.get_data()
        y_val = y_val[:,label_mask]
        X_val = X_val[:,:input_shape[0],...]
        X_val = model.preprocessing(X_val.astype(np.float32))
        y_pred = net.predict(X_val,batch_size=batch_size,verbose=1)
        eval_results, thresholds = evaluate(y_val,y_pred,eval_best='f1score',return_thresholds=True)
        print('Validation results', eval_results)
        print('Validation threshold',thresholds)
        val_threshold = thresholds
    else:
        val_threshold = [0.5]

    
    np.save('results/%s/%s/val_threshold.npy'%(mode,run),val_threshold)

    #### Test the data
    X_test = model.preprocessing(X_test.astype(np.float32))
    y_pred = net.predict(X_test,batch_size=batch_size,verbose=1)



    eval_results = evaluate(y_test,threshold(y_pred,val_threshold))
    print(eval_results)


    print(np.array(dataset_test.labels)[label_mask])
    print('y_test per category',np.sum(y_test,axis=0), 'shape',y_test.shape)
    print('y_pred per category',np.sum(threshold(y_pred,val_threshold),axis=0), 'shape',y_pred.shape)


    label = 'mountaineer'
    label_idx = np.argmax(np.array(dataset_test.labels)[label_mask] == label)
    imgcnn_idx = np.argmax(np.array(dataset_test.labels)[label_mask] == label)

    print(label_idx)
    y_pred = y_pred[:,imgcnn_idx]
    y_test = y_test[:,label_idx]

    y_pred_thres = threshold(y_pred,val_threshold[label_idx])

eval_results = evaluate(y_test,y_pred_thres)
# print(label)
print(eval_results)

np.save('results/%s/%s/eval_results.npy'%(mode,run),eval_results)

# save predictions as csv
timestamps = dataset_test.timestamps.copy()
print(timestamps.columns)
timestamps['labels'] = "[]"
timestamps.loc[y_pred_thres.astype(np.bool),'labels'] = "['%s']" % label
# timestamps.to_csv('tmp/predictions.csv')


# If we have events we should evaluate also for the long-segment dataset
# since this gives us the true knowledge if we detected all mountaineers correctly
if 'events' in mode:
    import re
    mode_nonevents = re.sub('\_events$', '', mode)
    test_params = {}
    test_params['datadir'] = '/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/dataset/'
    test_params['dataframe'] = test_params['datadir'] + 'annotations/test.csv'
    dataset_nonevents = dataset_class(tmp_dir,name='%s_test'%mode_nonevents,params=test_params,transform_type=params['transform_type'],labels=labels)

    pred = timestamps
    pred['start_time']                  = pd.to_datetime(pred['start_time'],utc=True)
    pred['end_time']                    = pd.to_datetime(pred['end_time'],utc=True)
    
    timestamps = dataset_nonevents.timestamps.copy()
    timestamps['num_pred_mt_events'] = 0
    overlap_indices = check_overlap(timestamps,pred)
    for item in overlap_indices:
        if 'mountaineer' in pred.iloc[item[1]]['labels']:
            iloc = timestamps.index[item[0]]
            timestamps.loc[iloc, 'num_pred_mt_events'] += 1

    y_true = timestamps['microseismic_labels'].apply(lambda x: 'mountaineer' in x)
    y_pred = timestamps['num_pred_mt_events'] > 0
    # mask = np.logical_and(mask0,mask1)

    print('On nonevents')
    eval_results = evaluate(y_true,y_pred)
    print(eval_results)

    np.save('results/%s/%s/eval_results_nonevents.npy'%(mode,run),eval_results)


exit() 


y_pred_p = y_pred_thres.astype(np.bool)
y_test_p = y_test.astype(np.bool)
mask_fp = np.logical_and(y_pred_p, ~y_test_p)
mask_tp = np.logical_and(y_pred_p,  y_test_p)
mask_fn = np.logical_and(~y_pred_p, y_test_p)
fp = np.sum(mask_fp)
tp = np.sum(mask_tp)
fn = np.sum(mask_fn)

print('true positives ',tp)
print('false positives ',fp)
print('false negatives  ',fn)


relabel = True
if relabel:
    timestamps = dataset_test.timestamps
    timestamps = timestamps.iloc[:mask_fp.shape[0]]
    fp_timestamps = timestamps[mask_fp]
    relabel = fp_timestamps.append(timestamps[mask_fn])
    relabel.to_csv('tmp/relabel.csv')

    save_images = True
    if save_images:
        for i,x in enumerate(X_test[mask_fp]):
            scipy.misc.imsave('tmp/fp/x%d.png'%i,np.rot90(x,k=3))
        for i,x in enumerate(X_test[mask_tp]):
            scipy.misc.imsave('tmp/tp/x%d.png'%i,np.rot90(x,k=3))
        for i,x in enumerate(X_test[mask_fn]):
            scipy.misc.imsave('tmp/fn/x%d.png'%i,np.rot90(x,k=3))
