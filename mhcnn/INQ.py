"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""


'''Script to run the INQ algorithm on a given model. To run it on theserver set server = 1 and make sure the lines for disabling the gpu are commented!'''
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# flags
########################################################################################################################
debug = 0                   # for easy debugging, images are plotted during execution to check whether data augmentation is properly set up
                            # furthermore, model evaluation is disabled
                            # 0: debugging off, 1: debug validation images, 2: debug train images, 3: debug all images
verbose = 0                 # plotting kernels and biases in intermediate steps
server = 0                  # 1: the script will be running on the gpu server, 0: it will be running on the laptop
save_model = 1
evaluation_flag = 1         # 0: don't evaluate models, 1: evaluate models


modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events','image','ensemble']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpu", type=str,
                    help="use the given gpu")
parser.add_argument("-m", "--mode", type=str,
                    help="choose a mode from %s"%str(modes))
args = parser.parse_args()

if args.gpu:
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    GPU_ID = str(args.gpu)
if args.mode:
    if args.mode not in modes:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))
    mode = str(args.mode)
else:
    raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(modes))


model_selector = 'acoustic_cnn'   
# mode = 'microseismic'
run = 'test'

if server:
    from test_gpu_availability import GPU_availability_check
    GPU_availability_check()


import os
import sys
import traceback

import pickle

# if server == 0:
#     # Force CPU usage (necessary if GPU memory is too small to load and run a model)
#     print('\n', 'GPU disabled!')
#     os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
#     os.environ["CUDA_VISIBLE_DEVICES"] = ""

import keras
import keras.backend as K
from keras.layers import Conv2D, MaxPooling2D, GlobalAveragePooling2D, Reshape, Activation, Dense, Dropout, Flatten
from keras.models import Sequential
from keras.models import load_model
import json

from keras.datasets import cifar10
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input, decode_predictions

from INQ_model import INQ_Model
from INQ_layers import INQ_Conv2D, INQ_Dense

from keras.models import model_from_json

import pandas as pd
import numpy as np

import time

import h5py

from keras.preprocessing.image import ImageDataGenerator

import matplotlib as mpl
if server:
    mpl.use('Agg')
import matplotlib.pyplot as plt
# import cv2

import time
from datetime import datetime,timedelta

import aednn.custom_metrics


# in order to import functions via a parent directory it has to be added to the python path first! (not necessary in pycharm but if the script is run in the command prompt)
parentPath = os.path.abspath("../../")
if parentPath not in sys.path:
    sys.path.insert(0, parentPath)


# parameters
########################################################################################################################
# INQ parameters (some of these parameters are changed in case server == 0 or debugging is activated, see below)
if model_selector == 'acoustic_cnn':
    num_bits = 8
    num_epochs = 1
    steps = [0.5, 0.75, 0.875, 1]
    strategy = 'random'
    seed = None
    val_batch_size = 32
    learning_rate = 0.001/10/10
    train_batch_size = 32


    params = {}
    # params['loss'] = 'binary_crossentropy'
    params['loss'] = 'categorical_crossentropy'
    params['metrics'] = 'accuracy'
    params['optimizer'] = 'adam'
    params['learning_rate'] = learning_rate
    params['num_epochs'] = num_epochs
    params['batch_size'] = train_batch_size
    params['shuffle'] = True
    tmp_dir = 'tmp/'
    #results_dir = 'results/%s/' % (datetime.now().strftime('%Y%m%d-%H%M%S'))
    #os.makedirs(results_dir, exist_ok=True)
    #params['net_filename'] = results_dir + 'keras.net'
    input_shape = (400, 64, 1)
else:
    print('Unknown model chosen! Abort...')
    quit()

if debug != 0:
    steps = [0.5, 1]

if server == 0 and model_selector == 'dummy':
    # if train_batch_size > 4:    # important for the 'dummy' model
    #     train_batch_size = 4
    if val_batch_size > 4:      # important for the 'dummy' model
        val_batch_size = 3

if debug == 2 or debug == 3:
    train_batch_size = 3
if debug == 1 or debug == 3:
    val_batch_size = 3


# paths
if server:
    hdf5_path = '/data/matthmey/datasets/data_timofa/data_sets/imagenet_cls_loc_2016/imagenet_cls_loc.hdf5'
    table_path = '/data/matthmey/datasets/data_timofa/data_sets/imagenet_cls_loc_2016/map_clsloc.txt'
    save_dir = '/data/matthmey/datasets/data_timofa/results/temp'
else:
    hdf5_path = '/media/timo/Elements/MT/ImageNet/imagenet_cls_loc_keras_isotropical_complete.hdf5'
    table_path = '/home/timo/polybox/ETH/MT/data_sets/devkit2016/data/map_clsloc.txt'
    save_dir = 'tmp/INQ/%s/'%(datetime.now().strftime('%Y%m%d-%H%M%S'))

if debug != 0:
    hdf5_path = '/media/timo/Elements/MT/ImageNet/imagenet_cls_loc_keras_isotropical_10_train_images.hdf5'

os.makedirs(save_dir, exist_ok=True)

# to access ImageNet data
if model_selector == 'vgg16' or model_selector == 'dummy_vgg':
    f = h5py.File(hdf5_path, mode='r')

# # read in conversion table for imagenet labels
# table = pd.read_csv(table_path, sep=' ', header=None)   # needed for the validation labels
# # sort the list according to the synsets to convert the taining labels
# table_sorted = table.sort_values(by=0)
# table_sorted.reset_index(drop=True, inplace=True)

accuracies = []
losses = []

top_5_hist = []
top_1_hist = []

histories = []

if save_model:
    # directory to store generated files
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)

# Informations for log-file
print('\n', 'Session informations:')
print('model_selector =', model_selector)
print('server =', server)
print('evaluation_flag =', evaluation_flag)
print('\n')
print('num_bits =', num_bits)
print('steps =', steps)
# print('train_batch_size =', train_batch_size)
# print('val_batch_size =', val_batch_size)
# print('learning_rate =', learning_rate)
# print('weight_decay =', weight_decay)
# print('momentum =', momentum)
# print('strategy =', strategy)
# print('seed =', seed)


# # tensorflow gpu usage preferences (no efficient usage of gpu!)
# if K.backend() == 'tensorflow':
#     config = K.tf.ConfigProto()
#     config.gpu_options.allow_growth = True  # only allocates necessary amount (like theano). Otherwise tf would pre-allocate a fixed amount (determined by the next line)
#     config.gpu_options.per_process_gpu_memory_fraction = 1  # 1 = 100% of the GPU memory is accessible by tf
#     K.set_session(K.tf.Session(config=config))


# convenience functions
########################################################################################################################

def save_data(name, mdata):
    my_path = os.path.join(save_dir, name +'.pkl')
    my_file = open(my_path, 'wb')
    pickle.dump(mdata, my_file)
    my_file.close()
    print('\nData successfully saved at %s' %my_path)



# from https://github.com/fchollet/keras/issues/3338
def center_crop(x, center_crop_size, **kwargs):
    centerw, centerh = x.shape[1]//2, x.shape[2]//2
    halfw, halfh = center_crop_size[0]//2, center_crop_size[1]//2
    return x[:, centerw-halfw:centerw+halfw,centerh-halfh:centerh+halfh]

# from https://github.com/fchollet/keras/issues/3338
def random_crop(x, random_crop_size, sync_seed=None, **kwargs):
    np.random.seed(sync_seed)
    w, h = x.shape[1], x.shape[2]
    rangew = (w - random_crop_size[0]) // 2
    rangeh = (h - random_crop_size[1]) // 2
    offsetw = 0 if rangew == 0 else np.random.randint(rangew)
    offseth = 0 if rangeh == 0 else np.random.randint(rangeh)
    return x[:, offsetw:offsetw+random_crop_size[0], offseth:offseth+random_crop_size[1]]




def random_rgb_shift(x):
    # TODO
    return x


def gpu_memory():
    '''Returns the current gpu memory usage.'''
    out = os.popen("nvidia-smi").read()
    res = out.split('\n')[8][37:40]
    return res




def customize_model(model):

    # Disassemble layers
    layers = [l for l in model.layers]

    custom_model = INQ_Model()

    for layer in layers:
        # replace the learnable layers by corresponding INQ_layers
        if layer.__class__ == Conv2D:
            temp = INQ_Conv2D.from_config(layer.get_config())   # this call works because INQ_Conv2D is derived from Conv2D, i.e. has similar structure
            temp.save_initial_weights(layer.get_weights())
            custom_model.add(temp)
            custom_model.layers[-1].set_inq_flag()
        elif layer.__class__ == Dense:
            temp = INQ_Dense.from_config(layer.get_config())
            temp.save_initial_weights(layer.get_weights())
            custom_model.add(temp)
        else:
            custom_model.add(layer)

    return custom_model





def one_hot(y, num_classes):
  '''Returns labels in a binary NumPy ndarray.'''
  container_size = y.size
  labels_one_hot = np.zeros((container_size, num_classes))

  labels_one_hot[np.arange(container_size), y] = 1

  return labels_one_hot




def evaluate_model(model, percentage=1):
    '''For ImageNet networks this function computes the exact same numbers for the top1 and top5 accuracy as the matlab script given in the devkit2016.
    For cifar10 models it computes the test accuracy.'''
    t0 = time.time()
    if model_selector == 'acoustic_cnn':
        # X_test, y_test = dataset_val.get_data(reload=False)
        # y_test = y_test[:,label_mask]

        # X_test = X_test.transpose((0, 2, 1, 3))
        model_score = model.evaluate_generator(datagen_val)
        accuracies.append(model_score[1])
        losses.append(model_score[0])
        print('Test loss:', model_score[0])
        print('Test accuracy:', model_score[1])
    else:
        print('Unknown model chosen! Abort...')
        quit()

    t1 = time.time()
    print('Predicting took {} s'.format(t1 - t0))





    def train_model(model):
        pass







# load a model and customize it for the INQ algorithm
########################################################################################################################
print('\n', 'Load model and customize it for quantization...')

if model_selector == 'acoustic_cnn':
    from keras.utils.generic_utils import get_custom_objects
    import aednn.custom_metrics
    # generate a fake custom loss
    def binary_crossentropy_weighted(y_true, y_pred):
        return y_true*y_pred*0
    get_custom_objects().update({"binary_crossentropy_weighted": binary_crossentropy_weighted})


    model = load_model('results/%s/%s/keras.netbest'%(mode,run))

    f = open('tmp/weights.bin', 'wb')
    for element in model.get_weights():
        element.tofile(f)
    f.close()


    print("Loaded acoustic model from disk.")


else:
    print('Unknown model chosen! Abort...')
    quit()

custom_model = customize_model(model)

# if server:
#     print('\n', 'Convert custom_model to a mutli_gpu_model...')
#     #model = training_utils.multi_gpu_model(model, gpus=2)
#     custom_model = training_utils.multi_gpu_model(custom_model, gpus=2)




# prepare data for training and validation
########################################################################################################################
print('\n', 'Prepare training data...')

if model_selector == 'acoustic_cnn':
    # from aednn.datasets.aed_dataset import aed_dataset

    # dataset = aed_dataset(tmp_dir, '/usr/itetnas03/data-tik-01/matthmey/datasets/AED/sorted_dataset/')
    # X_train, y_train = dataset.get_data(reload=False) 
    # dataset_test = aed_dataset(tmp_dir, '/usr/itetnas03/data-tik-01/matthmey/datasets/AED/sorted_dataset/', name='aed_test', is_testset=True)
    # X_test, y_test = dataset_test.get_data(reload=False)
    # X_train = X_train.transpose((0, 2, 1, 3))
    # X_test = X_test.transpose((0, 2, 1, 3))
    # params = {}
    # params['metrics'] = ['accuracy']
    params['optimizer'] = 'adam'
    params['learning_rate'] = learning_rate
    params['num_epochs'] = num_epochs
    params['batch_size'] = train_batch_size
    params['shuffle'] = True
    params['transform_type'] = 'melspec'
    params['use_generator'] = True 
    params['random_seed'] = 8494563

    # print(X_train.shape)
    from aednn.datasets.MHdataset import microseismic_dataset as dataset_class
    from aednn.models.geocnn import geocnn
    # import aednn.models.aecnn as model 
    labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)

    if mode == 'microseismic_events':
        input_shape = (24,64,1)
        params['loss'] = 'binary_crossentropy'
    elif mode == 'microseismic':
        input_shape = (400,64,1)
        params['loss'] = 'binary_crossentropy_weighted'

    # params['use_generator'] = False

    label_mask = [3]

#    label_whitelist = ['mountaineer']


    dataset_params = {}
    # dataset_params['datadir'] = '/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/dataset/'
    # dataset_params['datadir'] = '/scratch/matthmey/tmp/MHdataset/'
    dataset_params['datadir'] = '../data/event_dataset/'
    dataset_params['dataframe'] = dataset_params['datadir'] + 'annotations/train.csv'

    # dataset_params['dataframe'] = '/usr/itetnas03/data-tik-01/matthmey/datasets/MHdataset/annotations/dataset/train_only.csv'
    dataframe = pd.read_csv(dataset_params['dataframe'])
    # dataframe['end_time'] = pd.to_datetime(dataframe['end_time'],utc=True)
    # dataframe = dataframe.loc[dataframe['end_time'] < datetime(2016,7,11)]
    dataframe = dataframe.sample(frac=1,random_state=params['random_seed']).reset_index(drop=True) # shuffle rows
    # dataframe = dataframe.iloc[:600]
    train_split = int(0.9 * len(dataframe))
    dataset_params['dataframe'] = dataframe[:train_split]
    # dataset_params['bounding_boxes'] = pd.read_csv(dataset_params['datadir'] + 'annotations/bounding_boxes.csv')
    dataset = dataset_class(tmp_dir,name='%s_train'%mode,params=dataset_params,crop_image=False,transform_type=params['transform_type'],labels=labels)
    X_train0, y_train0 = dataset.get_data(reload=False)

    dataset_params_val = dataset_params
    dataset_params_val['dataframe'] = dataframe[train_split:]
    dataset_val = dataset_class(tmp_dir,name='%s_val'%mode,params=dataset_params_val,crop_image=False,transform_type=params['transform_type'],labels=labels)
    X_val0, y_val0 = dataset_val.get_data(reload=False)


    from aednn.custom_classes import CustomDataGenerator as DataGenerator
    datagen = DataGenerator(circular_shift = True, crop_spec=True,crop_size=input_shape[0],featurewise_center=False, samplewise_center=False, featurewise_std_normalization=False, samplewise_std_normalization=False, zca_whitening=False, zca_epsilon=1e-6, rotation_range=0.0, width_shift_range=0.0, height_shift_range=0.0, shear_range=0., zoom_range=[1,1], channel_shift_range=0., fill_mode='reflect', cval=0., horizontal_flip=False, vertical_flip=False, rescale=None, preprocessing_function=geocnn.preprocessing, data_format=K.image_data_format())
    X_train, y_train = dataset.get_data(reload=False)
    y_train = y_train[:,label_mask]
    datagen_train = datagen.flow(x=X_train, y=y_train, dataset=None, batch_size=params['batch_size'],distribution='one_per_batch',label_whitelist=None)
    X_val, y_val = dataset_val.get_data(reload=False)
    y_val = y_val[:,label_mask]
    datagen_val = datagen.flow(x=X_val, y=y_val, dataset=None,batch_size=params['batch_size'],label_whitelist=None)

    if mode == 'microseismic_events':
        X_train = geocnn.preprocessing(X_train)
        X_val = geocnn.preprocessing(X_val)
    steps_per_epoch = len(y_train)//params['batch_size']
    val_steps = len(y_val)//params['batch_size']

else:
    print('Unknown model chosen! Abort...')
    quit()



from functools import partial, update_wrapper

def wrapped_partial(func, *args, **kwargs):
    partial_func = partial(func, *args, **kwargs)
    update_wrapper(partial_func, func)
    return partial_func

def binary_crossentropy_weighted(y_true, y_pred, class_weights):
    y_pred = K.clip(y_pred, K.epsilon(), 1.0 - K.epsilon())
    true_weights = class_weights[:,1] * y_true
    false_weights = class_weights[:,0] * (-y_true + 1)
    sample_and_class_weights = true_weights + false_weights
    loss = K.mean(sample_and_class_weights*(-y_true * K.log(y_pred) - (1.0 - y_true) * K.log(1.0 - y_pred)),axis=-1)
    return loss

def generate_weighted_loss():
    y_train = dataset.get_y()
    # y_train = dataset.microseismic_dataset.get_y()
    num_labels = len(label_mask)

    N = y_train.shape[1] if num_labels is None else num_labels

    import sklearn
    class_weights = np.zeros((N,2))
    for i in range(N):
        # idx = dataset.label_ids[label_whitelist[i]]
        # idx = label_mask[i]
        idx = i
        class_weight = sklearn.utils.class_weight.compute_class_weight('balanced', np.unique(y_train[:,idx]), y_train[:,idx])
        # print(labels_list[i],class_weight)
        # class_weights[i] = np.minimum(class_weight,15)
        class_weights[i] = class_weight
        # class_weights[i][0] = 1
        # class_weights[i][1] = 1
        # class_weights[i][0] = np.minimum(1,class_weights[i][0])
        # class_weights[i][1] = np.maximum(class_weights[i][1],class_weights[i][0])
        # class_weights[i][1] /= 2

    class_weights /= np.max(class_weights)

    bcw = wrapped_partial(binary_crossentropy_weighted, class_weights=class_weights)
    get_custom_objects().update({"binary_crossentropy_weighted": bcw})

    return class_weights
class_weights = generate_weighted_loss()


# Compile the models
########################################################################################################################
print('\n', 'Compile models...')
if model_selector == 'acoustic_cnn':
    model.compile(optimizer=params['optimizer'], loss=params['loss'], metrics=[params['metrics']])
    custom_model.compile(optimizer=params['optimizer'], loss=params['loss'], metrics=[params['metrics']])
else:
    print('Unknown model chosen! Abort...')
    quit()



# actual INQ-algorithm
########################################################################################################################
print('\n', 'Start INQ Algorithm...')

if debug != 2 and evaluation_flag == 1:
    print('\n', 'Evaluating original model for verification of the model customization procedure...')
    evaluate_model(model)


if save_model:
    model_path = os.path.join(save_dir, model_selector + '_custom_model.json')
    json_string = custom_model.to_json()
    model_file = open(model_path, 'w')
    model_file.write(json_string)
    model_file.close()
    print('\n', 'Saved model at %s ' % model_path)

for step in steps:
    if debug == 0 and evaluation_flag == 1:
        print('\n', 'Evaluate model before step {}...'.format(step))
        evaluate_model(custom_model)

    if save_model:
        weight_name = model_selector + '_before_INQ_step_{}_with_num_epochs_{}'.format(step, num_epochs)
        weight_path = os.path.join(save_dir, weight_name)
        # serialize weights to HDF5
        custom_model.save_weights(weight_path + '.h5')
        custom_model.save(weight_path + '.net')
        print('\n', 'Saved weights at %s ' % weight_path)

    print('\n', 'INQ algorithm Step: %f' %step, '---------------------------------------------------------------------')
    custom_model.partition_weights(step, strategy=strategy, seed=seed)
    custom_model.quantize(num_bits)

    if step == 1:
        if save_model:
            weight_name = model_selector + '_final_model_with_num_epochs_{}_per_step'.format(num_epochs)
            weight_path = os.path.join(save_dir, weight_name)
            # serialize weights to HDF5
            custom_model.save_weights(weight_path + '.h5')
            custom_model.save(weight_path + '.net')
            print('\n', 'Saved weights at %s ' % weight_path)
            custom_model.save('results/%s/inq/keras.netbest'%mode)


    if debug == 0 and evaluation_flag == 1:
        print('\n', 'Evaluate model after step {}...'.format(step))
        evaluate_model(custom_model)


    if step < 1:
        if model_selector == 'acoustic_cnn':
            print(dataset_val.shape[0] // params['batch_size'])

            # add callback to generate training behaviour plots
            from aednn.custom_callbacks import myCallbackPlot

            callbacks = []
            mm = myCallbackPlot(save_dir + 'trainingplot' + str(100 * step) + '.eps', ['loss', 'acc'])
            callbacks.append(mm)

            if mode == 'microseismic_events':
                history = custom_model.fit(X_train, y_train, batch_size=params['batch_size'], epochs=params['num_epochs'], validation_data=(X_val, y_val), callbacks=callbacks)
            else:
                history = custom_model.fit_generator(datagen_train,steps_per_epoch=steps_per_epoch, epochs=params['num_epochs'], validation_data=datagen_val, validation_steps=val_steps, callbacks=callbacks, workers=0)
            histories.append(history.history)
        else:
            print('Unknown model chosen! Abort...')
            quit()
    else:
        print('\n', 'The last step of the INQ algorithm was performed successfully! :)')


if save_model:
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(range(len(accuracies)), accuracies)
    ax.plot(range(len(losses)), losses)
    fig.savefig(os.path.join(save_dir, 'evaluation.eps'), format='eps')


# save training history
with open(os.path.join(save_dir, 'trainHistoryDict'), 'wb') as file_pi:
    pickle.dump(histories, file_pi)

# save quantization losses
with open(os.path.join(save_dir, 'quantizationHistoryDict'), 'wb') as file_pi:
    quantizationHistory = {}
    quantizationHistory['test_losses'] = losses
    quantizationHistory['test_acc'] = accuracies
    pickle.dump(quantizationHistory, file_pi)