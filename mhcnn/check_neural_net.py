"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import numpy as np
import pandas as pd
from keras.models import model_from_json, load_model
from INQ_layers import INQ_Conv2D, INQ_Dense, Flatten
import INQ_model
from keras.models import Model
from aednn.models.geocnn import geocnn

# we are going to use two transformations to the input signal
# 1. transform to time-frequency representation with dataset.transform
# 2. log scaling with geocnn.transform
from aednn.datasets.MHdataset import microseismic_dataset

# Parameters
tmp_dir = 'tmp/'
mode = 'microseismic_events'
run = 'zeropadding'
run = 'nozeropadding'
model_path = 'results/%s/%s/keras.netbest'%(mode,run)

# load the waveforms and the ground truth
waveforms = np.load('tmp/waveforms.npy')
ground_truths = np.load('tmp/nn_out.npy')[:,0]

# load the model
net = load_model(model_path,custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})
net.summary()

# initialize dataset class without parameters. we only want to use the transform() function
dataset = microseismic_dataset('tmp/',name='none',params={'dataframe':pd.DataFrame.from_dict({'microseismic_labels':['none'],'start_time':[0]}),'datadir':0},transform_type='custom_melspec')

# load the threshold
val_threshold = np.load('results/%s/%s/val_threshold.npy'%(mode,run))


# create a model which stops at a given layer
layer_number=-6
print('Short Model last layer name',net.layers[layer_number].name)
short_model = Model(net.layers[0].input,net.layers[layer_number].output)

# loop through the waveforms
for i in range(len(waveforms)):
    waveform = waveforms[i]
    ground_truth = ground_truths[i]

    spec = dataset.transform(waveform)
    spec = np.expand_dims(spec,-1)
    spec_pp = geocnn.preprocessing(spec)
    spec_pp = np.expand_dims(spec_pp,0)
    
    value = net.predict(spec_pp,batch_size=1)[0]
    out = value
    out[out>=val_threshold] = 1
    out[out<val_threshold] = 0

    print('Output model',value, 'Decision', out, 'Ground truth', ground_truth)
    values = short_model.predict(spec_pp,batch_size=1)[0]
    # print('Output short model',values)

    # break # Remove this to test for more waveforms
