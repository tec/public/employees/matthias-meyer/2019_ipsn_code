"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import pandas as pd 
import numpy as np 
from tqdm import tqdm  

import permasense
permasense_vault_dir = '/home/perma/permasense_vault/'
permasense.global_settings.get_global_settings()['permasense_vault_dir'] = permasense_vault_dir
import permasense.microseismic.data_loaders as ms_loader

to_c_file = True
to_np_file = False

def extract_waveforms(name):
    events = pd.read_csv('tmp/microseismic_%s_events.csv'%name)
    events['start_time']                  = pd.to_datetime(events['start_time'],utc=True)
    events['end_time']                    = pd.to_datetime(events['end_time'],utc=True)
    events['microseismic_labels']         = events['microseismic_labels'].map(eval)
    events['labels'] = events['microseismic_labels']
    events['labels'] = events['labels'].apply(lambda x: np.unique(x))

    fh = open('tmp/waveforms.h', 'w')
    fc = open('tmp/waveforms.c', 'w')

    waveform_list = []

    for i in range(1):
    # for i in range(939,940):
    # for i in tqdm(range(len(events))):
        event = events.iloc[i]

        waveform = np.zeros((12800,))
        waveform[:12800] = ms_loader.get_numpy_array(event['start_time'],event['end_time'],station='MH36',channels=['EHZ']).reshape((-1,))[:12800]

        if to_c_file:
            # write to header file
            fh.write('const float waveform{}[{}];'.format(i,len(waveform)))

            # write to c file
            # fc.write('#include "stm32f4xx_hal.h"\n\n')
            fc.write('const float waveform{}[{}] = '.format(i,len(waveform)))

            string = '{'
            for j in range(waveform.shape[0]):
                string += '{}'.format(waveform[j].astype(np.float32))
                if j != waveform.shape[0]-1:
                    string += ','

            string += '};\n'

            fc.write(string)

        if to_np_file:
            waveform_list.append(waveform)
            

        # print(string)

    fh.close()
    fc.close()

    if to_np_file:
        np.save('tmp/waveforms.npy',np.array(waveform_list))



extract_waveforms('test')
