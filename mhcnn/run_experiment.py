"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import subprocess, shlex
from shutil import copyfile
from pathlib import * 
from datetime import datetime
import os, sys
import numpy as np 
import yaml 


# modes = ['microseismic','quakenet','svm','image']
# modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events','image']
# modes = ['microseismic','microseismic_events','quakenet','quakenet_events','svm','svm_events']
# modes = ['microseismic_events','quakenet_events','svm_events']
# modes = ['microseismic','quakenet','svm']
# modes = ['microseismic']
modes = ['microseismic_events']
runs = ['test','inq']

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("-id", "--eval_id", type=str,
                    help="Select which id should be evaluated")
# parser.add_argument("-m", "--mode", type=str,
#                     help="choose a mode from %s"%str(modes))
parser.add_argument("-e", "--epochs", type=int,
                    help="number of epochs used in training")
parser.add_argument("-r", "--run", type=str, default='test',
                    help="choose a run from %s"%str(runs))
parser.add_argument("-s", "--silent", type=int,
                    help="does not display output from the subprocesses")
args = parser.parse_args()


if args.eval_id:
    run_id = args.eval_id
else:
    run_id = datetime.now().strftime('%Y%m%d-%H%M%S')

run = 'inq'
if args.run:
    if args.run not in runs:
        raise ValueError('Please choose an appropriate mode from %s with the -m option'%str(runs))
    run = str(args.run)


tmp_dir = 'tmp/'
number_runs = 40
# arguments for the subscripts
params_train = ''
params_test = ''
# params_train = '-e 0'       # can be used to check if dataset is loaded and reload all the dataset files if necessary without training
if args.epochs is not None:
    params_train = '-e %s'%(args.epochs)

all_eval_results = {}
for mode in modes:
    all_eval_results[mode] = []
    if 'events' in mode:
        all_eval_results[mode + '_nonevents'] = []

if not args.eval_id:
    for run_no in range(number_runs):
        run_dir = 'results/runs/%s/%d/'%(run_id,run_no)
        os.makedirs(run_dir,exist_ok=True)

        for mode in modes:
            mode_dir = run_dir + '%s/%s/'%(mode,run)
            os.makedirs(mode_dir,exist_ok=True)
            print('===== %s train ====='%mode)
            cmd = shlex.split('python train.py -m %s %s'%(mode,params_train))
            proc_util = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stream = proc_util.stdout.decode('utf-8')
            with open(mode_dir + 'trainlog_%s.txt'%mode, 'a') as f:
                f.write(stream)
            print('===== %s INQ ====='%mode)
            cmd = shlex.split('python INQ.py -m %s %s'%(mode,params_train))
            proc_util = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stream = proc_util.stdout.decode('utf-8')
            with open(mode_dir + 'INQ_%s.txt'%mode, 'a') as f:
                f.write(stream)

            print('===== %s test ====='%mode)
            cmd = shlex.split('python test.py -r %s -m %s %s'%(run,mode,params_test))
            proc_util = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stream = proc_util.stdout.decode('utf-8')
            with open(mode_dir + 'testlog_%s.txt'%mode, 'a') as f:
                f.write(stream)
            
            def copy_files(results_dir,dst_dir):
                p = Path(results_dir)
                for file in p.glob('*'):
                    os.makedirs(dst_dir,exist_ok=True)
                    dst = dst_dir + str(file.name)
                    # print('Copying file',file, 'to', dst)
                    copyfile(str(file), dst)
                    
            copy_files('results/%s/%s/'%(mode,'inq'),run_dir + '%s/%s/'%(mode,'inq'))
            copy_files('results/%s/%s/'%(mode,'test'),run_dir + '%s/%s/'%(mode,'test'))


            results_file = run_dir + '%s/%s/eval_results.npy'%(mode,run)
            if os.path.isfile(results_file):
                eval_results = np.load(results_file)
                all_eval_results[mode].append(eval_results)

                print(eval_results.item().get('f1score'))

                if 'events' in mode:
                    results_file = 'results/%s/%s/eval_results_nonevents.npy'%(mode,run)
                    eval_results = np.load(results_file)
                    all_eval_results[mode + '_nonevents'].append(eval_results)

    np.save('results/runs/%s/all_results.npy'%run_id,all_eval_results)
    for mode in all_eval_results.keys():
        f1scores = []
        ers = []
        for eval_results in all_eval_results[mode]:
            f1scores.append(eval_results.item().get('f1score'))
            ers.append(eval_results.item().get('er'))
        idx = np.argmax(f1scores)
        print(mode,np.mean(f1scores),f1scores[idx])
        print(mode,np.mean(ers),ers[idx])
else:
    p = Path('results/runs/%s/'%(run_id))
    for dir in sorted(p.glob('*')):
        if not dir.is_dir():
            continue
        run_dir = str(dir)
        for mode in modes:
            mode_dir = run_dir + '/%s/%s/'%(mode,run)
            p = Path(mode_dir)
            for file in p.glob('*'):
                dst_dir = 'results/%s/%s/'%(mode,run)
                os.makedirs(dst_dir,exist_ok=True)
                dst = dst_dir + str(file.name)
                # print('Copying file',file, 'to', dst)
                copyfile(str(file), dst)
            
            print('===== %s test ====='%mode)
            cmd = shlex.split('python test.py -r %s -m %s %s'%(run,mode,params_test))
            proc_util = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            stream = proc_util.stdout.decode('utf-8')
            with open(mode_dir + 'testlog_%s.txt'%mode, 'a') as f: 
                for line in stream:
                    # if not args.silent:
                    #     sys.stdout.write(line)
                    f.write(line)

            results_file = 'results/%s/%s/eval_results.npy'%(mode,run)
            if os.path.isfile(results_file):
                eval_results = np.load(results_file)
                all_eval_results[mode].append(eval_results)

                print(eval_results.item().get('f1score'))

                if 'events' in mode:
                    results_file = 'results/%s/%s/eval_results_nonevents.npy'%(mode,run)
                    eval_results = np.load(results_file)
                    all_eval_results[mode + '_nonevents'].append(eval_results)


    print('==============================')
    print('========== Results ===========')
    np.save('results/runs/%s/all_results.npy'%run_id,all_eval_results)
    for mode in all_eval_results.keys():
        print('-------',mode,'-------')
        f1scores = []
        ers = []
        for eval_results in all_eval_results[mode]:
            f1scores.append(eval_results.item().get('f1score'))
            ers.append(eval_results.item().get('er'))
        idx = np.argmax(f1scores)
        print('F1 Score\t',np.mean(f1scores),'(mean)\t',f1scores[idx],'(max)')
        print('Error Rate\t', np.mean(ers),'(mean)\t',ers[idx],'(max)')
        print('Best run', idx)
        print('')
        
