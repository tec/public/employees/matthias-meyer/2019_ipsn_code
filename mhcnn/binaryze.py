"""
Copyright (c) 2019, Swiss Federal Institute of Technology (ETH Zurich)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""

import numpy as np
import pandas as pd

tmp_dir = 'tmp/'
mode = 'microseismic_events'

import permasense
permasense_vault_dir = '/home/perma/permasense_vault/'
permasense.global_settings.get_global_settings()['permasense_vault_dir'] = permasense_vault_dir
test_params = {}
test_params['dataframe'] = 'tmp/microseismic_test_events.csv'
labels = list(pd.read_csv('common/microseismic_labels.csv')['microseismic_labels'].values)

from aednn.datasets.MHdataset import microseismic_dataset as dataset_class
dataset_test = dataset_class('tmp/',name='%s_test'%mode,params=test_params,transform_type='custom_melspec',labels=labels)
X_test, y_test = dataset_test.get_data(reload=False)

from aednn.models.geocnn import geocnn as model
X_test = model.preprocessing(X_test.astype(np.float32),bypass_transpose=True)

# shuffle test data
indices = np.arange(X_test.shape[0])
np.random.seed(5467)
np.random.shuffle(indices)
X_test = X_test[indices]
y_test = y_test[indices]

X_test = X_test[...,0]
X_test = np.squeeze(X_test) # shape is (num_image, T, F) --> after flattening, the first 64 values build the first spectrogram column etc.

y_test = y_test[:,3]
print(X_test.shape)

# X_test = np.arange(X_test.size).reshape(X_test.shape)
# X_test = np.arange(X_test.size).reshape(X_test.shape)
print(X_test.shape)
# print(X_test)

f = open('tmp/mh_test_data.bin', 'wb')

print(np.sum(y_test))
print(y_test.shape)
# print(np.array(y_test).astype(np.float32))


# we are going to interleave the samples and their labels that at the beginning we have alteranting pos/neg
positive_mask = np.where(y_test==1)[0]
negative_mask = np.where(y_test==0)[0]

short_mask = negative_mask
long_mask = positive_mask

indices = np.zeros(y_test.shape,dtype=np.int)
indices[0:2*len(short_mask):2] = short_mask
indices[1:2*len(short_mask):2] = long_mask[:len(short_mask)]
indices[2*len(short_mask):] = long_mask[len(short_mask):]

y_test = y_test[indices]
X_test = X_test[indices]

print(y_test)
for ii in range(X_test.shape[0]):
    data = np.array(X_test[ii]).flatten().astype(np.float32)
    # label = np.array(np.where(y_test[ii])[0]).astype(np.float32)
    label = np.array(y_test[ii]).astype(np.float32)
    # label = np.float32(ii)
    # print(label)

    label.tofile(f)
    data.tofile(f)

f.close()

# WRITE to c file


for i in range(8):
    fh = open('tmp/event{}.h'.format(i), 'w')
    fc = open('tmp/event{}.c'.format(i), 'w')
    event = X_test[i].flatten()

    # write to header file
    fh.write('const int8_t label{};'.format(i))
    fh.write('const float event{}[{}];'.format(i,len(event)))

    # write to c file
    fc.write('#include "stm32f4xx_hal.h"\n\n')
    fc.write('const int8_t label{} = {};'.format(i,y_test[i].astype(np.int8)))
    fc.write('const float event{}[{}] = '.format(i,len(event)))

    string = '{'
    for j in range(event.shape[0]):
        string += '{}'.format(event[j].astype(np.float32))
        if j != event.shape[0]-1:
            string += ','

    string += '};\n'

    fc.write(string)

    # print(string)

fh.close()
fc.close()

# test it
from keras.models import model_from_json, load_model
from INQ_layers import INQ_Conv2D, INQ_Dense, Flatten
import INQ_model
from keras.models import Model

import aednn.custom_metrics
from keras.utils.generic_utils import get_custom_objects
# generate a fake custom loss
def binary_crossentropy_weighted(y_true, y_pred):
    return y_true*y_pred*0
get_custom_objects().update({"binary_crossentropy_weighted": binary_crossentropy_weighted})



model_path = 'results/%s/inq/keras.netbest'%mode

net = load_model(model_path,custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})
print(net.summary())

X_test0 = X_test[:16]
X_test0 = np.expand_dims(X_test0,-1)
X_test0 = X_test0.transpose((0,2, 1, 3))
print(np.sum(X_test0,axis=(1,2,3)))

out = net.predict(X_test0,batch_size=16).squeeze()
print(out)
out[out>=0.45] = 1
out[out<0.45] = 0
print(out)

layer_number=-6
print(net.layers[layer_number].name)
net = Model(net.layers[0].input,net.layers[layer_number].output)
out = net.predict(X_test0,batch_size=16).squeeze()
print(out)
np.savetxt('tmp/layer%d.txt'%layer_number,out)


# generate a fake custom loss
def binary_crossentropy_weighted(y_true, y_pred):
    return y_true*y_pred*0
from keras.utils.generic_utils import get_custom_objects
get_custom_objects().update({"binary_crossentropy_weighted": binary_crossentropy_weighted})

model_path = 'results/%s/test/keras.netbest'%mode
net = load_model(model_path,custom_objects={'INQ_Model': INQ_model.INQ_Model, 'INQ_Conv2D': INQ_Conv2D, 'INQ_Dense': INQ_Dense})
out = net.predict(X_test0,batch_size=16).squeeze()
print(out)
out[out>=0.65] = 1
out[out<0.65] = 0
print(out)

