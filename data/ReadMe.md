# Matterhorn dataset


Download and extract the dataset from https://zenodo.org/record/1320835 or via DOI: 10.5281/zenodo.1320835

Place the *dataset*, *event_dataset* and *secondary_data* folders into this folder.